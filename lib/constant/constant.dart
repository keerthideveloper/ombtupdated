//Colors that we use in our color

import 'package:ombt/pageMap.dart';

const appPrimaryColor = Color(0xff58BC7A);
const appTextColor = Color(0xff58BC7A);
const appHeadingColor = Color(0xff58BC7A);
const appSubHeadingColor = Color(0xff58BC7A);
const appNormalTextColor = Color(0xff58BC7A);
const appButtonColor = Color(0xff58BC7A);
const appButtonTextColor = Colors.white;
const appUnselectedColor = Color(0xffF36523);

double appButtonTextSize = 2 * SizeConfig.textMultiplier;
double appNormalTextSize = 1.8 * SizeConfig.textMultiplier;
double appHeadingFontSize = 2.5 * SizeConfig.textMultiplier;
double appSubHeadingFontSize = 2 * SizeConfig.textMultiplier;

// double appButtonTextSize = 4 * SizeConfig.textMultiplier;
// double appNormalTextSize = 2.8 * SizeConfig.textMultiplier;
// double appHeadingFontSize = 3.5 * SizeConfig.textMultiplier;
// double appSubHeadingFontSize = 4 * SizeConfig.textMultiplier;
