import 'package:ombt/pageMap.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return OrientationBuilder(builder: (context, orientation) {
        SizeConfig().init(constraints, orientation);
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            primaryColor: Color(0xff58BC7A),
            accentColor: appPrimaryColor,
            textTheme: TextTheme(
                bodyText1: TextStyle(
                    fontFamily: 'Questrial',
                    color: Color.fromRGBO(109, 195, 135, 1)),
                bodyText2: TextStyle(
                    color: Color.fromRGBO(109, 195, 135, 1),
                    fontFamily: 'Questrial')),
            // textTheme: GoogleFonts.questrialTextTheme(
            //   Theme.of(context).textTheme,
            // ).apply(
            //   bodyColor: appTextColor,
            //   displayColor: appTextColor,
            // ),
            buttonTheme: ButtonThemeData(
              buttonColor: appPrimaryColor,
              textTheme: ButtonTextTheme.accent,
              colorScheme: Theme.of(context)
                  .colorScheme
                  .copyWith(secondary: appTextColor),
            ),
            unselectedWidgetColor: appUnselectedColor,
          ),
          home: OMBTSplashScreen(),
        );
      });
    });
  }
}
