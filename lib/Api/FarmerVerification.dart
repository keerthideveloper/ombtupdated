import 'package:http/http.dart' as http;

class ApiFarmerVerify {
  Future<http.Response> createFarmerOTP(data) async {
    final http.Response response = await http.post(
      'https://app.mycllc.ca/ombtapi/FarmerVerifyCtrl/GetOtp?MobileNumber=%2B91$data',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Charset': 'utf-8'
      },
      // body: jsonEncode(data),
    );

    return response;
  }

  Future<http.Response> createFarmerVerifyOTP(number, otp) async {
    final http.Response response = await http.post(
      'https://app.mycllc.ca/ombtapi/FarmerVerifyCtrl/VerifyOtp?MobileNumber=%2B91$number&Otp=$otp',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Charset': 'utf-8'
      },
    );

    return response;
  }
}
