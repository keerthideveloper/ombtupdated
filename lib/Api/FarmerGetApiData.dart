import 'package:http/http.dart' as http;

class ApiFarmerDataGet {
  Future getFarmerData(data) async {
    final response = await http.get(
      'https://app.mycllc.ca/ombtapi/farmerdetails/getfarmerbyid/$data',
    );
    // return FarmerGetData.fromJson(jsonDecode(response.body));
    return response.body;
  }
}
