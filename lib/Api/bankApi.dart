import 'dart:async';
import 'dart:io';

import 'package:dio/dio.dart';

class bankapi {
  Future<dynamic> createAlbum(data) async {
    print('${data} am here');
    FormData formData = FormData.fromMap(data);
    Response response;
    Dio dio = new Dio();
    String baseUrl =
        'https://app.mycllc.ca/ombtapi/farmerdetails/Postfarmerbankdata';

    for (File file in data['BankBooKImage']) {
      formData.files.addAll([
        MapEntry("BankBooKImage", await MultipartFile.fromFile(file.path)),
      ]);
    }

    response = await dio.post(baseUrl,
        data: formData,
        options: Options(method: 'POST', contentType: 'multipart/form-data'));

    print(response.statusCode);
    return response.data;

    // print("${response}am here find me");
  }
}
