import 'dart:convert';

import 'package:http/http.dart' as http;

class ApiLogincall {
  Future<http.Response> createLoginOTP(data) async {
    final http.Response response = await http.post(
      'https://app.mycllc.ca/ombtapi/LoginCtrl/GetOtp?MobileNumber=%2B91$data',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Charset': 'utf-8'
      },
      body: jsonEncode(data),
    );

    return response;
  }

  Future<http.Response> createLoginVerifyOTP(number, otp) async {
    final http.Response response = await http.post(
      'https://app.mycllc.ca/ombtapi/LoginCtrl/VerifyOtp?MobileNumber=%2B91$number&Otp=$otp',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Charset': 'utf-8'
      },
      body: jsonEncode('data'),
    );

    return response;
  }
}
