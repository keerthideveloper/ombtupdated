import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:ombt/modules/farmerModule.dart';
import 'package:ombt/pageMap.dart';

class FarmerApi {
  static String baseUrl = 'https://app.mycllc.ca/ombtapi/farmerdetails/';
  static const header = {
    'Content-type': 'application/json',
    'Accept': 'application/json',
  };
  Response response;
  Dio dio = new Dio();

  //Add Farmer Personal Details
  Future<dynamic> createPersonalDetail(data) async {
    FormData formData = FormData.fromMap(data);

    for (File file in data['AdharCardImage']) {
      formData.files.addAll([
        MapEntry("AdharCardImage", await MultipartFile.fromFile(file.path)),
      ]);
    }
    for (File file in data['FarmImage']) {
      formData.files.addAll([
        MapEntry("FarmImage", await MultipartFile.fromFile(file.path)),
      ]);
    }
    for (File file in data['FarmerImage']) {
      formData.files.addAll([
        MapEntry("FarmerImage", await MultipartFile.fromFile(file.path)),
      ]);
    }
    for (File file in data['PanCardImage']) {
      formData.files.addAll([
        MapEntry("PanCardImage", await MultipartFile.fromFile(file.path)),
      ]);
    }
    response = await dio.post(
        "https://app.mycllc.ca/ombtapi/farmerdetails/Postfarmerdata",
        data: formData,
        options: Options(method: 'POST', contentType: 'multipart/form-data'));

    // print(formData.fields);
    return response.data;
  }

  //Add Farmer Bank Details

  Future<dynamic> createBankDetails(data, routeUrl) async {
    print(data);
    FormData formData = FormData.fromMap(data);

    for (File file in data['BankBooKImage']) {
      formData.files.addAll([
        MapEntry("BankBooKImage", await MultipartFile.fromFile(file.path)),
      ]);
    }

    response = await dio.post(baseUrl + routeUrl,
        data: formData,
        options: Options(method: 'POST', contentType: 'multipart/form-data'));
    return response.data;
  }

  //Get Farmer Details
  Future<List<FarmerInfo>> getFarmerInfo(routUrl) async {
    print(routUrl);
    Response response = await dio.get(baseUrl + routUrl,
        options: Options(headers: {
          'Content-type': 'application/json',
          'Accept': 'application/json',
        }));

    if (response.statusCode == 200) {
      final List farmerData = jsonDecode(jsonEncode(response.data['response']));
      List<FarmerInfo> listFarmerData =
          farmerData.map((e) => FarmerInfo.fromJson(e)).toList();
      // print('new::${farmerData}');
      return listFarmerData;
    } else {
      return null;
    }
  }
}
