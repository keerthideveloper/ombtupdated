import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:ombt/modules/Product_post_data.dart';
import 'package:ombt/modules/Product_view_List.dart';

class Apicallin {
  var dataget;
  Apicallin({this.dataget});
  var datamessage;
  Future<ProductView> productview() async {
    Map<String, String> headersin = {
      'Content-Type': 'application/json; charset=UTF-8',
      'Charset': 'utf-8'
    };
    final response = await http.get(
        'https://app.mycllc.ca/ombtapi/product/getdata',
        headers: headersin);

    final makechange = jsonDecode(response.body);
    dataget = makechange['response'];
    final gettingdata = makechange['response'];
    var finalchange;
    for (var item in gettingdata) {
      item['SelectProduct'] = false;
      item['Item'] = false;
      finalchange = jsonEncode(item);
    }

    if (response.statusCode == 200) {
      // print("${finalchange} am here bala");
      return ProductView.fromJson(jsonDecode(finalchange));
    } else {
      throw Exception('Failed to load Produce');
    }
  }

  // ignore: missing_return
  Future<http.Response> createProduce(List<ProductPost> datain) async {
    final http.Response response = await http.post(
      'https://app.mycllc.ca/ombtapi/farmerdetails/Postfarmerproducedata',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Charset': 'utf-8'
      },
      body: jsonEncode(datain),
    );

    return response;
  }
}
