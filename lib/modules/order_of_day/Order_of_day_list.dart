import 'package:flutter/material.dart';

List customerday =[
  {
    'customername':'Kailas Super Market',
    'judge':false,
    'colorcard':Color.fromRGBO(245, 132, 80, 1)
  },
  {
    'customername':'Maruti Vegetables',
    'judge':false,
    'colorcard':Color.fromRGBO(245, 132, 80, 1)
  },
  {
    'customername':'Balaji Traders',
    'judge':false,
    'colorcard':Color.fromRGBO(245, 132, 80, 1)
  },
  {
    'customername':'Om Super Market',
    'judge':false,
    'colorcard':Color.fromRGBO(245, 132, 80, 1)
  },
  {
    'customername':'Parihar Super Market',
    'judge':true,
    'colorcard':Color.fromRGBO(247, 187, 17, 1),
    'rupees':'1050'
  },
  {
    'customername':'Krishna Vegetables',
    'judge':true,
    'colorcard':Color.fromRGBO(247, 187, 17, 1),
    'rupees':'1350'
  },
  {
    'customername':'Sonakshi Fruits & \nVegetable',
    'judge':true,
    'colorcard':Color.fromRGBO(247, 187, 17, 1),
    'rupees':'1100'
  },
  {
    'customername':'Sankalp Vegetable \nMart',
    'judge':true,
    'colorcard':Color.fromRGBO(247, 187, 17, 1),
    'rupees':'1500'
  },
  {
    'customername':'Sakal Organic World',
    'judge':true,
    'colorcard':Color.fromRGBO(86, 187, 122, 1),
    'rupees':'2500'
  },
  {
    'customername':'Mahalaxmi Food Mart',
    'judge':true,
    'colorcard':Color.fromRGBO(86, 187, 122, 1),
    'rupees':'4500'
  },
];