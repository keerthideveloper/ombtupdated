import 'package:flutter/material.dart';

List orderlistdetails = [
  {
    "orderDate": "24/05/20",
    'orderid': '24656',
    'quantity': '25',
    'text1color': Color.fromRGBO(107, 204, 140, 1),
    'text2color': Color.fromRGBO(248, 186, 0, 1),
    'text3color': Color.fromRGBO(107, 204, 140, 1),
    'makechange': true
  },
  {
    "orderDate": "23/05/20",
    'orderid': '698745',
    'quantity': '30',
    'text1color': Color.fromRGBO(107, 204, 140, 1),
    'text2color': Color.fromRGBO(248, 186, 0, 1),
    'text3color': Color.fromRGBO(107, 204, 140, 1),
    'makechange': true
  },
  {
    "orderDate": "22/05/20",
    'orderid': '65897',
    'quantity': '28',
    'text1color': Color.fromRGBO(107, 204, 140, 1),
    'text2color': Color.fromRGBO(248, 186, 0, 1),
    'text3color': Color.fromRGBO(107, 204, 140, 1),
    'makechange': true
  },
  {
    "orderDate": "21/05/20",
    'orderid': '12347',
    'quantity': '20',
    'text1color': Color.fromRGBO(107, 204, 140, 1),
    'text2color': Color.fromRGBO(248, 186, 0, 1),
    'text3color': Color.fromRGBO(107, 204, 140, 1),
    'makechange': true
  },
  {
    "orderDate": "20/05/20",
    'orderid': '45789',
    'quantity': '21',
    'text1color': Color.fromRGBO(107, 204, 140, 1),
    'text2color': Color.fromRGBO(248, 186, 0, 1),
    'text3color': Color.fromRGBO(107, 204, 140, 1),
    'makechange': true
  },
  {
    "orderDate": "19/05/20",
    'orderid': '36587',
    'quantity': '34',
    'text1color': Color.fromRGBO(107, 204, 140, 1),
    'text2color': Color.fromRGBO(248, 186, 0, 1),
    'text3color': Color.fromRGBO(107, 204, 140, 1),
    'makechange': true
  },
  {
    "orderDate": "18/05/20",
    'orderid': '55749',
    'quantity': '38',
    'text1color': Color.fromRGBO(107, 204, 140, 1),
    'text2color': Color.fromRGBO(248, 186, 0, 1),
    'text3color': Color.fromRGBO(107, 204, 140, 1),
    'makechange': true
  },
  {
    "orderDate": "18/05/20",
    'text1color': Color.fromRGBO(107, 204, 140, 1),
    'text3color': Color.fromRGBO(243, 101, 35, 1),
    'makechange': false
  }
];
