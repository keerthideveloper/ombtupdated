List detailspopup = [
  {
    'orderimage': 'assets/icons/orange.png',
    'orderitem': 'Orange',
    'quantity': '100',
    'ordervalue': '2500'
  },
  {
    'orderimage': 'assets/icons/cauliflower.png',
    'orderitem': 'Cauliflower',
    'quantity': '50',
    'ordervalue': '500'
  },
  {
    'orderimage': 'assets/icons/potato.png',
    'orderitem': 'Potato',
    'quantity': '150',
    'ordervalue': '1500'
  }
];
