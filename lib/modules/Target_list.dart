import 'package:flutter/material.dart';

List dailytarget = [
  {
    "targetname": "Target",
    "count": "2500 kg",
    'textcolor': Color.fromRGBO(109, 195, 135, 1),
  },
  {
    "targetname": "On Boarding",
    "count": "20 ",
    'textcolor': Colors.yellow,
  },
  {
    "targetname": "Lead Generation",
    "count": "25",
    'textcolor': Colors.deepOrange,
  },
];

List weeklytarget = [
  {
    'Target': [
      {
        "targetname": "Quantity",
        "count": "2500 kg",
        'textcolor': Color.fromRGBO(109, 195, 135, 1),
        'choice': true
      },
      {
        "targetname": "On Boarding",
        "count": "20 kg ",
        'textcolor': Colors.yellow,
        'choice': false
      },
      {
        "targetname": "Lead Generation",
        "count": "25 kg",
        'textcolor': Colors.deepOrange,
        'choice': false
      },
    ],
    'Achieved': [
      {
        "targetname": "Quantity",
        "count": "2500 kg",
        'textcolor': Color.fromRGBO(109, 195, 135, 1),
        'choice': true
      },
      {
        "targetname": "On Boarding",
        "count": "20 kg",
        'textcolor': Colors.yellow,
        'choice': false
      },
      {
        "targetname": "Lead Generation",
        "count": "25 kg",
        'textcolor': Colors.deepOrange,
        'choice': false
      },
    ]
  }
];
