class Farmer {
  String farmerName;
  String farmerLocation;
  String farmerImg;
  Farmer({this.farmerImg, this.farmerLocation, this.farmerName});
}

List<Farmer> farmerList = [
  Farmer(
      farmerName: 'keerthi',
      farmerImg: 'assets/icons/user(1).png',
      farmerLocation: 'Baner,Pune'),
  Farmer(
      farmerName: 'karthi',
      farmerImg: 'assets/icons/user(1).png',
      farmerLocation: 'Vimannagar,Pune'),
  Farmer(
      farmerName: 'Bala',
      farmerImg: 'assets/icons/user(1).png',
      farmerLocation: 'Hingewadi,Pune'),
  Farmer(
      farmerName: 'Murali',
      farmerImg: 'assets/icons/user(1).png',
      farmerLocation: 'Baner,Pune'),
  Farmer(
      farmerName: 'Ram',
      farmerImg: 'assets/icons/user(1).png',
      farmerLocation: 'Baner,Pune'),
  Farmer(
      farmerName: 'keerthi',
      farmerImg: 'assets/icons/user(1).png',
      farmerLocation: 'Baner,Pune'),
  Farmer(
      farmerName: 'karthi',
      farmerImg: 'assets/icons/user(1).png',
      farmerLocation: 'Vimannagar,Pune'),
  Farmer(
      farmerName: 'Bala',
      farmerImg: 'assets/icons/user(1).png',
      farmerLocation: 'Hingewadi,Pune'),
  Farmer(
      farmerName: 'Murali',
      farmerImg: 'assets/icons/user(1).png',
      farmerLocation: 'Baner,Pune'),
  Farmer(
      farmerName: 'Ram',
      farmerImg: 'assets/icons/user(1).png',
      farmerLocation: 'Baner,Pune'),
];
