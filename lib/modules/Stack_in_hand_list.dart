List stackhand = [
  {
    'images': 'assets/icons/orange.png',
    'productname': 'Orange',
  },
  {
    'images': 'assets/icons/tomato.png',
    'productname': 'Tomato',
  },
  {
    'images': 'assets/icons/cabbage.png',
    'productname': 'Cabbage',
  },
  {
    'images': 'assets/icons/brinjal.png',
    'productname': 'Brinjal',
  },
  {
    'images': 'assets/icons/garlic.png',
    'productname': 'Garlic',
  },
  {
    'images': 'assets/icons/onion.png',
    'productname': 'Onion',
  },
  {
    'images': 'assets/icons/potato.png',
    'productname': 'Potato',
  }
];
