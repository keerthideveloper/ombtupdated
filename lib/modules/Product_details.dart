import 'package:flutter/material.dart';

List fruits = [
  {
    "vegimg": "assets/icons/apple.png",
    "exptqty": "150 Kg",
    'recqty': '150 Kg',
    'vegname': 'Apple',
    'stay': 'Completed',
    'grade': false,
    'raise2': 'Approved from CPT',
    'iconcolor': Color.fromRGBO(109, 195, 135, 1),
    'staycolor': Color.fromRGBO(109, 195, 135, 1),
    'rasiecolor': Color.fromRGBO(0, 149, 59, 1),
    'boxImg': "assets/icons/crate2.png",
    'selected': false,
  },
  {
    "vegimg": "assets/icons/grape.png",
    "exptqty": "100 Kg",
    'recqty': '170 Kg',
    'vegname': 'Grape',
    'stay': 'Hold    ',
    'grade': false,
    'raise2': 'Send to CPT Approval',
    'iconcolor': Colors.red,
    'staycolor': Colors.red,
    'rasiecolor': Color.fromRGBO(243, 101, 35, 1),
    'boxImg': "assets/icons/crate1.png",
    'selected': false,
  },
  {
    "vegimg": "assets/icons/orange.png",
    "exptqty": "150 Kg",
    'recqty': '190 Kg',
    'vegname': 'peas',
    'stay': 'Hold    ',
    'grade': false,
    'raise2': 'Send to CPT Approval',
    'iconcolor': Colors.yellow,
    'staycolor': Colors.red,
    'rasiecolor': Color.fromRGBO(243, 101, 35, 1),
    'boxImg': "assets/icons/crate3.png",
    'selected': false,
  },
];

class Gallery {
  String imgUrl;
  bool isSelected;

  Gallery({this.imgUrl, this.isSelected});
}

List<Gallery> galleryData = [
  Gallery(imgUrl: 'assets/icons/apple.png', isSelected: true),
  Gallery(imgUrl: 'assets/icons/grape.png', isSelected: false),
  Gallery(imgUrl: 'assets/icons/peas.png', isSelected: false),
];
class Gallerys {

  bool isSelecteds;

  Gallerys({ this.isSelecteds});
}

List<Gallerys> val = [
  Gallerys( isSelecteds: true),
  Gallerys( isSelecteds: false),
  Gallerys( isSelecteds: false),
];
