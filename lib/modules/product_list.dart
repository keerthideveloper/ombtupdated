class Product {
  String productName;
  String productImg;
  bool selectProduct;
  String productQty;
  bool item;
  Product(
      {this.productName,
      this.productQty,
      this.productImg,
      this.selectProduct,
      this.item});
}

List<Product> productData = [
  Product(
      productName: 'Apple',
      productImg: 'assets/icons/apple.png',
      productQty: '100kg',
      selectProduct: false,
      item: false),
  Product(
      productName: 'Brinjal',
      productImg: 'assets/icons/aubergine-svgrepo-com.png',
      productQty: '1000kg',
      selectProduct: false,
      item: false),
  Product(
      productName: 'Strawberry',
      productImg: 'assets/icons/Emojione_1F353.png',
      productQty: '1000kg',
      selectProduct: false,
      item: false),
  Product(
      productName: 'Grapes',
      productImg: 'assets/icons/grape.png',
      productQty: '1000kg',
      selectProduct: false,
      item: false),
  Product(
      productName: 'Pineapple',
      productImg: 'assets/icons/pineapple (1).png',
      productQty: '200kg',
      selectProduct: false,
      item: false),
  Product(
      productName: 'Orange',
      productImg: 'assets/icons/orange.png',
      productQty: '100kg',
      selectProduct: false,
      item: false),
  Product(
      productName: 'Potato',
      productImg: 'assets/icons/potato.png',
      productQty: '750kg',
      selectProduct: false,
      item: false),
  Product(
      productName: 'Onion',
      productImg: 'assets/icons/clipart2067070.png',
      productQty: '500kg',
      selectProduct: false,
      item: false),
  Product(
      productName: 'cabbage',
      productImg: 'assets/icons/cabbage.png',
      productQty: '1000kg',
      selectProduct: false,
      item: false),
  Product(
      productName: 'Cauliflower',
      productImg: 'assets/icons/cauliflower.png',
      productQty: '500kg',
      selectProduct: false,
      item: false),
  Product(
      productName: 'Pumpkin',
      productImg: 'assets/icons/pumpkin.png',
      productQty: '1000kg',
      selectProduct: false,
      item: false),
  Product(
      productName: 'Pea',
      productImg: 'assets/icons/peas.png',
      productQty: '500kg',
      selectProduct: false,
      item: false),
];
var selectedProductList =
    productData.where((element) => element.selectProduct == true).toList();
