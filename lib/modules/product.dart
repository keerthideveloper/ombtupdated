class Product {
  String productName;
  String productImg;
  bool selectProduct;
  String productQty;
  Product(
      {this.productName, this.productQty, this.productImg, this.selectProduct});
}

List<Product> productData = [
  Product(
      productName: 'Apple',
      productImg: 'assets/icons/apple.png',
      productQty: '100 kg',
      selectProduct: true),
  Product(
      productName: 'Brinjal',
      productImg: 'assets/icons/aubergine-svgrepo-com.png',
      productQty: '100 kg',
      selectProduct: false),
  Product(
      productName: 'Strawberry',
      productImg: 'assets/icons/Emojione_1F353.png',
      productQty: '100 kg',
      selectProduct: false),
  Product(
      productName: 'Grapes',
      productImg: 'assets/icons/grape.png',
      productQty: '100 kg',
      selectProduct: false),
  Product(
      productName: 'Pineapple',
      productImg: 'assets/icons/pineapple (1).png',
      productQty: '200 kg',
      selectProduct: true),
  Product(
      productName: 'Orange',
      productImg: 'assets/icons/orange.png',
      productQty: '100 kg',
      selectProduct: true),
  Product(
      productName: 'Potato',
      productImg: 'assets/icons/potato.png',
      productQty: '750 kg',
      selectProduct: true),
  Product(
      productName: 'Onion',
      productImg: 'assets/icons/clipart2067070.png',
      productQty: '500 kg',
      selectProduct: true),
  Product(
      productName: 'cabbage',
      productImg: 'assets/icons/cabbage.png',
      productQty: '100 kg',
      selectProduct: false),
  Product(
      productName: 'Cauliflower',
      productImg: 'assets/icons/cauliflower.png',
      productQty: '500 kg',
      selectProduct: false),
  Product(
      productName: 'Pumpkin',
      productImg: 'assets/icons/pumpkin.png',
      productQty: '100 kg',
      selectProduct: false),
  Product(
      productName: 'Pea',
      productImg: 'assets/icons/peas.png',
      productQty: '500 kg',
      selectProduct: false),
];
