List<dynamic> vehicleList = [
  {
    "vehicleNo": "MH11 AB 4545",
    "crates": "200",
    "Color": "Colors.red",
    "storeList": [
      {
        "storeName": "Kailas Super Market",
        "customerId": "123456",
        "location": "Baner",
        "crates": "75",
        "cratesImg": "assets/icons/tool-box-1.png",
        "selected": false,
        "productList": [
          {
            "productImg": "assets/icons/bananas.png",
            "productName": "Banana",
            "productQty": "100kg",
            "crates": "assets/icons/tool-box-1.png",
            "cratesQty": "15",
          },
          {
            "productImg": "assets/icons/cauliflower.png",
            "productName": "Cauliflower",
            "productQty": "50pcs",
            "crates": "assets/icons/tool-box-2.png",
            "cratesQty": "25",
          },
          {
            "productImg": "assets/icons/bananas.png",
            "productName": "Banana",
            "productQty": "90kg",
            "crates": "assets/icons/tool-box.png",
            "cratesQty": "35",
          },
        ]
      },
      {
        "storeName": "Maruti Traders",
        "customerId": "245698",
        "location": "Baner",
        "crates": "50",
        "cratesImg": "assets/icons/tool-box-2.png",
        "selected": false,
        "productList": [
          {
            "productImg": "assets/icons/bananas.png",
            "productName": "Banana",
            "productQty": "100kg",
            "crates": "assets/icons/tool-box-1.png",
            "cratesQty": "15",
          },
          {
            "productImg": "assets/icons/cauliflower.png",
            "productName": "Cauliflower",
            "productQty": "50pcs",
            "crates": "assets/icons/tool-box-2.png",
            "cratesQty": "25",
          },
          {
            "productImg": "assets/icons/bananas.png",
            "productName": "Banana",
            "productQty": "90kg",
            "crates": "assets/icons/tool-box.png",
            "cratesQty": "35",
          },
        ]
      },
      {
        "storeName": "Balaji Traders",
        "customerId": "123478",
        "location": "Baner",
        "crates": "75",
        "cratesImg": "assets/icons/tool-box.png",
        "selected": false,
        "productList": [
          {
            "productImg": "assets/icons/bananas.png",
            "productName": "Banana",
            "productQty": "100kg",
            "crates": "assets/icons/tool-box-1.png",
            "cratesQty": "15",
          },
          {
            "productImg": "assets/icons/cauliflower.png",
            "productName": "Cauliflower",
            "productQty": "50pcs",
            "crates": "assets/icons/tool-box-2.png",
            "cratesQty": "25",
          },
          {
            "productImg": "assets/icons/bananas.png",
            "productName": "Banana",
            "productQty": "90kg",
            "crates": "assets/icons/tool-box.png",
            "cratesQty": "35",
          },
        ]
      },
    ]
  },
  {
    "vehicleNo": "MH11 AB 5043",
    "crates": "170",
    "storeList": [
      {
        "storeName": "New Super Market",
        "customerId": "345612",
        "location": "Baner",
        "crates": "35",
        "cratesImg": "assets/icons/tool-box-1.png",
        "selected": false,
        "productList": [
          {
            "productImg": "assets/icons/bananas.png",
            "productName": "Banana",
            "productQty": "100kg",
            "crates": "assets/icons/tool-box-1.png",
            "cratesQty": "15",
          },
          {
            "productImg": "assets/icons/cauliflower.png",
            "productName": "Cauliflower",
            "productQty": "50pcs",
            "crates": "assets/icons/tool-box-2.png",
            "cratesQty": "25",
          },
          {
            "productImg": "assets/icons/bananas.png",
            "productName": "Banana",
            "productQty": "90kg",
            "crates": "assets/icons/tool-box.png",
            "cratesQty": "35",
          },
        ]
      },
      {
        "storeName": "Maruti Traders",
        "customerId": "245698",
        "location": "Baner",
        "crates": "35",
        "cratesImg": "assets/icons/tool-box-2.png",
        "selected": false,
        "productList": [
          {
            "productImg": "assets/icons/bananas.png",
            "productName": "Banana",
            "productQty": "100kg",
            "crates": "assets/icons/tool-box-1.png",
            "cratesQty": "15",
          },
          {
            "productImg": "assets/icons/cauliflower.png",
            "productName": "Cauliflower",
            "productQty": "50pcs",
            "crates": "assets/icons/tool-box-2.png",
            "cratesQty": "25",
          },
          {
            "productImg": "assets/icons/bananas.png",
            "productName": "Banana",
            "productQty": "90kg",
            "crates": "assets/icons/tool-box.png",
            "cratesQty": "35",
          },
        ]
      },
      {
        "storeName": "Balaji Traders",
        "customerId": "123478",
        "location": "Baner",
        "crates": "100",
        "cratesImg": "assets/icons/tool-box.png",
        "selected": false,
        "productList": [
          {
            "productImg": "assets/icons/bananas.png",
            "productName": "Banana",
            "productQty": "100kg",
            "crates": "assets/icons/tool-box-1.png",
            "cratesQty": "15",
          },
          {
            "productImg": "assets/icons/cauliflower.png",
            "productName": "Cauliflower",
            "productQty": "50pcs",
            "crates": "assets/icons/tool-box-2.png",
            "cratesQty": "25",
          },
          {
            "productImg": "assets/icons/bananas.png",
            "productName": "Banana",
            "productQty": "90kg",
            "crates": "assets/icons/tool-box.png",
            "cratesQty": "35",
          },
        ]
      },
    ]
  }
];
