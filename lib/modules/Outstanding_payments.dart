class Outpay {
  String orderid;
  String orderdate;
  String ordervalue;

  Outpay({this.orderid, this.orderdate, this.ordervalue});
}

var outpay = <Outpay>[
  Outpay(orderid: "12347", orderdate: "21/05/2020", ordervalue: "775"),
  Outpay(orderid: "35871", orderdate: "18/05/2020", ordervalue: "1050"),
  Outpay(orderid: "36584", orderdate: "16/05/2020", ordervalue: "1800")
];
