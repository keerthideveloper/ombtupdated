class FarmerGetData {
  String status;
  List<Response> response;
  String message;

  FarmerGetData({this.status, this.response, this.message});

  FarmerGetData.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['response'] != null) {
      response = new List<Response>();
      json['response'].forEach((v) {
        response.add(new Response.fromJson(v));
      });
    }
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.response != null) {
      data['response'] = this.response.map((v) => v.toJson()).toList();
    }
    data['message'] = this.message;
    return data;
  }
}

class Response {
  Farmer farmer;
  List<FarmerProduce> farmerProduce;
  List<FarmerFarmLand> farmerFarmLand;
  List<FarmerAdharCard> farmerAdharCard;
  FarmerBankDetails farmerBankDetails;
  List<FarmerPanCard> farmerPanCard;
  String adharcardStatus;
  String pandcardStatus;
  String bankdetailsStatus;

  Response(
      {this.farmer,
      this.farmerProduce,
      this.farmerFarmLand,
      this.farmerAdharCard,
      this.farmerBankDetails,
      this.farmerPanCard,
      this.adharcardStatus,
      this.pandcardStatus,
      this.bankdetailsStatus});

  Response.fromJson(Map<String, dynamic> json) {
    farmer =
        json['Farmer'] != null ? new Farmer.fromJson(json['Farmer']) : null;
    if (json['FarmerProduce'] != null) {
      farmerProduce = new List<FarmerProduce>();
      json['FarmerProduce'].forEach((v) {
        farmerProduce.add(new FarmerProduce.fromJson(v));
      });
    }
    if (json['FarmerFarmLand'] != null) {
      farmerFarmLand = new List<FarmerFarmLand>();
      json['FarmerFarmLand'].forEach((v) {
        farmerFarmLand.add(new FarmerFarmLand.fromJson(v));
      });
    }
    if (json['FarmerAdharCard'] != null) {
      farmerAdharCard = new List<FarmerAdharCard>();
      json['FarmerAdharCard'].forEach((v) {
        farmerAdharCard.add(new FarmerAdharCard.fromJson(v));
      });
    }
    farmerBankDetails = json['FarmerBankDetails'] != null
        ? new FarmerBankDetails.fromJson(json['FarmerBankDetails'])
        : null;
    if (json['FarmerPanCard'] != null) {
      farmerPanCard = new List<FarmerPanCard>();
      json['FarmerPanCard'].forEach((v) {
        farmerPanCard.add(new FarmerPanCard.fromJson(v));
      });
    }
    adharcardStatus = json['AdharcardStatus'];
    pandcardStatus = json['PandcardStatus'];
    bankdetailsStatus = json['BankdetailsStatus'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.farmer != null) {
      data['Farmer'] = this.farmer.toJson();
    }
    if (this.farmerProduce != null) {
      data['FarmerProduce'] =
          this.farmerProduce.map((v) => v.toJson()).toList();
    }
    if (this.farmerFarmLand != null) {
      data['FarmerFarmLand'] =
          this.farmerFarmLand.map((v) => v.toJson()).toList();
    }
    if (this.farmerAdharCard != null) {
      data['FarmerAdharCard'] =
          this.farmerAdharCard.map((v) => v.toJson()).toList();
    }
    if (this.farmerBankDetails != null) {
      data['FarmerBankDetails'] = this.farmerBankDetails.toJson();
    }
    if (this.farmerPanCard != null) {
      data['FarmerPanCard'] =
          this.farmerPanCard.map((v) => v.toJson()).toList();
    }
    data['AdharcardStatus'] = this.adharcardStatus;
    data['PandcardStatus'] = this.pandcardStatus;
    data['BankdetailsStatus'] = this.bankdetailsStatus;
    return data;
  }
}

class Farmer {
  int farmerId;
  String fullName;
  String phoneNumber;
  String village;
  String farmArea;
  String pinCode;
  String locationLatitude;
  String locationLongitude;
  String farmerImage;
  String otp;
  String approved;

  Farmer(
      {this.farmerId,
      this.fullName,
      this.phoneNumber,
      this.village,
      this.farmArea,
      this.pinCode,
      this.locationLatitude,
      this.locationLongitude,
      this.farmerImage,
      this.otp,
      this.approved});

  Farmer.fromJson(Map<String, dynamic> json) {
    farmerId = json['FarmerId'];
    fullName = json['FullName'];
    phoneNumber = json['PhoneNumber'];
    village = json['Village'];
    farmArea = json['FarmArea'];
    pinCode = json['PinCode'];
    locationLatitude = json['LocationLatitude'];
    locationLongitude = json['LocationLongitude'];
    farmerImage = json['FarmerImage'];
    otp = json['Otp'];
    approved = json['Approved'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['FarmerId'] = this.farmerId;
    data['FullName'] = this.fullName;
    data['PhoneNumber'] = this.phoneNumber;
    data['Village'] = this.village;
    data['FarmArea'] = this.farmArea;
    data['PinCode'] = this.pinCode;
    data['LocationLatitude'] = this.locationLatitude;
    data['LocationLongitude'] = this.locationLongitude;
    data['FarmerImage'] = this.farmerImage;
    data['Otp'] = this.otp;
    data['Approved'] = this.approved;
    return data;
  }
}

class FarmerProduce {
  int farmerProduceId;
  int productId;
  int farmerId;
  String quantity;
  String deliveryType;

  FarmerProduce(
      {this.farmerProduceId,
      this.productId,
      this.farmerId,
      this.quantity,
      this.deliveryType});

  FarmerProduce.fromJson(Map<String, dynamic> json) {
    farmerProduceId = json['FarmerProduceId'];
    productId = json['ProductId'];
    farmerId = json['FarmerId'];
    quantity = json['Quantity'];
    deliveryType = json['DeliveryType'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['FarmerProduceId'] = this.farmerProduceId;
    data['ProductId'] = this.productId;
    data['FarmerId'] = this.farmerId;
    data['Quantity'] = this.quantity;
    data['DeliveryType'] = this.deliveryType;
    return data;
  }
}

class FarmerFarmLand {
  String image;
  int farmImageId;
  int farmerId;

  FarmerFarmLand({this.image, this.farmImageId, this.farmerId});

  FarmerFarmLand.fromJson(Map<String, dynamic> json) {
    image = json['Image'];
    farmImageId = json['FarmImageId'];
    farmerId = json['FarmerId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Image'] = this.image;
    data['FarmImageId'] = this.farmImageId;
    data['FarmerId'] = this.farmerId;
    return data;
  }
}

class FarmerAdharCard {
  String image;
  int adharCardImageId;
  int farmerId;

  FarmerAdharCard({this.image, this.adharCardImageId, this.farmerId});

  FarmerAdharCard.fromJson(Map<String, dynamic> json) {
    image = json['Image'];
    adharCardImageId = json['AdharCardImageId'];
    farmerId = json['FarmerId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Image'] = this.image;
    data['AdharCardImageId'] = this.adharCardImageId;
    data['FarmerId'] = this.farmerId;
    return data;
  }
}

class FarmerBankDetails {
  String bankFullName;
  String bankName;
  String accountNumber;
  String ifsccode;
  String bankBookImage;
  int farmerId;
  int farmerBankId;

  FarmerBankDetails(
      {this.bankFullName,
      this.bankName,
      this.accountNumber,
      this.ifsccode,
      this.bankBookImage,
      this.farmerId,
      this.farmerBankId});

  FarmerBankDetails.fromJson(Map<String, dynamic> json) {
    bankFullName = json['BankFullName'];
    bankName = json['BankName'];
    accountNumber = json['AccountNumber'];
    ifsccode = json['Ifsccode'];
    bankBookImage = json['BankBookImage'];
    farmerId = json['FarmerId'];
    farmerBankId = json['FarmerBankId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['BankFullName'] = this.bankFullName;
    data['BankName'] = this.bankName;
    data['AccountNumber'] = this.accountNumber;
    data['Ifsccode'] = this.ifsccode;
    data['BankBookImage'] = this.bankBookImage;
    data['FarmerId'] = this.farmerId;
    data['FarmerBankId'] = this.farmerBankId;
    return data;
  }
}

class FarmerPanCard {
  String image;
  int panCardImageId;
  int farmerId;

  FarmerPanCard({this.image, this.panCardImageId, this.farmerId});

  FarmerPanCard.fromJson(Map<String, dynamic> json) {
    image = json['Image'];
    panCardImageId = json['PanCardImageId'];
    farmerId = json['FarmerId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Image'] = this.image;
    data['PanCardImageId'] = this.panCardImageId;
    data['FarmerId'] = this.farmerId;
    return data;
  }
}
