import 'package:ombt/pageMap.dart';

class Unloading {
  int vehicleId;
  String vehicleNo;
  String storeName;
  String mobileNo;
  int crates;
  Color color;

  Unloading(
      {this.vehicleId,
      this.color,
      this.crates,
      this.vehicleNo,
      this.storeName,
      this.mobileNo});
}

List<Unloading> unloadingData = [
  Unloading(
    vehicleId: 1,
    vehicleNo: 'MH14 PB 1245',
    storeName: 'Gopal Tawde',
    mobileNo: '+91 98675 42098',
    crates: 20,
    color: Color(0xffFF854D),
  ),
  Unloading(
    vehicleId: 2,
    vehicleNo: 'MH14 PB 1246',
    storeName: 'Babu Bhai',
    mobileNo: '+91 98675 42098',
    crates: 20,
    color: Color(0xffFF854D),
  ),
  Unloading(
    vehicleId: 3,
    vehicleNo: 'MH14 PB 1247',
    storeName: 'Sonu Melava',
    mobileNo: '+91 98675 42098',
    crates: 20,
    color: Color(0xffF8BA00),
  ),
  Unloading(
    vehicleId: 4,
    vehicleNo: 'MH14 PB 1248',
    storeName: 'Mohan Gokhale',
    mobileNo: '+91 98675 42098',
    crates: 20,
    color: Color(0xffF8BA00),
  ),
];

class ProductDataNew {
  int vehicleId;
  String productImg;
  String productName;
  String productQty;
  String crates;
  String cratesQty;
  bool selected;
  ProductDataNew(
      {this.vehicleId,
      this.productImg,
      this.productName,
      this.productQty,
      this.cratesQty,
      this.crates,
      this.selected});
}

List<ProductDataNew> productDataNew = [
  ProductDataNew(
    vehicleId: 1,
    productImg: "assets/icons/bananas.png",
    productName: "Banana",
    productQty: "100kg",
    crates: "assets/icons/tool-box-1.png",
    cratesQty: "15",
    selected: false,
  ),
  ProductDataNew(
    vehicleId: 1,
    productImg: "assets/icons/cauliflower.png",
    productName: "Cauliflower",
    productQty: "50pcs",
    crates: "assets/icons/tool-box-2.png",
    cratesQty: "25",
    selected: false,
  ),
  ProductDataNew(
    vehicleId: 1,
    productImg: "assets/icons/bananas.png",
    productName: "Banana",
    productQty: "90kg",
    crates: "assets/icons/tool-box.png",
    cratesQty: "35",
    selected: false,
  ),
  ProductDataNew(
    vehicleId: 2,
    productImg: "assets/icons/bananas.png",
    productName: "Banana",
    productQty: "100kg",
    crates: "assets/icons/tool-box-1.png",
    cratesQty: "15",
    selected: false,
  ),
  ProductDataNew(
    vehicleId: 2,
    productImg: "assets/icons/cauliflower.png",
    productName: "Cauliflower",
    productQty: "50pcs",
    crates: "assets/icons/tool-box-2.png",
    cratesQty: "25",
    selected: false,
  ),
  ProductDataNew(
    vehicleId: 2,
    productImg: "assets/icons/bananas.png",
    productName: "Banana",
    productQty: "90kg",
    crates: "assets/icons/tool-box.png",
    cratesQty: "35",
    selected: false,
  ),
  ProductDataNew(
    vehicleId: 3,
    productImg: "assets/icons/bananas.png",
    productName: "Banana",
    productQty: "100kg",
    crates: "assets/icons/tool-box-1.png",
    cratesQty: "15",
    selected: false,
  ),
  ProductDataNew(
    vehicleId: 3,
    productImg: "assets/icons/cauliflower.png",
    productName: "Cauliflower",
    productQty: "50pcs",
    crates: "assets/icons/tool-box-2.png",
    cratesQty: "25",
    selected: false,
  ),
  ProductDataNew(
    vehicleId: 3,
    productImg: "assets/icons/bananas.png",
    productName: "Banana",
    productQty: "90kg",
    crates: "assets/icons/tool-box.png",
    cratesQty: "35",
    selected: false,
  ),
  ProductDataNew(
    vehicleId: 4,
    productImg: "assets/icons/bananas.png",
    productName: "Banana",
    productQty: "100kg",
    crates: "assets/icons/tool-box-1.png",
    cratesQty: "15",
    selected: false,
  ),
  ProductDataNew(
    vehicleId: 4,
    productImg: "assets/icons/cauliflower.png",
    productName: "Cauliflower",
    productQty: "50pcs",
    crates: "assets/icons/tool-box-2.png",
    cratesQty: "25",
    selected: false,
  ),
  ProductDataNew(
    vehicleId: 4,
    productImg: "assets/icons/bananas.png",
    productName: "Banana",
    productQty: "90kg",
    crates: "assets/icons/tool-box.png",
    cratesQty: "35",
    selected: false,
  ),
];
