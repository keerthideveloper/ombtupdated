import 'package:flutter/material.dart';

List pickinglistEDC = [
  {
    'productimg': Image.asset('assets/icons/tomato.png'),
    'productname': 'Tomato',
    'productweight': '100 Kg',
    'productcrateimg': Image.asset('assets/icons/tool-box-y.png'),
    'productcrateno': '35',
    'checkin': false
  },
  {
    'productimg': Image.asset('assets/icons/cauliflower.png'),
    'productname': 'Cauliflower',
    'productweight': '50 pcs',
    'productcrateimg': Image.asset('assets/icons/tool-box-g.png'),
    'productcrateno': '15',
    'checkin': false
  },
  {
    'productimg': Image.asset('assets/icons/bananas.png'),
    'productname': 'Banana',
    'productweight': '80 pcs',
    'productcrateimg': Image.asset('assets/icons/tool-box-o.png'),
    'productcrateno': '14',
    'checkin': false
  },
  {
    'productimg': Image.asset('assets/icons/cabbage.png'),
    'productname': 'Cabbage',
    'productweight': '160 pcs',
    'productcrateimg': Image.asset('assets/icons/tool-box-y.png'),
    'productcrateno': '11',
    'checkin': false
  },
  {
    'productimg': Image.asset('assets/icons/onion.png'),
    'productname': 'Onion',
    'productweight': '130 Kg',
    'productcrateimg': Image.asset('assets/icons/tool-box-o.png'),
    'productcrateno': '20',
    'checkin': false
  }
];
