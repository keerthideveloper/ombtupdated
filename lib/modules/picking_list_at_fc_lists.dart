import 'package:flutter/material.dart';

List picklistfc = [
  {
    'color': Colors.red,
    'title': 'EDC-1',
    'Subtitle': 'Kothrud',
    'address': 'Anand Park Chowk, Near SBI ATM, Pune'
  },
  {
    'color': Colors.yellow,
    'title': 'EDC-2',
    'Subtitle': 'Kharadi',
    'address': 'Anand Park Chowk, Near SBI ATM, Pune'
  },
  {
    'color': Colors.green,
    'title': 'EDC-3',
    'Subtitle': 'Baner',
    'address': 'Anand Park Chowk, Near SBI ATM, Pune'
  },
  {
    'color': Colors.red,
    'title': 'EDC-4',
    'Subtitle': 'NIBM',
    'address': 'Anand Park Chowk, Near SBI ATM, Pune'
  },
  {
    'color': Colors.yellow,
    'title': 'DC-5',
    'Subtitle': 'Viman Nagar',
    'address': 'Anand Park Chowk, Near SBI ATM, Pune'
  },
  {
    'color': Colors.green,
    'title': 'DC-6',
    'Subtitle': 'Pashan',
    'address': 'Anand Park Chowk, Near SBI ATM, Pune'
  }
];
