class ProductPost {
  int productID;
  String quantity;
  int farmerId;
  String deliveryType;

  ProductPost(
      {this.productID, this.quantity, this.farmerId, this.deliveryType});

  ProductPost.fromJson(Map<String, dynamic> json) {
    productID = json['ProductID'];
    quantity = json['Quantity'];
    farmerId = json['FarmerId'];
    deliveryType = json['DeliveryType'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ProductID'] = this.productID;
    data['Quantity'] = this.quantity;
    data['FarmerId'] = this.farmerId;
    data['DeliveryType'] = this.deliveryType;
    return data;
  }
}
