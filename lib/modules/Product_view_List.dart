class ProductView {
  String productName;
  String sku;
  String barcodeType;
  String productImage;
  String unit;
  int businessLocation;
  int crate;
  int moq;
  int gst;
  int category;
  int subCategory;
  bool enableManageStock;
  int alertQuantity;
  String productDescription;
  int productId;
  bool selectProduct;
  bool item;

  ProductView(
      {this.productName,
      this.sku,
      this.barcodeType,
      this.productImage,
      this.unit,
      this.businessLocation,
      this.crate,
      this.moq,
      this.gst,
      this.category,
      this.subCategory,
      this.enableManageStock,
      this.alertQuantity,
      this.productDescription,
      this.productId,
      this.selectProduct,
      this.item});

  ProductView.fromJson(Map<String, dynamic> json) {
    productName = json['ProductName'];
    sku = json['Sku'];
    barcodeType = json['BarcodeType'];
    productImage = json['ProductImage'];
    unit = json['Unit'];
    businessLocation = json['BusinessLocation'];
    crate = json['Crate'];
    moq = json['Moq'];
    gst = json['Gst'];
    category = json['Category'];
    subCategory = json['SubCategory'];
    enableManageStock = json['EnableManageStock'];
    alertQuantity = json['AlertQuantity'];
    productDescription = json['ProductDescription'];
    productId = json['ProductId'];
    selectProduct = json['SelectProduct'];
    item = json['Item'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ProductName'] = this.productName;
    data['Sku'] = this.sku;
    data['BarcodeType'] = this.barcodeType;
    data['ProductImage'] = this.productImage;
    data['Unit'] = this.unit;
    data['BusinessLocation'] = this.businessLocation;
    data['Crate'] = this.crate;
    data['Moq'] = this.moq;
    data['Gst'] = this.gst;
    data['Category'] = this.category;
    data['SubCategory'] = this.subCategory;
    data['EnableManageStock'] = this.enableManageStock;
    data['AlertQuantity'] = this.alertQuantity;
    data['ProductDescription'] = this.productDescription;
    data['ProductId'] = this.productId;
    data['SelectProduct'] = this.selectProduct;
    data['Item'] = this.item;

    return data;
  }
}
