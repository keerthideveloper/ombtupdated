import 'package:flutter/material.dart';

void showInSnackBar(_scaffoldkey, value) {
  _scaffoldkey.currentState.showSnackBar(new SnackBar(
    duration: Duration(seconds: 2),
    content: new Text('$value'),
  ));
}
