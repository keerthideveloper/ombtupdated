import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:ombt/Api/FarmerVerification.dart';
import 'package:ombt/pageMap.dart';
import 'package:sms_autofill/sms_autofill.dart';

class Getresponse {
  String status;
  String response;
  int farmerid;

  Getresponse({this.status, this.response, this.farmerid});

  Getresponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    response = json['response'];
    farmerid = json['Farmerid'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['response'] = this.response;
    data['Farmerid'] = this.farmerid;
    return data;
  }
}

ApiFarmerVerify farmerVerify = ApiFarmerVerify();
TextEditingController farmerOTPController = TextEditingController();

void showDialogpop(BuildContext context, String farmerno) {
  farmerOTPController.text = '';
  showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) => StatefulBuilder(
          builder: (BuildContext context, StateSetter setState) => AlertDialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0),
                ),
                titlePadding: EdgeInsets.all(2),
                title: Align(
                  alignment: Alignment.topRight,
                  child: IconButton(
                      icon: Icon(Icons.close),
                      onPressed: () => Navigator.pop(context)),
                ),
                content: Container(
                  height: 38 * SizeConfig.heightMultiplier,
                  width: 85.4 * SizeConfig.widthMultiplier,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        child: Column(
                          children: [
                            Image.asset(
                              'assets/logos/otp.png',
                              height: 19.2 * SizeConfig.imageSizeMultiplier,
                            ),
                            SizedBox(
                              height: 2.6 * SizeConfig.heightMultiplier,
                            ),
                            Text(
                              "Enter OTP Send To Farmer Mobile No",
                              style: TextStyle(
                                fontSize: 1.8 * SizeConfig.textMultiplier,
                                color: Color(0xff404040),
                              ),
                            ),
                            SizedBox(
                              height: 1.7 * SizeConfig.heightMultiplier,
                            ),
                            Text(
                              "+91-$farmerno",
                              style: TextStyle(
                                fontSize: 1.9 * SizeConfig.textMultiplier,
                                color: Color(0xff404040),
                              ),
                            ),
                            SizedBox(
                              height: 1.7 * SizeConfig.heightMultiplier,
                            ),
                            Container(
                              height: 5.4 * SizeConfig.heightMultiplier,
                              width: 64.2 * SizeConfig.widthMultiplier,
                              child: PinInputTextField(
                                controller: farmerOTPController,
                                pinLength: 4,
                                decoration: BoxLooseDecoration(
                                    strokeColorBuilder: FixedColorBuilder(
                                        Color(0xff62c483).withOpacity(0.8))),
                                keyboardType: TextInputType.number,
                              ),
                            ),
                            SizedBox(
                              height: 2.5 * SizeConfig.heightMultiplier,
                            ),
                            InkWell(
                              child: CircleAvatar(
                                radius: 3 * SizeConfig.heightMultiplier,
                                backgroundColor: Color(0xff58BC7A),
                                child: Icon(
                                  Icons.check,
                                  color: Colors.white,
                                  size: 6 * SizeConfig.imageSizeMultiplier,
                                ),
                              ),
                              onTap: () {
                                showDialog(
                                    context: context,
                                    child: Center(
                                        child: CircularProgressIndicator()));
                                farmerVerify
                                    .createFarmerVerifyOTP(
                                        farmerno, farmerOTPController.text)
                                    .then((value) {
                                  Map kill = jsonDecode(value.body);
                                  var killer = Getresponse.fromJson(kill);
                                  if (value.body.contains('true')) {
                                    Navigator.of(context).pop();
                                    showDialog(
                                        context: context,
                                        child: AlertDialog(
                                            contentPadding: EdgeInsets.all(10),
                                            content: Row(
                                              children: [
                                                CircleAvatar(
                                                  radius: 3 *
                                                      SizeConfig
                                                          .heightMultiplier,
                                                  backgroundColor:
                                                      Color(0xff58BC7A),
                                                  child: Icon(
                                                    Icons.done,
                                                    color: Colors.white,
                                                    size: 6 *
                                                        SizeConfig
                                                            .imageSizeMultiplier,
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 10,
                                                ),
                                                Text(
                                                    'Farmer Verified '),
                                              ],
                                            )));
                                    Future.delayed(Duration(seconds: 1), () {
                                      setState(() {
                                        farmerOTPController.text = '';
                                      });
                                      Navigator.of(context).pop();
                                    });
                                    Future.delayed(
                                        Duration(seconds: 2),
                                        () => Navigator.pushReplacement(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      FarmerDetails(
                                                          killer.farmerid)),
                                            ));
                                  } else {
                                    Navigator.of(context).pop();
                                    setState(() {
                                      farmerOTPController.text = '';
                                    });
                                    showDialog(
                                        context: context,
                                        child: AlertDialog(
                                            contentPadding: EdgeInsets.all(10),
                                            content: Row(
                                              children: [
                                                CircleAvatar(
                                                  radius: 3 *
                                                      SizeConfig
                                                          .heightMultiplier,
                                                  backgroundColor: Colors.red,
                                                  child: Icon(
                                                    Icons.error,
                                                    color: Colors.white,
                                                    size: 6 *
                                                        SizeConfig
                                                            .imageSizeMultiplier,
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 10,
                                                ),
                                                Text('Farmer OTP InValid')
                                              ],
                                            )));
                                    Future.delayed(Duration(seconds: 1),
                                        () => Navigator.of(context).pop());
                                  }
                                });
                              },
                            )
                          ],
                        ),
                      ),
                      Container(
                        width: 42.7 * SizeConfig.widthMultiplier,
                        decoration: BoxDecoration(
                          color: Color(0xffDDDDDD),
                          borderRadius: BorderRadius.circular(30.0),
                          border: Border.all(
                            width: 1 * SizeConfig.widthMultiplier,
                            color: Color(0xffDDDDDD),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ))
  );
}
