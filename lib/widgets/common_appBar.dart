import 'package:ombt/pageMap.dart';

AppBar commonAppbar({String title, Function() onPressed, context}) {
  Color textcolor = Colors.white;
  return AppBar(
    leading: IconButton(
        icon: Icon(
          Icons.arrow_back_ios,
          color: textcolor,
        ),
        onPressed: onPressed != null
            ? () => onPressed()
            : () {
                print('leading pressed');
              }),
    title: Text(
      title,
      style: TextStyle(color: textcolor, fontSize: 18),
    ),
    centerTitle: true,
    actions: [
      PopupMenuButton<int>(
        icon: Icon(
          Icons.more_vert,
          color: Colors.white,
        ),
        onSelected: (v) {
          print(v);
          if (v == 3) {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => OtpSend()));
          }
        },
        itemBuilder: (context) => [
          PopupMenuItem(
            value: 1,
            child: Center(
              child: CircleAvatar(
                radius: 25.0,
                backgroundColor: Color(0xff58BC7A),
                child: Image.asset(
                  'assets/icons/user.png',
                  height: 5.7 * SizeConfig.imageSizeMultiplier,
                ),
              ),
            ),
          ),
          PopupMenuItem(
            value: 2,
            child: Center(
                child: Text(
              "Farmer Scouts",
              style: TextStyle(fontSize: 18),
            )),
          ),
          PopupMenuItem(
            value: 3,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                // Icon(
                //   Icons.logout,
                //   color: Colors.green,
                // ),
                SizedBox(
                  width: 10.0,
                ),
                Text("Logout")
              ],
            ),
          ),
        ],
      ),
    ],
  );
}
