import 'package:ombt/pageMap.dart';
class LiveMapDialog extends StatefulWidget {
  @override
  _LiveMapDialogState createState() => _LiveMapDialogState();
}

class _LiveMapDialogState extends State<LiveMapDialog> {
  static LatLng _currentPosition;
  static LatLng _initialPosition = LatLng(21.0000, 78.0000);
  TextEditingController latitudeController = TextEditingController();
  TextEditingController longitudeController = TextEditingController();
  Completer<GoogleMapController> _controller = Completer();
  final Set<Marker> _markers = {};
  MapType _currentMapType = MapType.hybrid;
  _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
  }

  @override
  void initState() {
    _getCurrentLocation();
    super.initState();
  }

  void _getCurrentLocation() async {
    Position position =
        await getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    print(position);
    if (position != null) {
      _currentPosition = LatLng(position.latitude, position.longitude);
    } else {
      print('else');
    }
    setState(() {
      latitudeController.text = position.latitude.toString();
      longitudeController.text = position.longitude.toString();
    });
  }

  Set<Circle> circles = Set.from([
    Circle(
      circleId: CircleId('1'),
      center: _currentPosition == null ? _initialPosition : _currentPosition,
      fillColor: Colors.lightBlue.withOpacity(0.2),
      radius: 300,
      strokeWidth: 1,
      strokeColor: Colors.lightBlue,
      onTap: () {
        print('object');
      },
    ),
  ]);
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      insetPadding:
          const EdgeInsets.only(bottom: 10.0, left: 0, right: 0, top: 50.0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      content: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            children: [
              Container(
                height: 56.6 * SizeConfig.heightMultiplier,
                width: 86.7 * SizeConfig.widthMultiplier,
                child: GoogleMap(
                  onMapCreated: _onMapCreated,
                  myLocationButtonEnabled: true,
                  myLocationEnabled: true,
                  initialCameraPosition:
                      CameraPosition(target: _initialPosition, zoom: 4.5),
                  mapType: _currentMapType,
                  markers: _markers,
                  onCameraMove: null,
                  circles: circles,
                ),
              ),
              SizedBox(
                height: 1.7 * SizeConfig.heightMultiplier,
              ),
              Row(
                children: [
                  new Flexible(
                    child: Container(
                      height: 7.3 * SizeConfig.heightMultiplier,
                      child: TextFormField(
                        readOnly: true,
                        controller: latitudeController,
                        enableInteractiveSelection: true,
                        style: TextStyle(color: appTextColor),
                        decoration: InputDecoration(
                          hintText: 'Latitude',
                          filled: true,
                          fillColor: Colors.white,
                          hintStyle: TextStyle(
                            color: Color(0xff58BC7A),
                            fontSize: 2 * SizeConfig.textMultiplier,
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Color(0xff62c483), width: 2.0),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Color(0xff62c483).withOpacity(0.3),
                                width: 2.0),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 20.0,
                  ),
                  new Flexible(
                    child: Container(
                      height: 7.3 * SizeConfig.heightMultiplier,
                      child: TextFormField(
                        readOnly: true,
                        controller: longitudeController,
                        style: TextStyle(color: appTextColor),
                        decoration: InputDecoration(
                          hintText: 'Longitude',
                          filled: true,
                          fillColor: Colors.white,
                          hintStyle: TextStyle(
                            color: Color(0xff58BC7A),
                            fontSize: 2 * SizeConfig.textMultiplier,
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Color(0xff62c483), width: 2.0),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Color(0xff62c483).withOpacity(0.3),
                                width: 2.0),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
          Container(
            child: Column(
              children: [
                Container(
                  height: 6.5 * SizeConfig.heightMultiplier,
                  width: double.infinity,
                  child: AppButton(
                    onPressed: () {
                      var location = {
                        'long': longitudeController.text,
                        'lat': latitudeController.text,
                      };
                      // print(location);
                      Navigator.pop(context, location);
                    },
                    name: 'capture location',
                  ),
                ),
                SizedBox(
                  height: 3.3 * SizeConfig.heightMultiplier,
                ),
                Container(
                  width: 26.7 * SizeConfig.widthMultiplier,
                  decoration: BoxDecoration(
                    border: Border.all(
                      width: 6.0,
                      color: Color(0xffDDDDDD),
                    ),
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
