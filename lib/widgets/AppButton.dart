import 'package:ombt/pageMap.dart';

class AppButton extends StatefulWidget {
  String name;
  Function onPressed;
  Color color;
  FocusNode focusNode;
  Color textColor;
  AppButton({this.name,this.focusNode, this.onPressed, this.color, this.textColor});

  @override
  _AppButtonState createState() => _AppButtonState();
}

class _AppButtonState extends State<AppButton> {
  @override
  Widget build(BuildContext context) {
    print(' hiiii::${2.2 * SizeConfig.heightMultiplier}');
    return Container(
      height: 6 * SizeConfig.heightMultiplier,
      width: double.infinity,
      child: RaisedButton(
        elevation: 5,
        color: widget.color != null ? widget.color : appButtonColor,
        focusNode: widget.focusNode,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
        padding: EdgeInsets.symmetric(vertical: 12),
        onPressed: widget.onPressed,
        child: Text(
          widget.name != null ? widget.name : '',
          style: TextStyle(
              fontSize: appButtonTextSize,
              color:
                  widget.textColor != null ? widget.textColor : Colors.white),
        ),
      ),
    );
  }
}
