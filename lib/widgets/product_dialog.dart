import 'package:ombt/modules/Product_post_data.dart';
import 'package:ombt/pageMap.dart';

class ProductDialog extends StatefulWidget {
  final int indexin;
  final product;
  final bool checker;
  final List getlist;
  ProductDialog({this.indexin, this.product, this.checker, this.getlist});
  @override
  _ProductDialogState createState() => _ProductDialogState();
}

class _ProductDialogState extends State<ProductDialog> {
  String _radioValue;
  var selected;

  double titleFontSize = 2.5;
  double subTitleFontSize = 2;
  double content = 1.6;
  // List<Map<String, dynamic>> productdata = <Map<String, dynamic>>[];
  TextEditingController quantityController = TextEditingController();
  void radioButtonChange(String value) {
    setState(() {
      _radioValue = value;
    });
  }

  bool validatein = false;
  @override
  Widget build(BuildContext context) {
    // final landscape =
    //     MediaQuery.of(context).orientation == Orientation.landscape;
    if (!widget.checker) {
      _radioValue = widget.getlist[widget.indexin].deliveryType;
    }

    quantityController.text = widget.product.alertQuantity.toString();
    return AlertDialog(
      insetPadding: const EdgeInsets.all(0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(30.0),
      ),
      title: Text(
        'Product List',
        style: TextStyle(fontSize: appHeadingFontSize),
      ),
      content: Container(
        height: 33.7 * SizeConfig.heightMultiplier,
        width: 100 * SizeConfig.widthMultiplier,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              children: [
                Container(
                  padding: EdgeInsets.only(bottom: 10.0),
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(color: Color(0xffF9C93A), width: 2.0),
                    ),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Product',
                        style: TextStyle(fontSize: appSubHeadingFontSize),
                      ),
                      Text(
                        'Quantity',
                        style: TextStyle(fontSize: appSubHeadingFontSize),
                      ),
                      Text(
                        'Daily/weekly/monthly',
                        style: TextStyle(fontSize: appSubHeadingFontSize),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 3.3 * SizeConfig.heightMultiplier,
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Container(
                        width: 18.0 * SizeConfig.widthMultiplier,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Image.network(
                              'https://app.mycllc.ca/ombtbackend/images/${widget.product.productImage}',
                              height: 6.2 * SizeConfig.imageSizeMultiplier,
                            ),
                            SizedBox(
                              height: content * SizeConfig.heightMultiplier,
                            ),
                            Text(
                              widget.product.productName,
                              style: TextStyle(fontSize: appNormalTextSize),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        width: 4 * SizeConfig.widthMultiplier,
                      ),
                      Container(
                        padding: const EdgeInsets.only(top: 10.0),
                        height: 7.3 * SizeConfig.heightMultiplier,
                        width: 20 * SizeConfig.widthMultiplier,
                        child: AppTextField(
                          controllerName: quantityController,
                          labelName: "Quantity",
                        ),
                      ),
                      Container(
                          width: 44.0 * SizeConfig.widthMultiplier,
                          child: Column(children: [
                            validatein
                                ? Center(
                                    child: Text(
                                      'Please select any \n Daily/weekly/monthly',
                                      style: TextStyle(
                                          color: Colors.red, fontSize: 10),
                                    ),
                                  )
                                : Container(),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Container(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      Radio(
                                        focusColor: Colors.orange,
                                        materialTapTargetSize:
                                            MaterialTapTargetSize.shrinkWrap,
                                        activeColor: Color(0xffF36523),
                                        value: 'Daily',
                                        groupValue: _radioValue,
                                        onChanged: (v) {
                                          setState(() {
                                            radioButtonChange(v);
                                          });
                                        },
                                      ),
                                      Text(
                                        'Daily',
                                        style: TextStyle(
                                            fontSize: appNormalTextSize),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  width: 5.0,
                                ),
                                Container(
                                  child: Column(
                                    children: [
                                      Radio(
                                        materialTapTargetSize:
                                            MaterialTapTargetSize.shrinkWrap,
                                        focusColor: Colors.orange,
                                        activeColor: Color(0xffF36523),
                                        value: 'Weekly',
                                        groupValue: _radioValue,
                                        onChanged: (v) {
                                          setState(() {
                                            radioButtonChange(v);
                                          });
                                        },
                                      ),
                                      Text(
                                        'Weekly',
                                        style: TextStyle(
                                            fontSize: appNormalTextSize),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  width: 5.0,
                                ),
                                Container(
                                  child: Column(
                                    children: [
                                      Radio(
                                        materialTapTargetSize:
                                            MaterialTapTargetSize.shrinkWrap,
                                        focusColor: Colors.orange,
                                        activeColor: Color(0xffF36523),
                                        value: 'Monthly',
                                        groupValue: _radioValue,
                                        onChanged: (v) {
                                          setState(() {
                                            radioButtonChange(v);
                                          });
                                        },
                                      ),
                                      Text(
                                        'Monthly',
                                        style: TextStyle(
                                            fontSize: appNormalTextSize),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ])),
                    ],
                  ),
                ),
              ],
            ),
            Column(
              children: [
                Visibility(
                  visible: widget.checker,
                  child: Container(
                    height: 6.2 * SizeConfig.heightMultiplier,
                    width: 32 * SizeConfig.widthMultiplier,
                    child: AppButton(
                      onPressed: () {
                        setState(() {
                          if (_radioValue == null) {
                            validatein = true;
                          } else {
                            Navigator.pop(context, widget.product);

                            var productdata = ProductPost(
                                productID: 12,
                                quantity: quantityController.text,
                                farmerId: 1,
                                deliveryType: _radioValue);
                            widget.getlist.add(productdata);
                            // print('${widget.getlist} bk am here');
                          }
                        });
                      },
                      name: 'Submit',
                    ),
                  ),
                ),
                Visibility(
                  visible: !widget.checker,
                  child: Container(
                    height: 6.2 * SizeConfig.heightMultiplier,
                    width: 32 * SizeConfig.widthMultiplier,
                    child: AppButton(
                      color: Color(0xffF36523),
                      onPressed: () {
                        Navigator.pop(context, widget.product);
                        try {
                          if (widget.getlist != null) {
                            widget.getlist.removeAt(widget.indexin);
                          }
                        } catch (Exception) {}

                        print(widget.getlist);
                      },
                      name: 'Delete',
                    ),
                  ),
                ),
                SizedBox(
                  height: 3.2 * SizeConfig.heightMultiplier,
                ),
                Container(
                  width: 26.8 * SizeConfig.widthMultiplier,
                  decoration: BoxDecoration(
                    border: Border.all(
                      width: 1 * SizeConfig.widthMultiplier,
                      color: Color(0xffDDDDDD),
                    ),
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
