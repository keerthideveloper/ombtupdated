import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:ombt/pageMap.dart';

class AppTextField extends StatefulWidget {
  TextEditingController controllerName;
  int maxLength;
  TextInputAction textInputAction;
  String hintName;
  String labelName;
  TextInputType keyboardType;
  FormFieldValidator validateFunction;
  bool readOnly = false;
  Function onChange;
  Icon prefixIcon;
  Icon suffixIcon;
  String prefixText;
  String suffixText;
  Color focusEnableColor;
  bool enableInteractiveSelection;
  AppTextField({
    this.readOnly,
    this.textInputAction,
    this.maxLength,
    this.controllerName,
    this.keyboardType,
    this.hintName,
    this.labelName,
    this.onChange,
    this.prefixIcon,
    this.suffixIcon,
    this.prefixText,
    this.suffixText,
    this.focusEnableColor,
    this.validateFunction,
    this.enableInteractiveSelection,
  });
  @override
  _AppTextFieldState createState() => _AppTextFieldState();
}

class _AppTextFieldState extends State<AppTextField> {
  @override
  Widget build(BuildContext context) {
    print(widget.hintName);
    return Container(
      child: TextFormField(
        controller: widget.controllerName,
        // enableInteractiveSelection: widget.enableInteractiveSelection != null
        //     ? widget.enableInteractiveSelection
        //     : false,
        validator: widget.validateFunction,
        onChanged: widget.onChange,
        maxLength: widget.maxLength != null ? widget.maxLength : null,
        textInputAction: widget.maxLength != null ? TextInputAction.done : null,
        readOnly: widget.readOnly != null ? widget.readOnly : false,
        style: TextStyle(fontSize: appSubHeadingFontSize, color: appTextColor),
        keyboardType: widget.keyboardType != null ? widget.keyboardType : null,
        // onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
        decoration: InputDecoration(
          prefixIcon: widget.prefixIcon,
          counterText: "",
          prefixText: widget.prefixText,
          suffixText: widget.suffixText,
          suffixIcon: widget.suffixIcon,
          hintText: widget.hintName,
          filled: true,
          fillColor: Colors.white,
          hintStyle: TextStyle(
            color: appTextColor,
            fontSize: appSubHeadingFontSize,
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: widget.focusEnableColor == null
                  ? Color(0xff58BC7A)
                  : widget.focusEnableColor,
              width: 2.0,
            ),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: widget.focusEnableColor == null
                  ? Color(0xff58BC7A).withOpacity(0.2)
                  : widget.focusEnableColor,
              width: 2.0,
            ),
          ),
          errorBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.redAccent,
              width: 1.0,
            ),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.redAccent,
              width: 1.0,
            ),
          ),
        ),
      ),
    );
  }
}
