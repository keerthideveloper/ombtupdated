import 'package:ombt/pageMap.dart';

class CommonBody extends StatefulWidget {
  Widget child;
  String appTitle;
  String appLogo;
  Function() onPressed;
  CommonBody(
      {Key key,
      @required this.appLogo,
      @required this.child,
      @required this.appTitle,
      this.onPressed})
      : super(key: key);
  @override
  _CommonBodyState createState() => _CommonBodyState();
}

class _CommonBodyState extends State<CommonBody> {
  @override
  Widget build(BuildContext context) {
    print('title::: ${widget.appTitle}');
    print('test::: ${widget.appLogo}');
    return Scaffold(
      appBar: commonAppbar(
          title: widget.appTitle,
          onPressed: widget.onPressed,
          context: context),
      body: SingleChildScrollView(
        child: Container(
          // height: 87.3 * SizeConfig.heightMultiplier,
          height: 92.3 * SizeConfig.heightMultiplier,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/logos/ekkam-logo-small.png'),
            ),
          ),
          child: Column(
            children: [
              SizedBox(
                height: 2 * SizeConfig.heightMultiplier,
              ),
              // Text('${35.6 * SizeConfig.heightMultiplier}'),
              Container(
                padding: EdgeInsets.only(right: 29.0),
                child: Align(
                  alignment: Alignment.topRight,
                  child: widget.appLogo == null
                      ? Image.asset(
                          'assets/logos/ekkam@2x.png',
                          height: 13 * SizeConfig.imageSizeMultiplier,
                        )
                      : Image.asset(
                          'assets/logos/${widget.appLogo}',
                          height: 13 * SizeConfig.imageSizeMultiplier,
                        ),
                ),
              ),
              SizedBox(
                height: 2 * SizeConfig.heightMultiplier,
              ),
              Container(
                // color: Colors.lightGreen,
                height: 100 * SizeConfig.heightMultiplier -
                    (32 * SizeConfig.heightMultiplier),
                margin: const EdgeInsets.symmetric(horizontal: 25.0),
                child: widget.child,
              ),
              Expanded(
                child: Footer(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
