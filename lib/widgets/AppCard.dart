import 'package:ombt/pageMap.dart';

class AppCard extends StatefulWidget {
  Color color;
  double height;
  double width;
  Widget child;
  Function onTap;
  double elevation;

  AppCard(
      {this.color,
      this.height,
      this.width,
      this.child,
      this.onTap,
      this.elevation});
  @override
  _AppCardState createState() => _AppCardState();
}

class _AppCardState extends State<AppCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: GestureDetector(
        onTap: widget.onTap,
        child: Card(
          color: widget.color,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
            side: BorderSide(
                color: Color(0xff58BC7A).withOpacity(0.2), width: 1.0),
          ),
          elevation: widget.elevation != null ? widget.elevation : 3.0,
          shadowColor: Color(0xff62c483),
          child: Container(
            height: widget.height,
            width: widget.width,
            child: widget.child,
          ),
        ),
      ),
    );
  }
}
