import 'package:ombt/pageMap.dart';
import 'package:path_provider/path_provider.dart' as syspaths;

class UploadOptionDialog extends StatefulWidget {
  @override
  _UploadOptionDialogState createState() => _UploadOptionDialogState();
}

class _UploadOptionDialogState extends State<UploadOptionDialog> {
  double content = 1.8;
  File galleryImage;
  File cameraImage;

  List<dynamic> uploadedImages = List<dynamic>();
  Future getImageFromCamera() async {
    cameraImage = await ImagePicker.pickImage(source: ImageSource.camera);

    if (cameraImage != null) {
      final appDir = await syspaths.getApplicationDocumentsDirectory();
      final fileName = '${DateTime.now().millisecondsSinceEpoch}.jpg';
      final savedImage = await cameraImage.copy('${appDir.path}/$fileName');
      setState(() {
        uploadedImages.add(savedImage);
      });
    }
  }

  Future getImageFromGallery() async {
    galleryImage = await ImagePicker.pickImage(source: ImageSource.gallery);
    print('galleryImage::${galleryImage}');

    if (galleryImage != null) {
      setState(() {
        uploadedImages.add(galleryImage);
      });
    }
  }

  void imageSubmit(data) {
    print('uploadedImages- ${uploadedImages}');
    if (data.length > 0) {
      Navigator.pop(context, data);
    } else {
      Navigator.pop(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: AlertDialog(
        insetPadding: const EdgeInsets.only(
          bottom: 4.0,
          left: 0,
          right: 0,
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30.0),
        ),
        content: Container(
          height: 98.5 * SizeConfig.heightMultiplier,
          width: 100 * SizeConfig.widthMultiplier,
          child: Column(
            children: [
              SizedBox(height: 4.4 * SizeConfig.heightMultiplier),
              Container(
                height: 52 * SizeConfig.heightMultiplier,
                width: 80 * SizeConfig.widthMultiplier,
                child: GridView.count(
                  crossAxisCount: 2,
                  crossAxisSpacing: 8.0,
                  mainAxisSpacing: 8.0,
                  childAspectRatio: (130.0 / 75.0),
                  children: List.generate(
                    uploadedImages.length,
                    (index) => Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                            image: FileImage(uploadedImages[index]),
                            fit: BoxFit.fill),
                        borderRadius: BorderRadius.circular(8.0),
                        border: Border.all(
                          color: Color(0xff58BC7A).withOpacity(0.3),
                          width: 2.0,
                        ),
                      ),
                      child: Align(
                        alignment: Alignment.topRight,
                        child: Padding(
                          padding: const EdgeInsets.all(3.0),
                          child: InkWell(
                            onTap: () {
                              setState(() {
                                print(index);
                                uploadedImages.removeAt(index);
                              });
                            },
                            child: Icon(
                              Icons.cancel,
                              color: Color(0xffF36523),
                              size: 17.0,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 2.3 * SizeConfig.heightMultiplier,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Flexible(
                    child: Container(
                      child: GestureDetector(
                        onTap: () {
                          getImageFromCamera();
                        },
                        child: Container(
                          height: 12.2 * SizeConfig.heightMultiplier,
                          width: 41.2 * SizeConfig.widthMultiplier,
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: Color(0xff62c483).withOpacity(0.2),
                              width: 2.0,
                            ),
                          ),
                          child: Center(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Image.asset(
                                  'assets/icons/camera.png',
                                  height: 6 * SizeConfig.imageSizeMultiplier,
                                ),
                                Text(
                                  'Take Photo',
                                  style: TextStyle(
                                      fontSize: 1.8 * SizeConfig.textMultiplier,
                                      color: appTextColor),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 3.2 * SizeConfig.widthMultiplier,
                  ),
                  Flexible(
                    child: Container(
                      child: GestureDetector(
                        onTap: () {
                          getImageFromGallery();
                        },
                        child: Container(
                          height: 12.2 * SizeConfig.heightMultiplier,
                          width: 41.2 * SizeConfig.widthMultiplier,
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: Color(0xff62c483).withOpacity(0.2),
                              width: 2.0,
                            ),
                          ),
                          child: Center(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Image.asset(
                                  'assets/icons/photo.png',
                                  height: 6 * SizeConfig.imageSizeMultiplier,
                                ),
                                Text(
                                  'Gallery',
                                  style: TextStyle(
                                      fontSize: 1.8 * SizeConfig.textMultiplier,
                                      color: appTextColor),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 2.3 * SizeConfig.heightMultiplier,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Flexible(
                    child: Container(
                      height: 50.0,
                      width: double.infinity,
                      child: AppButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        name: 'Cancel',
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 3.2 * SizeConfig.widthMultiplier,
                  ),
                  Flexible(
                    child: Container(
                      height: 50.0,
                      width: double.infinity,
                      child: AppButton(
                        onPressed: () {
                          imageSubmit(uploadedImages);
                        },
                        name: 'Submit',
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 4.4 * SizeConfig.heightMultiplier),
              Container(
                width: 21.4 * SizeConfig.widthMultiplier,
                decoration: BoxDecoration(
                  border: Border.all(
                      color: Color(0xffDDDDDD),
                      width: 1 * SizeConfig.widthMultiplier),
                  borderRadius: BorderRadius.circular(10.0),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
