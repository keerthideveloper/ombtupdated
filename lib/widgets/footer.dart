import 'package:ombt/pageMap.dart';

class Footer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: FractionalOffset.bottomCenter,
      child: Container(
        child: Image.asset(
          'assets/logos/farm.png',
          fit: BoxFit.fill,
          // height: 17.4 * SizeConfig.imageSizeMultiplier,
          width: MediaQuery.of(context).size.width,
        ),
      ),
    );
  }
}
