import 'package:ombt/modules/product_list.dart';
import 'package:ombt/pageMap.dart';

class ProductListDialog extends StatefulWidget {
  @override
  _ProductListDialogState createState() => _ProductListDialogState();
}

class _ProductListDialogState extends State<ProductListDialog> {
  String _radioValue;
  var selected;

  double titleFontSize = 2.5;
  double subTitleFontSize = 2;
  double content = 1.6;

  TextEditingController quantityController = TextEditingController();
  void radioButtonChange(String value) {
    setState(() {
      print(value);
      _radioValue = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    final landscape =
        MediaQuery.of(context).orientation == Orientation.landscape;
    return AlertDialog(
        insetPadding: const EdgeInsets.all(0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30.0),
        ),
        title: Text(
          'Product List',
          style: TextStyle(fontSize: appHeadingFontSize),
        ),
        content: Container(
          height: 64.7 * SizeConfig.heightMultiplier,
          width: 100 * SizeConfig.widthMultiplier,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                children: [
                  Container(
                    padding: EdgeInsets.only(bottom: 10.0),
                    decoration: BoxDecoration(
                      border: Border(
                        bottom:
                            BorderSide(color: Color(0xffF9C93A), width: 2.0),
                      ),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Product',
                          style: TextStyle(fontSize: appSubHeadingFontSize),
                        ),
                        Text(
                          'Quantity',
                          style: TextStyle(fontSize: appSubHeadingFontSize),
                        ),
                        Text(
                          'Daily/weekly/monthly',
                          style: TextStyle(fontSize: appSubHeadingFontSize),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 3.4 * SizeConfig.heightMultiplier,
                  ),
                  Container(
                    height: landscape
                        ? MediaQuery.of(context).size.height - 214.0
                        : 48.6 * SizeConfig.heightMultiplier,
                    width: MediaQuery.of(context).size.width,
                    child: ListView.builder(
                        itemCount: selectedProductList.length,
                        itemBuilder: (BuildContext context, int index) {
                          Product _selectedProductList =
                              selectedProductList[index];
                          return Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                width: 18.0 * SizeConfig.widthMultiplier,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Image.asset(
                                      _selectedProductList.productImg,
                                      height:
                                          6.2 * SizeConfig.imageSizeMultiplier,
                                    ),
                                    SizedBox(
                                      height:
                                          content * SizeConfig.heightMultiplier,
                                    ),
                                    Text(
                                      _selectedProductList.productName,
                                      style: TextStyle(
                                          fontSize: appNormalTextSize),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                padding: const EdgeInsets.only(top: 10.0),
                                height: 7.3 * SizeConfig.heightMultiplier,
                                width: 19 * SizeConfig.widthMultiplier,
                                child: AppTextField(
                                  controllerName: quantityController,
                                  labelName: 'Qty',
                                ),
                              ),
                              Container(
                                width: 45.0 * SizeConfig.widthMultiplier,
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Container(
                                      child: Column(
                                        children: [
                                          Radio(
                                            focusColor: Colors.orange,
                                            materialTapTargetSize:
                                                MaterialTapTargetSize
                                                    .shrinkWrap,
                                            activeColor: Color(0xffF36523),
                                            value: 'Daily',
                                            groupValue: _radioValue,
                                            onChanged: (v) {
                                              setState(() {
                                                radioButtonChange(v);
                                              });
                                            },
                                          ),
                                          Text(
                                            'Daily',
                                            style: TextStyle(
                                                fontSize: appNormalTextSize),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      child: Column(
                                        children: [
                                          Radio(
                                            materialTapTargetSize:
                                                MaterialTapTargetSize
                                                    .shrinkWrap,
                                            focusColor: Colors.orange,
                                            activeColor: Color(0xffF36523),
                                            value: index.toString(),
                                            groupValue: 'Weekly',
                                            onChanged: (v) {
                                              setState(() {
                                                radioButtonChange(v);
                                              });
                                            },
                                          ),
                                          Text(
                                            'Weekly',
                                            style: TextStyle(
                                                fontSize: appNormalTextSize),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      child: Column(
                                        children: [
                                          Radio(
                                            materialTapTargetSize:
                                                MaterialTapTargetSize
                                                    .shrinkWrap,
                                            focusColor: Colors.orange,
                                            activeColor: Color(0xffF36523),
                                            value: 'Monthly',
                                            groupValue: _radioValue,
                                            onChanged: (v) {
                                              setState(() {
                                                radioButtonChange(v);
                                              });
                                            },
                                          ),
                                          Text(
                                            'Monthly',
                                            style: TextStyle(
                                                fontSize: appNormalTextSize),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          );
                        }),
                  ),
                ],
              ),
              Visibility(
                visible: true,
                child: Center(
                  child: Container(
                    width: 42.7 * SizeConfig.widthMultiplier,
                    decoration: BoxDecoration(
                      border: Border.all(
                        width: 1 * SizeConfig.widthMultiplier,
                        color: Color(0xffDDDDDD),
                      ),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
