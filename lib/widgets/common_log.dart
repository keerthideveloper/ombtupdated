import 'package:flutter/material.dart';

// ignore: must_be_immutable
class Commonlog extends StatefulWidget {
  Widget child;
  Commonlog({Key key, @required this.child}) : super(key: key);
  @override
  _CommonlogState createState() => _CommonlogState();
}

class _CommonlogState extends State<Commonlog> {
  @override
  Widget build(BuildContext context) {
    var heightin = MediaQuery.of(context).size.height;
    var widthin = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Container(
        height: heightin,
        width: widthin,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/logo/ekkam-logo.png'),
                fit: BoxFit.cover)),
        child: Stack(
          children: [
            // Positioned(
            //     top: heightin / 8,
            //     right: widthin / 4,
            //     left: widthin / 4,
            //     child: Image.asset('assets/logo/ekkam.png')),
            Positioned(
                left: 0.0,
                right: 0.0,
                bottom: 0.0,
                child: Image.asset('assets/images/farm.png')),
            widget.child
          ],
          overflow: Overflow.clip,
        ),
      ),
    );
  }
}
