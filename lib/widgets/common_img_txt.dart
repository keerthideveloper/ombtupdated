import 'package:flutter/material.dart';

// ignore: must_be_immutable
class ImageText extends StatelessWidget {
  String image;
  String text;
  ImageText(Key key, this.image, this.text) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(width: 30, height: 30, child: Image.asset(image)),
        Container(
          width: 70,
          height: 20,
          child: Text(text),
          alignment: Alignment.center,
        )
      ],
    );
  }
}
