import 'package:flutter/material.dart';

class CustomSwitch extends StatefulWidget {
  final bool value;
  final ValueChanged<bool> onChanged;
  final double switchwidth;
  final String leftyes;
  final String rightno;
  final Color swhcolorbodyes;
  final Color swhcolorbodno;
  final Color swhcoloritemyes;
  final Color swhColoritemno;

  CustomSwitch(
      {Key key,
      this.value,
      this.onChanged,
      this.switchwidth = 90.0,
      this.leftyes = '',
      this.rightno = '',
      this.swhcolorbodyes = const Color.fromRGBO(0, 149, 59, 1),
      this.swhcolorbodno = Colors.grey,
      this.swhcoloritemyes = const Color.fromRGBO(88, 188, 122, 1),
      this.swhColoritemno = const Color.fromRGBO(189, 189, 189, 1)})
      : super(key: key);

  @override
  _CustomSwitchState createState() => _CustomSwitchState();
}

class _CustomSwitchState extends State<CustomSwitch>
    with SingleTickerProviderStateMixin {
  Animation _circleAnimation;
  AnimationController _animationController;

  @override
  void initState() {
    super.initState();
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 60));
    _circleAnimation = AlignmentTween(
            begin: widget.value ? Alignment.centerRight : Alignment.centerLeft,
            end: widget.value ? Alignment.centerLeft : Alignment.centerRight)
        .animate(CurvedAnimation(
            parent: _animationController, curve: Curves.linear));
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _animationController,
      builder: (context, child) {
        return GestureDetector(
          onTap: () {
            if (_animationController.isCompleted) {
              _animationController.reverse();
            } else {
              _animationController.forward();
            }
            widget.value == false
                ? widget.onChanged(true)
                : widget.onChanged(false);
          },
          child: Stack(
            children: [
              Container(
                width: widget.switchwidth,
                height: 28.0,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(2.0),
                  color: _circleAnimation.value == Alignment.centerLeft
                      ? widget.swhcolorbodyes
                      : widget.swhcolorbodno,
                ),
                child: Padding(
                  padding: EdgeInsets.only(
                      top: 1.0, bottom: 1.0, right: 1.0, left: 1.0),
                  child: Container(
                    alignment: widget.value
                        ? Alignment.centerLeft
                        : Alignment.centerRight,
                    child: Container(
                      width: 20.0,
                      height: 28.0,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(2.0),
                        shape: BoxShape.rectangle,
                        color: _circleAnimation.value == Alignment.centerLeft
                            ? widget.swhcoloritemyes
                            : widget.swhColoritemno,
                      ),
                    ),
                  ),
                ),
              ),
              Positioned(
                top: widget.value ? 7 : 7,
                bottom: 0,
                left: widget.value ? 30 : 8,
                right: 0,
                child: widget.value
                    ? Text(
                        widget.rightno,
                        style: TextStyle(color: Colors.white),
                      )
                    :
                Text(
                        widget.leftyes,
                        style: TextStyle(color: Colors.white),
                      ),
              ),
            ],
          ),
        );
      },
    );
  }
}
