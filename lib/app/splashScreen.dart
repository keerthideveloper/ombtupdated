import 'package:ombt/pageMap.dart';

class OMBTSplashScreen extends StatefulWidget {
  @override
  _OMBTSplashScreenState createState() => _OMBTSplashScreenState();
}

class _OMBTSplashScreenState extends State<OMBTSplashScreen> {
  @override
  void initState() {
    Timer(
        Duration(seconds: 4),
        () => Navigator.push(
            context, MaterialPageRoute(builder: (context) => OtpSend())));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
            image: AssetImage('assets/images/splash.png'), fit: BoxFit.fill),
      ),
    );
  }
}
