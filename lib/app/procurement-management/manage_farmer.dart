import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:ombt/pageMap.dart';

class ManageFarmer extends StatefulWidget {
  @override
  _ManageFarmerState createState() => _ManageFarmerState();
}

class _ManageFarmerState extends State<ManageFarmer> {
  String title = "Manage Farmer";
  String currentDate;
  @override
  void initState() {
    var now = DateTime.now();
    currentDate = DateFormat('d/MMMM/y').format(now);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CommonBody(
        appTitle: 'Manage Farmer',
        onPressed: () => Navigator.pop(context),
        child: Container(
          height: 65 * SizeConfig.heightMultiplier,
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.only(bottom: 15.0),
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      color: Color(0xff58BC7A),
                    ),
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: Row(
                        children: [
                          Image.asset(
                            'assets/icons/user(2).png',
                          ),
                          SizedBox(
                            width: 10.0,
                          ),
                          Text(
                            'Farmer List',
                            style: TextStyle(
                              fontSize: appSubHeadingFontSize,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      child: Row(
                        children: [
                          Icon(
                            Icons.calendar_today,
                            color: Color(0xff58BC7A),
                            size: 4.5 * SizeConfig.widthMultiplier,
                          ),
                          SizedBox(
                            width: 10.0,
                          ),
                          Text(
                            currentDate,
                            style: TextStyle(
                              fontSize: appSubHeadingFontSize,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 2.2 * SizeConfig.heightMultiplier,
              ),
              Expanded(
                child: ListView.builder(
                    itemCount: productData.length,
                    itemBuilder: (BuildContext context, int index) {
                      Product _product = productData[index];
                      return GestureDetector(
                        onTap: () {},
                        child: AppCard(
                          child: Container(
                            height: 10.5 * SizeConfig.heightMultiplier,
                            padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.22,
                                  child: Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Image.asset(_product.productImg),
                                      Text(
                                        _product.productName,
                                        style: TextStyle(
                                            fontSize: appNormalTextSize),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  height: 30.0,
                                  decoration: BoxDecoration(
                                    border: Border(
                                      right: BorderSide(
                                        color:
                                            Color(0xff58BC7A).withOpacity(0.2),
                                        width: 2.0,
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 10.0,
                                ),
                                Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.32,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        'Sonu Melava',
                                        style: TextStyle(
                                          fontSize: appSubHeadingFontSize,
                                          color: Color(0xff8c8c8c),
                                        ),
                                      ),
                                      Text(
                                        'Tolerance-5%',
                                        style: TextStyle(
                                            color: Color(0xff8c8c8c),
                                            fontSize: appSubHeadingFontSize),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  height: 4.5 * SizeConfig.heightMultiplier,
                                  decoration: BoxDecoration(
                                    border: Border(
                                      right: BorderSide(
                                        color:
                                            Color(0xff58BC7A).withOpacity(0.2),
                                        width: 2.0,
                                      ),
                                    ),
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.only(
                                    left: 06.0,
                                  ),
                                  width:
                                      MediaQuery.of(context).size.width * 0.15,
                                  child: Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Text(
                                        _product.productQty,
                                        style: TextStyle(
                                            fontSize: appSubHeadingFontSize),
                                      ),
                                      Text(
                                        '\u20B9 100',
                                        style: TextStyle(
                                            fontSize: appSubHeadingFontSize),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  height: 4.5 * SizeConfig.heightMultiplier,
                                  decoration: BoxDecoration(
                                    border: Border(
                                      right: BorderSide(
                                        color:
                                            Color(0xff58BC7A).withOpacity(0.2),
                                        width: 2.0,
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: InkWell(
                                    child: Container(
                                      padding: EdgeInsets.only(top: 10.0),
                                      child:
                                          Image.asset('assets/icons/phone.png'),
                                    ),
                                    onTap: () {},
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      );
                    }),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
