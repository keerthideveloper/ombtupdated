import 'package:ombt/pageMap.dart';

class Otpvalidate extends StatefulWidget {
  @override
  _OtpvalidateState createState() => _OtpvalidateState();
}

class _OtpvalidateState extends State<Otpvalidate> {
  @override
  Widget build(BuildContext context) {
    return CommonBody(
      appTitle: 'Validate Farmer OTP',
      onPressed: () => Navigator.pop(context),
      child: Column(
        children: <Widget>[
          Container(
            height: 7.5 * SizeConfig.heightMultiplier,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Sonnu Melava",
                      style: TextStyle(fontSize: appHeadingFontSize),
                    ),
                    SizedBox(
                      height: SizeConfig.heightMultiplier * 1,
                    ),
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.location_on,
                          color: Colors.yellow,
                          size: 5 * SizeConfig.imageSizeMultiplier,
                        ),
                        SizedBox(
                          width: SizeConfig.widthMultiplier * 1,
                        ),
                        Text(
                          "Vimannagar",
                          style: TextStyle(
                            fontSize: appSubHeadingFontSize,
                            color: Colors.yellow,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                Container(
                  child: CircleAvatar(
                    radius: 30,
                    backgroundColor: Color.fromRGBO(242, 242, 242, 1),
                    child: Container(
                      width: 10 * SizeConfig.widthMultiplier,
                      height: 4 * SizeConfig.heightMultiplier,
                      child: Image.asset('assets/icons/user.png'),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: SizeConfig.heightMultiplier * 10,
          ),
          Container(
            height: 7 * SizeConfig.heightMultiplier,
            padding: EdgeInsets.only(
              left: 20.0,
              right: 20.0,
              bottom: 10.0,
            ),
            decoration: BoxDecoration(
              border: Border.all(
                color: appPrimaryColor,
                width: 2.0,
              ),
            ),
            child: PinFieldAutoFill(
              keyboardType: TextInputType.phone,
              decoration: UnderlineDecoration(
                  colorBuilder:
                      FixedColorBuilder(Color(0xff62c483).withOpacity(0.8))),
              // decoration: UnderlineDecoration(
              //   color: Color(0xff62c483),
              // ),
              codeLength: 4,
              onCodeChanged: (v) {
                print(v);
              },
            ),
          ),
          SizedBox(
            height: SizeConfig.heightMultiplier * 3,
          ),
          GestureDetector(
            child: Text(
              "OR",
              style: TextStyle(fontSize: appSubHeadingFontSize),
            ),
            onTap: () => Navigator.push(context,
                MaterialPageRoute(builder: (context) => Selectfruit())),
          ),
          SizedBox(
            height: SizeConfig.heightMultiplier * 3,
          ),
          Container(
            height: 7.5 * SizeConfig.heightMultiplier,
            child: AppTextField(
              focusEnableColor: Color.fromRGBO(109, 195, 135, 1),
              hintName: "Search Farmer by Name/Id",
              prefixIcon: Icon(
                Icons.search,
                color: Color.fromRGBO(109, 195, 135, 1),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
