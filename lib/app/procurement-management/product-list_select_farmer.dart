import 'package:intl/intl.dart';
import 'package:ombt/pageMap.dart';

class ProductListFromSelectFarmer extends StatefulWidget {
  @override
  _ProductListFromSelectFarmerState createState() =>
      _ProductListFromSelectFarmerState();
}

class _ProductListFromSelectFarmerState
    extends State<ProductListFromSelectFarmer> {
  String currentDate;
  @override
  void initState() {
    setState(() {
      final now = new DateTime.now();
      currentDate = DateFormat('d/MMMM/y').format(now);
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var portrait = MediaQuery.of(context).orientation == Orientation.portrait;
    var landscape = MediaQuery.of(context).orientation == Orientation.landscape;

    return Scaffold(
      body: CommonBody(
        appTitle: 'Product List To select Farmers',
        onPressed: () => Navigator.pop(context),
        child: Container(
          height: 65 * SizeConfig.heightMultiplier,
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.only(bottom: 15.0),
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      color: Color(0xff58BC7A),
                    ),
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: Row(
                        children: [
                          Icon(
                            Icons.calendar_today,
                            color: Color(0xff58BC7A),
                            size: 4 * SizeConfig.imageSizeMultiplier,
                          ),
                          SizedBox(
                            width: 10.0,
                          ),
                          Text(
                            currentDate,
                            style: TextStyle(fontSize: appSubHeadingFontSize),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      child: Row(
                        children: [
                          Icon(
                            Icons.location_on,
                            size: 20.0,
                            color: Color(0xff58BC7A),
                          ),
                          SizedBox(
                            width: 2.8 * SizeConfig.widthMultiplier,
                          ),
                          Text(
                            'Baner,Pune',
                            style: TextStyle(fontSize: appSubHeadingFontSize),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 2 * SizeConfig.heightMultiplier,
              ),
              Expanded(
                child: ListView.builder(
                    itemCount: productData.length,
                    itemBuilder: (BuildContext context, int index) {
                      Product _product = productData[index];
                      return AppCard(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => FarmerListForProduct(
                                        product: _product)));
                          },
                          child: ListTile(
                            title: Row(
                              children: [
                                Image.asset(_product.productImg),
                                SizedBox(
                                  width: 4 * SizeConfig.widthMultiplier,
                                ),
                                Text(
                                  _product.productName,
                                  style: TextStyle(fontSize: appNormalTextSize,color: appTextColor),
                                ),
                              ],
                            ),
                            trailing: Text(
                              _product.productQty,
                              style: TextStyle(fontSize: appNormalTextSize),
                            ),
                          ));
                    }),
              ),
            ],
          ),
        ),
      ),
    );
  }

}
