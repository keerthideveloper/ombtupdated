import 'package:intl/intl.dart';
import 'package:ombt/modules/farmer.dart';
import 'package:ombt/pageMap.dart';

class FarmerListForProduct extends StatefulWidget {
  var product;
  FarmerListForProduct({this.product});
  @override
  _FarmerListForProductState createState() => _FarmerListForProductState();
}

class _FarmerListForProductState extends State<FarmerListForProduct> {
  String currentDate;
  @override
  void initState() {
    var now = DateTime.now();
    currentDate = DateFormat('d/MMMM/y').format(now);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CommonBody(
        appTitle: 'Farmer List For Product',
        onPressed: () => Navigator.pop(context),
        child: Container(
          height: 65 * SizeConfig.heightMultiplier,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                padding: EdgeInsets.only(bottom: 15.0),
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      color: Color(0xff58BC7A),
                    ),
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: Row(
                        children: [
                          Icon(
                            Icons.calendar_today,
                            color: Color(0xff58BC7A),
                            size: 4 * SizeConfig.imageSizeMultiplier,
                          ),
                          SizedBox(
                            width: 10.0,
                          ),
                          Text(
                            currentDate,
                            style: TextStyle(fontSize: appSubHeadingFontSize),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      child: Row(
                        children: [
                          Icon(
                            Icons.location_on,
                            size: 20.0,
                            color: Color(0xff58BC7A),
                          ),
                          SizedBox(
                            width: 2.8 * SizeConfig.widthMultiplier,
                          ),
                          Text(
                            'Baner,Pune',
                            style: TextStyle(fontSize: appSubHeadingFontSize),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 2 * SizeConfig.heightMultiplier,
              ),
              AppCard(
                child: ListTile(
                  title: Row(
                    children: [
                      Image.asset(widget.product.productImg),
                      SizedBox(
                        width: 4 * SizeConfig.widthMultiplier,
                      ),
                      Text(
                        widget.product.productName,
                        style: TextStyle(
                            fontSize: appNormalTextSize, color: appTextColor),
                      ),
                    ],
                  ),
                  trailing: Text(widget.product.productQty),
                ),
              ),
              SizedBox(
                height: 5.0,
              ),
              Card(
                child: AppTextField(
                  prefixIcon: Icon(
                    Icons.search,
                    color: Color(0xff58BC7A),
                  ),
                  suffixIcon: Icon(
                    Icons.tune,
                    color: Colors.orange,
                  ),
                  focusEnableColor: Colors.orangeAccent,
                  hintName: 'Search',
                ),
              ),
              Expanded(
                child: ListView.builder(
                    itemCount: farmerList.length,
                    itemBuilder: (BuildContext context, int index) {
                      Farmer _farmerList = farmerList[index];
                      return GestureDetector(
                        onTap: () {},
                        child: ListTile(
                          title: Row(
                            children: [
                              CircleAvatar(
                                child: Image.asset(
                                  _farmerList.farmerImg,
                                ),
                                backgroundColor: Colors.grey[300],
                                radius: 20.0,
                              ),
                              SizedBox(
                                width: 15.0,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    _farmerList.farmerName,
                                    style: TextStyle(
                                        fontSize: appNormalTextSize,
                                        color: appTextColor),
                                  ),
                                  SizedBox(
                                    height: 1 * SizeConfig.heightMultiplier,
                                  ),
                                  Row(
                                    children: [
                                      Image.asset(
                                          'assets/icons/Place Icon.png'),
                                      SizedBox(
                                        width: 1 * SizeConfig.widthMultiplier,
                                      ),
                                      Text(
                                        _farmerList.farmerLocation,
                                        style: TextStyle(
                                            fontSize: appNormalTextSize,
                                            color: appTextColor),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ],
                          ),
                          trailing: Container(
                            height: 6.5 * SizeConfig.heightMultiplier,
                            width: 18 * SizeConfig.widthMultiplier,
                            child: AppTextField(
                              prefixText: '\u20B9',
                              focusEnableColor:
                                  Color(0xff58BC7A).withOpacity(0.3),
                            ),
                          ),
                        ),
                      );
                    }),
              ),
              AppButton(
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Otpvalidate()));
                },
                name: 'SEND FARMER LIST TO CPT',
              )
            ],
          ),
        ),
      ),
    );
  }
}
