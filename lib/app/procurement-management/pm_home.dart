import 'package:intl/intl.dart';
import 'package:ombt/pageMap.dart';

class ProcurementManagement extends StatefulWidget {
  @override
  _ProcurementManagementState createState() => _ProcurementManagementState();
}

class _ProcurementManagementState extends State<ProcurementManagement> {
  String selected = 'Procurement';

  String currentDate;

  @override
  void initState() {
    final now = new DateTime.now();
    currentDate = DateFormat('d/MMMM/y').format(now);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CommonBody(
        appTitle: 'DashBoard',
        onPressed: () => Navigator.pop(context),
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(bottom: 15.0),
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(
                    color: Color(0xff58BC7A),
                  ),
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Row(
                      children: [
                        Icon(
                          Icons.calendar_today,
                          color: Color(0xff58BC7A),
                          size: 4 * SizeConfig.imageSizeMultiplier,
                        ),
                        SizedBox(
                          width: 3 * SizeConfig.widthMultiplier,
                        ),
                        Text(
                          currentDate,
                          style: TextStyle(
                            fontSize: appSubHeadingFontSize,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    child: Row(
                      children: [
                        Icon(
                          Icons.location_on,
                          size: 5 * SizeConfig.imageSizeMultiplier,
                          color: Color(0xff58BC7A),
                        ),
                        SizedBox(
                          width: 2.8 * SizeConfig.widthMultiplier,
                        ),
                        Text(
                          'Baner,Pune',
                          style: TextStyle(
                            fontSize: appSubHeadingFontSize,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 8.1 * SizeConfig.heightMultiplier,
            ),
            Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AppCard(
                    onTap: () {
                      setState(() {
                        selected = 'Farmer Scouts';
                      });
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => FarmerScoutsHome()));
                    },
                    color:
                        selected == 'Farmer Scouts' ? Color(0xff58BC7A) : null,
                    height: 14.6 * SizeConfig.heightMultiplier,
                    width: 34.7 * SizeConfig.widthMultiplier,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          'assets/icons/farmers.png',
                          color:
                              selected == 'Farmer Scouts' ? Colors.white : null,
                        ),
                        SizedBox(
                          height: 1 * SizeConfig.heightMultiplier,
                        ),
                        Text(
                          'Farmer Scout',
                          style: TextStyle(
                            fontSize: appSubHeadingFontSize,
                            color: selected == 'Farmer Scouts'
                                ? Colors.white
                                : null,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 2.1 * SizeConfig.heightMultiplier,
                  ),
                  AppCard(
                    onTap: () {
                      setState(() {
                        selected = 'Procurement';
                      });
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => WelcomeEkkam(),
                        ),
                      );
                    },
                    color: selected == 'Procurement' ? Color(0xff58BC7A) :null,
                    height: 14.6 * SizeConfig.heightMultiplier,
                    width: 34.7 * SizeConfig.widthMultiplier,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          'assets/icons/user(2).png',
                          color: selected == 'Procurement'
                              ? Colors.white
                              : Colors.deepOrangeAccent[200],
                        ),
                        SizedBox(
                          height: 1 * SizeConfig.heightMultiplier,
                        ),
                        Text(
                          'Procurement\n    Manager',
                          style: TextStyle(
                              fontSize: appSubHeadingFontSize,
                              color: selected == 'Procurement'
                                  ? Colors.white
                                  :null),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 2.1 * SizeConfig.heightMultiplier,
                  ),
                  AppCard(
                    onTap: () {
                      setState(() {
                        selected = 'Quality';
                      });
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Indent()));
                    },
                    color: selected == 'Quality' ? Color(0xff58BC7A) : null,
                    height: 14.6 * SizeConfig.heightMultiplier,
                    width: 34.7 * SizeConfig.widthMultiplier,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          'assets/icons/worker.png',
                          color: selected == 'Quality' ? Colors.white : null,
                        ),
                        SizedBox(
                          height: 1 * SizeConfig.heightMultiplier,
                        ),
                        Text(
                          'Quality Manager',
                          style: TextStyle(
                            fontSize: appSubHeadingFontSize,
                            color: selected == 'Quality' ? Colors.white : null,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
