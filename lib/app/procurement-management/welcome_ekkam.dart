import 'package:ombt/pageMap.dart';

class WelcomeEkkam extends StatefulWidget {
  @override
  _WelcomeEkkamState createState() => _WelcomeEkkamState();
}

class _WelcomeEkkamState extends State<WelcomeEkkam> {
  String selected = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CommonBody(
        appTitle: 'Welcome to ekkam',
        onPressed: () => Navigator.pop(context),
        child: Container(
          height: 65 * SizeConfig.heightMultiplier,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              AppCard(
                color: selected == 'CPT Suggestion'
                    ? Color(0xff58BC7A)
                    : Colors.white,
                height: 14.6 * SizeConfig.heightMultiplier,
                width: 34.7 * SizeConfig.widthMultiplier,
                onTap: () {
                  setState(() {
                    selected = 'CPT Suggestion';
                  });
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ProductListFromSelectFarmer()),
                  );
                },
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      'assets/icons/support (1).png',
                      color: selected == 'CPT Suggestion' ? Colors.white : null,
                    ),
                    SizedBox(
                      height: 1 * SizeConfig.heightMultiplier,
                    ),
                    Text(
                      'CPT Suggestion',
                      style: TextStyle(
                        fontSize: appSubHeadingFontSize,
                        color:
                            selected == 'CPT Suggestion' ? Colors.white : null,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 5.5 * SizeConfig.widthMultiplier,
              ),
              AppCard(
                color: selected == 'Manage Farmer'
                    ? Color(0xff58BC7A)
                    : Colors.white,
                height: 14.6 * SizeConfig.heightMultiplier,
                width: 34.7 * SizeConfig.widthMultiplier,
                onTap: () {
                  setState(() {
                    selected = 'Manage Farmer';
                  });
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => ManageFarmer()),
                  );
                },
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      'assets/icons/turnover.png',
                      color: selected == 'Manage Farmer' ? Colors.white : null,
                    ),
                    SizedBox(
                      height: 1 * SizeConfig.heightMultiplier,
                    ),
                    Text(
                      'Manage Farmer',
                      style: TextStyle(
                        fontSize: appSubHeadingFontSize,
                        color:
                            selected == 'Manage Farmer' ? Colors.white : null,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
