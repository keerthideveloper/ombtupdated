import 'dart:convert';

import 'package:ombt/Api/FarmerGetApiData.dart';
import 'package:ombt/modules/FarmerGetDataModule.dart';
import 'package:ombt/pageMap.dart';

class FarmerDetails extends StatefulWidget {
  final int farmerid;
  FarmerDetails(this.farmerid);
  @override
  _FarmerDetailsState createState() => _FarmerDetailsState();
}

class _FarmerDetailsState extends State<FarmerDetails> {
  ApiFarmerDataGet farmerDataGet = ApiFarmerDataGet();
  String selected = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CommonBody(
        appTitle: "ekkam",
        onPressed: () => Navigator.pop(context),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            AppCard(
              color: selected == 'personal' ? Color(0xff58BC7A) : null,
              height: 14.6 * SizeConfig.heightMultiplier,
              width: 34.7 * SizeConfig.widthMultiplier,
              onTap: () {
                setState(() {
                  selected = 'personal';
                });
                farmerDataGet.getFarmerData(widget.farmerid).then((value) {
                  Map kill = jsonDecode(value);
                  var killer = FarmerGetData.fromJson(kill);
                  var queen = killer.response;
                  // print(queen.single.farmer.phoneNumber);
                  showDialog(
                      context: context,
                      child: Center(child: CircularProgressIndicator()));
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) => FarmerScoutsProfile(
                                idin: queen.single.farmer.farmerId,
                                fullnamein: queen.single.farmer.fullName,
                                villagein: queen.single.farmer.village,
                                farmAreain: queen.single.farmer.farmArea,
                                pinCodein: queen.single.farmer.pinCode,
                                mobilenoin: queen.single.farmer.phoneNumber,
                              )));
                });
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    'assets/icons/farmers.png',
                    height: 8 * SizeConfig.imageSizeMultiplier,
                    color: selected == 'personal' ? Colors.white : null,
                  ),
                  SizedBox(
                    height: 1 * SizeConfig.heightMultiplier,
                  ),
                  Text(
                    'Personal Details',
                    style: TextStyle(
                        fontSize: appSubHeadingFontSize,
                        color: selected == 'personal' ? Colors.white : null),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 2.5 * SizeConfig.heightMultiplier,
            ),
            AppCard(
              color: selected == 'bank' ? Color(0xff58BC7A) : null,
              height: 14.6 * SizeConfig.heightMultiplier,
              width: 34.7 * SizeConfig.widthMultiplier,
              onTap: () {
                setState(() {
                  selected = 'bank';
                });
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => FarmerBankDetail()));
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    'assets/icons/icon-money-wallet.png',
                    height: 8 * SizeConfig.imageSizeMultiplier,
                    color:
                        selected == 'bank' ? Colors.white : Color(0xffF36523),
                  ),
                  SizedBox(
                    height: 1 * SizeConfig.heightMultiplier,
                  ),
                  Text(
                    'Bank Details',
                    style: TextStyle(
                        fontSize: appSubHeadingFontSize,
                        color: selected == 'bank' ? Colors.white : null),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 2.5 * SizeConfig.heightMultiplier,
            ),
            AppCard(
              color: selected == 'product' ? Color(0xff58BC7A) : null,
              height: 14.6 * SizeConfig.heightMultiplier,
              width: 34.7 * SizeConfig.widthMultiplier,
              onTap: () {
                setState(() {
                  selected = 'product';
                });
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => ProductDetails()));
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    'assets/icons/diet.png',
                    height: 8 * SizeConfig.imageSizeMultiplier,
                    color: selected == 'product'
                        ? Colors.white
                        : Color(0xffF36523),
                  ),
                  SizedBox(
                    height: 1 * SizeConfig.heightMultiplier,
                  ),
                  Text(
                    'Product Details',
                    style: TextStyle(
                        fontSize: appSubHeadingFontSize,
                        color: selected == 'product' ? Colors.white : null),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 2.5 * SizeConfig.heightMultiplier,
            ),
          ],
        ),
      ),
    );
  }
}
