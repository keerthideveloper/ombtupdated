import 'package:fluttertoast/fluttertoast.dart';
import 'package:ombt/modules/Product_post_data.dart';
import 'package:ombt/modules/Product_view_List.dart';
import 'package:ombt/pageMap.dart';

class ProductDetails extends StatefulWidget {
  @override
  _ProductDetailsState createState() => _ProductDetailsState();
}

class _ProductDetailsState extends State<ProductDetails>
    with SingleTickerProviderStateMixin {
  List<ProductView> currentSelection = List<ProductView>();
  List<ProductView> productData = List<ProductView>();
  List<ProductPost> produceList = List<ProductPost>();
  GlobalKey<ScaffoldState> _scaffoldkey = GlobalKey<ScaffoldState>();

  Future<ProductView> futureproduct;
  AnimationController _controller;
  Apicallin productin = Apicallin();

  void initState() {
    _controller = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 1200));
    super.initState();
    futureproduct = productin.productview();
  }

  bool isLoading = false;
  bool knowing = false;
  @override
  Widget build(BuildContext context) {
    final landsCape =
        MediaQuery.of(context).orientation == Orientation.landscape;
    return Scaffold(
      key: _scaffoldkey,
      body: CommonBody(
        appTitle: "Product Details",
        onPressed: () => Navigator.pop(context),
        child: Stack(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextFormField(
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.white,
                    prefixIcon: Icon(
                      Icons.search,
                      color: Color(0xff58BC7A),
                    ),
                    suffixIcon: Icon(
                      Icons.tune,
                      color: Color(0xffF9C93A),
                    ),
                    hintText: 'Search',
                    hintStyle: TextStyle(
                      color: Color(0xff58BC7A),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Color(0xffF9C93A),
                        width: 2.0,
                      ),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Color(0xffF9C93A),
                        width: 2.0,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 1.5 * SizeConfig.heightMultiplier,
                ),
                Expanded(
                  child: FutureBuilder<ProductView>(
                    future: futureproduct,
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        if (knowing) {
                        } else {
                          final king =
                              productin.dataget.cast<Map<String, dynamic>>();
                          productData = ((king) as List)
                              .map((e) => ProductView.fromJson(e))
                              .toList();
                        }
                        knowing = true;
                        // print('${productData} king in');
                        return CustomScrollView(
                          slivers: <Widget>[
                            SliverToBoxAdapter(
                              child: Container(
                                padding: EdgeInsets.symmetric(vertical: 10.0),
                                child: Text(
                                  'Current Selections',
                                  style: TextStyle(
                                      fontSize: appSubHeadingFontSize),
                                ),
                              ),
                            ),
                            SliverToBoxAdapter(
                              child: Visibility(
                                visible: !(currentSelection.length > 0),
                                child: Container(
                                  child: Center(
                                    child: Text('Nill'),
                                  ),
                                ),
                              ),
                            ),
                            currentSelection.isNotEmpty
                                ? SliverGrid(
                                    gridDelegate:
                                        new SliverGridDelegateWithFixedCrossAxisCount(
                                            crossAxisCount: landsCape ? 4 : 3,
                                            childAspectRatio: (24.6 / 20.4),
                                            mainAxisSpacing: 10.0,
                                            crossAxisSpacing: 14.0),
                                    delegate: SliverChildBuilderDelegate(
                                      (context, index) {
                                        ProductView currentSelect =
                                            currentSelection[index];

                                        // print(currentSelect);
                                        return AppCard(
                                          child: Container(
                                            padding: EdgeInsets.all(7.0),
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceAround,
                                              children: [
                                                Align(
                                                  alignment: Alignment.topRight,
                                                  child: InkWell(
                                                    onTap: () async {
                                                      print(
                                                          '${currentSelect.selectProduct} remove');
                                                      var result =
                                                          await showDialog(
                                                        context: context,
                                                        child: ProductDialog(
                                                          indexin: index,
                                                          getlist: produceList,
                                                          product:
                                                              currentSelect,
                                                          checker: !currentSelect
                                                              .selectProduct,
                                                        ),
                                                      );
                                                      if (result != null) {
                                                        setState(() {
                                                          productData.add(
                                                              currentSelection[
                                                                  index]);

                                                          currentSelection
                                                              .removeAt(index);

                                                          currentSelect
                                                                  .selectProduct =
                                                              !currentSelect
                                                                  .selectProduct;
                                                        });
                                                      }
                                                    },
                                                    child: Icon(
                                                      Icons.cancel,
                                                      color: Colors.red,
                                                      size: 4 *
                                                          SizeConfig
                                                              .imageSizeMultiplier,
                                                    ),
                                                  ),
                                                ),
                                                Image.network(
                                                  'https://app.mycllc.ca/ombtbackend/images/${currentSelect.productImage}',
                                                  height: 4 *
                                                      SizeConfig
                                                          .heightMultiplier,
                                                ),
                                                Text(
                                                  currentSelect.productName,
                                                  style: TextStyle(
                                                    fontSize: appNormalTextSize,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        );
                                      },
                                      childCount: currentSelection.length,
                                    ),
                                  )
                                : SliverToBoxAdapter(
                                    child: Container(
                                      child: Center(
                                        child: Text(''),
                                      ),
                                    ),
                                  ),
                            SliverToBoxAdapter(
                              child: Container(
                                padding: EdgeInsets.symmetric(vertical: 10.0),
                                child: Text(
                                  'Product Selection',
                                  style: TextStyle(
                                      fontSize: appSubHeadingFontSize),
                                ),
                              ),
                            ),
                            productData.isNotEmpty
                                ? SliverGrid(
                                    gridDelegate:
                                        new SliverGridDelegateWithFixedCrossAxisCount(
                                            crossAxisCount: landsCape ? 4 : 3,
                                            childAspectRatio: (24.6 / 20.4),
                                            mainAxisSpacing: 10.0,
                                            crossAxisSpacing: 14.0),
                                    delegate: SliverChildBuilderDelegate(
                                        (context, index) {
                                      ProductView producelist =
                                          productData[index];

                                      return GestureDetector(
                                          onTap: () async {
                                            // print(
                                            //     '${producelist.selectProduct} add');
                                            var result = await showDialog(
                                              context: context,
                                              child: new ProductDialog(
                                                getlist: produceList,
                                                product: producelist,
                                                checker:
                                                    !producelist.selectProduct,
                                              ),
                                            );
                                            if (result != null) {
                                              setState(() {
                                                currentSelection
                                                    .add(productData[index]);
                                                productData.removeAt(index);
                                                producelist.selectProduct =
                                                    !producelist.selectProduct;
                                                // print(producelist.selectProduct);
                                              });
                                            }
                                          },
                                          child: AppCard(
                                            child: Container(
                                              padding: EdgeInsets.all(9.0),
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceAround,
                                                children: [
                                                  producelist.selectProduct
                                                      ? Align(
                                                          alignment: Alignment
                                                              .topRight,
                                                          child: Icon(
                                                            Icons.check_circle,
                                                            color: Colors.green,
                                                            size: 4 *
                                                                SizeConfig
                                                                    .imageSizeMultiplier,
                                                          ),
                                                        )
                                                      : Container(),
                                                  Image.network(
                                                    'https://app.mycllc.ca/ombtbackend/images/${producelist.productImage}',
                                                    height: 4 *
                                                        SizeConfig
                                                            .heightMultiplier,
                                                  ),
                                                  Text(
                                                    producelist.productName,
                                                    style: TextStyle(
                                                      fontSize:
                                                          appNormalTextSize,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ));
                                    }, childCount: productData.length),
                                  )
                                : SliverToBoxAdapter(
                                    child: Container(
                                      child: Center(
                                        child: Text(''),
                                      ),
                                    ),
                                  ),
                          ],
                        );
                      } else if (snapshot.hasError) {
                        return Text("${snapshot.error}");
                      }

                      // By default, show a loading spinner.
                      return Center(child: CircularProgressIndicator());
                    },
                  ),
                ),
                AbsorbPointer(
                  absorbing: isLoading,
                  child: Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.symmetric(vertical: 10.0),
                    child: AppButton(
                      name: 'Submit',
                      onPressed: () {
                        setState(() {
                          if (produceList.isEmpty) {
                            isLoading = false;
                            customSnackBar('Check the Current Selections');
                          } else {
                            isLoading = true;
                            productin.createProduce(produceList).then((value) {
                              if (value.statusCode == 200) {
                                currentSelection = [];
                                produceList = [];
                                customSnackBar('Farmer Produce Details Updated');
                                Future.delayed(Duration(seconds: 2),
                                    () => Navigator.pop(context));
                              } else {
                                customSnackBar('Check the Connection');
                              }
                            });
                          }
                        });
                      },
                    ),
                  ),
                ),
              ],
            ),
            Container(
              height: 70 * SizeConfig.heightMultiplier,
              width: 100 * SizeConfig.widthMultiplier,
              child: isLoading
                  ? Center(child: CircularProgressIndicator())
                  : Container(),
            )
          ],
        ),
      ),
    );
  }

  // void dispose() {
  //   super.dispose();
  //   _controller.dispose();
  // }

  // customSnackBar(data) {
  //   setState(() {
  //     isLoading = false;
  //   });
  //   final snackBarContent = SnackBar(
  //     // ignore: unnecessary_brace_in_string_interps
  //     content: Text("Farmer Scout ${data}"),
  //     action: SnackBarAction(
  //         label: 'UNDO',
  //         onPressed: _scaffoldkey.currentState.hideCurrentSnackBar),
  //   );
  //
  //   _scaffoldkey.currentState.showSnackBar(snackBarContent);
  // }

  void customSnackBar(value) {
    Fluttertoast.showToast(
        msg: value,
        backgroundColor: Colors.black,
        // fontSize: 25
        gravity: ToastGravity.TOP,
        textColor: Colors.white
    );

    // _scaffoldKey.currentState.showSnackBar(new SnackBar(
    //   content: new Text("Please Upload ${value} Image"),
    //   action: SnackBarAction(
    //       label: 'Undo',
    //       onPressed: _scaffoldKey.currentState.hideCurrentSnackBar),
    // ));
  }
}
