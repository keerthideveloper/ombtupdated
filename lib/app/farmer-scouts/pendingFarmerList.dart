import 'package:ombt/modules/farmerModule.dart';
import 'package:ombt/pageMap.dart';

class PendingFarmerList extends StatefulWidget {
  @override
  _PendingFarmerListState createState() => _PendingFarmerListState();
}

class _PendingFarmerListState extends State<PendingFarmerList> {
  FarmerApi _provider = FarmerApi();
  Future<List<FarmerInfo>> get getFarmerInfo =>
      _provider.getFarmerInfo('getdata');

  double titleFontSize = 2.6;
  double subTitleFontSize = 2.1;
  double content = 2;
  List<FarmerInfo> pendingFarmer = [];
  List<FarmerInfo> searchFarmer = [];
  String searchFiled = '';
  TextEditingController searchController = TextEditingController();

  void searchPendingFarmer(String searchFiled) {
    getFarmerInfo.then(
      (value) {
        searchFarmer = value
            .where((element) => (!(element.bankdetailsStatus == 'completed') &&
                (element.adharcardStatus == 'completed') &&
                (element.pandcardStatus == 'completed')))
            .where((element) => element.farmer.fullName
                .toString()
                .toLowerCase()
                .contains(searchFiled.toLowerCase()))
            .toList();
        print(searchFarmer);
      },
    );
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CommonBody(
        appLogo: 'ekkam.png',
        appTitle: 'Pending Farmer List',
        onPressed: () {
          Navigator.pop(context);
        },
        child: Container(
          child: Column(
            children: [
              AppTextField(
                prefixIcon: Icon(
                  Icons.search,
                  color: Color(0xff58BC7A),
                ),
                hintName: 'Search',
                suffixIcon: Icon(
                  Icons.tune,
                  color: Color(0xffF9C93A),
                ),
                controllerName: searchController,
                onChange: (v) {
                  setState(() {
                    searchFiled = v;
                    searchPendingFarmer(searchFiled);
                  });
                },
              ),
              SizedBox(
                height: 1.5 * SizeConfig.heightMultiplier,
              ),
              FutureBuilder(
                  future: getFarmerInfo,
                  builder: (BuildContext context,
                      AsyncSnapshot<List<FarmerInfo>> snapshot) {
                    print(snapshot.error);
                    switch (snapshot.connectionState) {
                      case ConnectionState.none:
                        return Container(
                          padding: const EdgeInsets.only(left: 25.0),
                          height: 17 * SizeConfig.heightMultiplier,
                          child: Center(
                            child: CircularProgressIndicator(),
                          ),
                        );
                        break;
                      case ConnectionState.waiting:
                        return Container(
                          padding: const EdgeInsets.only(left: 25.0),
                          height: 17 * SizeConfig.heightMultiplier,
                          child: Center(
                            child: CircularProgressIndicator(),
                          ),
                        );
                        break;

                      case ConnectionState.active:
                        return Container(
                          padding: const EdgeInsets.only(left: 25.0),
                          height: 17 * SizeConfig.heightMultiplier,
                          child: Center(
                            child: CircularProgressIndicator(),
                          ),
                        );
                        break;
                      case ConnectionState.done:
                        if (snapshot.hasError) {
                          return Container(
                            padding: const EdgeInsets.only(left: 25.0),
                            height: 17 * SizeConfig.heightMultiplier,
                            child: Center(
                              child: Text("Something Wrong"),
                            ),
                          );
                        } else {
                          var pendingFarmer = snapshot.data
                              .where((element) => (!(element
                                          .bankdetailsStatus ==
                                      'completed') &&
                                  (element.adharcardStatus == 'completed') &&
                                  (element.pandcardStatus == 'completed')))
                              .toList();

                          return Expanded(
                            child: searchFiled.isEmpty
                                ? ListView.builder(
                                    scrollDirection: Axis.vertical,
                                    itemCount: pendingFarmer.length,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      print(
                                          pendingFarmer[index].farmer.fullName);

                                      FarmerInfo _farmerData =
                                          pendingFarmer[index];
                                      print(
                                          'snapshot.data1::${_farmerData.farmer.farmerImage}');
                                      return Padding(
                                        padding:
                                            const EdgeInsets.only(bottom: 5.0),
                                        child: FarmerCard(
                                          farmerData: _farmerData,
                                        ),
                                      );
                                    })
                                : ListView.builder(
                                    scrollDirection: Axis.vertical,
                                    itemCount: searchFarmer.length,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      FarmerInfo _farmerData =
                                          searchFarmer[index];
                                      return Padding(
                                        padding:
                                            const EdgeInsets.only(bottom: 5.0),
                                        child: FarmerCard(
                                          farmerData: _farmerData,
                                        ),
                                      );
                                    }),
                          );
                        }
                        break;
                    }
                    return Container();
                  })
            ],
          ),
        ),
      ),
    );
  }
}

class FarmerCard extends StatelessWidget {
  const FarmerCard({
    Key key,
    @required FarmerInfo farmerData,
  })  : _farmerData = farmerData,
        super(key: key);

  final FarmerInfo _farmerData;

  @override
  Widget build(BuildContext context) {
    print(_farmerData);
    return Card(
      child: Container(
          width: 72 * SizeConfig.widthMultiplier,
          padding: EdgeInsets.only(top: 13.0, left: 14.0, right: 10.0),
          decoration: BoxDecoration(
            border: Border(
              left: BorderSide(color: Color(0xffF9C93A), width: 3.0),
            ),
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CircleAvatar(
                  radius: 3.3 * SizeConfig.heightMultiplier,
                  backgroundColor: Color(0xffEDEDED),
                  child:
                      // _farmerData.farmer.farmerImage!=null?Image.network(
                      //   'https://app.mycllc.ca/ombtbackend/images/${_farmerData.farmer.farmerImage}',
                      //   height: 5.7 *
                      //       SizeConfig
                      //           .imageSizeMultiplier,
                      // ):
                      Image.asset(
                    'assets/icons/user.png',
                    height: 5.7 * SizeConfig.imageSizeMultiplier,
                  )),
              SizedBox(
                width: 2.5 * SizeConfig.widthMultiplier,
              ),
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      _farmerData.farmer.fullName,
                      style: TextStyle(
                          fontSize: appHeadingFontSize, color: appTextColor),
                    ),
                    SizedBox(
                      height: 0.6 * SizeConfig.heightMultiplier,
                    ),
                    Container(
                      width: 63 * SizeConfig.widthMultiplier,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            child: Row(
                              children: [
                                _farmerData.adharcardStatus == 'completed'
                                    ? Icon(
                                        Icons.check_circle,
                                        color: Colors.lightGreen,
                                        size: 3.5 *
                                            SizeConfig.imageSizeMultiplier,
                                      )
                                    : Icon(
                                        Icons.cancel,
                                        color: Colors.red,
                                        size: 3.5 *
                                            SizeConfig.imageSizeMultiplier,
                                      ),
                                SizedBox(
                                  width: 1.5 * SizeConfig.widthMultiplier,
                                ),
                                Text(
                                  'Adhar Card',
                                  style: TextStyle(
                                    fontSize: appNormalTextSize,
                                    color: appTextColor,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          // SizedBox(
                          //   width: 10 * SizeConfig.widthMultiplier,
                          // ),
                          Container(
                            child: Row(
                              children: [
                                _farmerData.pandcardStatus == 'completed'
                                    ? Icon(
                                        Icons.check_circle,
                                        color: Colors.lightGreen,
                                        size: 3.5 *
                                            SizeConfig.imageSizeMultiplier,
                                      )
                                    : Icon(
                                        Icons.cancel,
                                        color: Colors.red,
                                        size: 3.5 *
                                            SizeConfig.imageSizeMultiplier,
                                      ),
                                SizedBox(
                                  width: 2 * SizeConfig.widthMultiplier,
                                ),
                                Text(
                                  'Pan Card',
                                  style: TextStyle(
                                    fontSize: appNormalTextSize,
                                    color: appTextColor,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 3 * SizeConfig.widthMultiplier,
                    ),
                    Container(
                      width: 63 * SizeConfig.widthMultiplier,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            child: Row(
                              children: [
                                _farmerData.bankdetailsStatus == 'completed'
                                    ? Icon(
                                        Icons.check_circle,
                                        color: Colors.lightGreen,
                                        size: 3.5 *
                                            SizeConfig.imageSizeMultiplier,
                                      )
                                    : Icon(
                                        Icons.cancel,
                                        color: Colors.red,
                                        size: 3.5 *
                                            SizeConfig.imageSizeMultiplier,
                                      ),
                                SizedBox(
                                  width: 2 * SizeConfig.widthMultiplier,
                                ),
                                Text(
                                  'Bank Details',
                                  style: TextStyle(
                                    fontSize: appNormalTextSize,
                                    color: appTextColor,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            child: Row(
                              children: [
                                Text(
                                  'Status : ',
                                  style: TextStyle(
                                    fontSize: appNormalTextSize,
                                    color: appTextColor,
                                  ),
                                ),
                                Text(
                                  'Pending',
                                  style: TextStyle(
                                    fontSize: appSubHeadingFontSize,
                                    color: Color(0xffF9C93A),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 3 * SizeConfig.widthMultiplier,
                    ),
                  ],
                ),
              )
            ],
          )),
    );
  }
}
