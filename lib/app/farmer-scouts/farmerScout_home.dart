import 'package:ombt/Api/FarmerVerification.dart';
import 'package:ombt/app/farmer-scouts/pendingFarmerList.dart';
import 'package:ombt/modules/farmerModule.dart';
import 'package:ombt/pageMap.dart';
import 'package:ombt/widgets/common_SnackBar.dart';
import 'package:ombt/widgets/farmerScout_homepop.dart';

class FarmerScoutsHome extends StatefulWidget {
  @override
  _FarmerScoutsHomeState createState() => _FarmerScoutsHomeState();
}

class _FarmerScoutsHomeState extends State<FarmerScoutsHome> {
  FarmerApi _provider = FarmerApi();
  Future<List<FarmerInfo>> get getFarmerInfo =>
      _provider.getFarmerInfo('getdata');
  TextEditingController farmerController = TextEditingController();
  GlobalKey<ScaffoldState> _scaffoldkey = new GlobalKey<ScaffoldState>();
  ApiFarmerVerify farmerVerify = ApiFarmerVerify();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldkey,
      appBar: commonAppbar(title: 'Welcome to ekkam', context: context,onPressed: () => Navigator.pop(context),),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: SingleChildScrollView(
          child: Container(
            height: 87.3 * SizeConfig.heightMultiplier,
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/logos/ekkam-logo-small.png'),
                  fit: BoxFit.none),
            ),
            child: Column(
              children: [
                SizedBox(
                  height: 2 * SizeConfig.heightMultiplier,
                ),
                // Text('${35.6 * SizeConfig.heightMultiplier}'),
                Container(
                  padding: EdgeInsets.only(right: 29.0),
                  child: Align(
                    alignment: Alignment.topRight,
                    child: Image.asset(
                      'assets/logos/ekkam.png',
                      height: 13 * SizeConfig.imageSizeMultiplier,
                    ),
                  ),
                ),
                Container(
                  height: 20,
                ),
                SizedBox(
                  height: 2 * SizeConfig.heightMultiplier,
                ),
                Container(
                  padding: EdgeInsets.only(
                      left: 28.0, right: 23.0, bottom: 25.5, top: 24.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Text(
                        'Farmer Phone Number',
                        style: TextStyle(
                          fontSize: appSubHeadingFontSize,
                        ),
                      ),
                      SizedBox(
                        height: 2 * SizeConfig.heightMultiplier,
                      ),
                      AppTextField(
                        maxLength: 10,
                        controllerName: farmerController,
                        keyboardType: TextInputType.phone,
                        hintName: '  Enter Mobile Number',
                        prefixText: '+91-',
                      ),
                      SizedBox(
                        height: 2.5 * SizeConfig.heightMultiplier,
                      ),
                      AppButton(
                        onPressed: () {
                          showDialog(
                              context: context,
                              child:
                                  Center(child: CircularProgressIndicator()));
                          farmerVerify
                              .createFarmerOTP(farmerController.text)
                              .then((value) => {
                                    if (value.body.contains('true'))
                                      {
                                        Navigator.of(context).pop(),

                                        FocusScope.of(context)
                                            .requestFocus(new FocusNode()),
                                        showDialog(context: context,
                                        child:  AlertDialog(
                                          contentPadding: EdgeInsets.all(15),
                                          content: Row(
                                            children: [
                                              CircleAvatar(
                                                radius: 3 *
                                                    SizeConfig
                                                        .heightMultiplier,
                                                backgroundColor:
                                                Color(0xff58BC7A),
                                                child: Icon(
                                                  Icons.done,
                                                  color: Colors.white,
                                                  size: 6 *
                                                      SizeConfig
                                                          .imageSizeMultiplier,
                                                ),
                                              ),
                                              SizedBox(
                                                width: 10,
                                              ),
                                              Text(
                                                  'OTP Send to Farmer '),
                                            ],
                                          )
                                          ,
                                        )),
                                        Future.delayed(
                                            Duration(seconds: 3),
                                            () => showDialogpop(
                                                context, farmerController.text))


                                        // FocusScope.of(context)
                                        //     .requestFocus(new FocusNode()),
                                        // showInSnackBar(
                                        //     _scaffoldkey, "OTP Send to Farmer"),
                                        // Future.delayed(
                                        //     Duration(seconds: 3),
                                        //     () => showDialogpop(
                                        //         context, farmerController.text))
                                      }
                                    // else
                                    //   {
                                    //     Navigator.of(context).pop(),
                                    //     FocusScope.of(context)
                                    //         .requestFocus(new FocusNode()),
                                    //     showInSnackBar(
                                    //         _scaffoldkey, "Invalid Number"),
                                    //   }
                          else {
                              Navigator.of(context).pop(),
                                      FocusScope.of(context)
                                              .requestFocus(new FocusNode()),

                          showDialog(
                              context: context,
                              child: AlertDialog(
                                  contentPadding: EdgeInsets.all(10),
                                  content: Row(
                                    children: [
                                      CircleAvatar(
                                        radius: 3 *
                                            SizeConfig
                                                .heightMultiplier,
                                        backgroundColor: Colors.red,
                                        child: Icon(
                                          Icons.error,
                                          color: Colors.white,
                                          size: 6 *
                                              SizeConfig
                                                  .imageSizeMultiplier,
                                        ),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Text('Invalid OTP ')
                                    ],
                                  ))),
                          Future.delayed(Duration(seconds: 3),
                                  () => Navigator.of(context).pop()),
                        }
                                  });
                        },
                        name: 'Verify Farmer',
                      ),
                      SizedBox(
                        height: 14 * SizeConfig.heightMultiplier,
                      ),
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Farmer Pending List',
                              style: TextStyle(
                                fontSize: appSubHeadingFontSize,
                              ),
                            ),
                            Container(
                              child: Row(
                                children: [
                                  InkWell(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  PendingFarmerList()));
                                    },
                                    child: Text(
                                      'See all',
                                      style: TextStyle(
                                        fontSize: appNormalTextSize,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 1.8 * SizeConfig.widthMultiplier,
                                  ),
                                  Icon(
                                    Icons.arrow_forward_ios,
                                    size: 14.0,
                                    color: appPrimaryColor,
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                FutureBuilder(
                    future: getFarmerInfo,
                    builder: (BuildContext context,
                        AsyncSnapshot<List<FarmerInfo>> snapshot) {
                      print(snapshot.error);
                      switch (snapshot.connectionState) {
                        case ConnectionState.none:
                          return Container(
                            padding: const EdgeInsets.only(left: 25.0),
                            height: 17 * SizeConfig.heightMultiplier,
                            child: Center(
                              child: CircularProgressIndicator(),
                            ),
                          );
                          break;
                        case ConnectionState.waiting:
                          return Container(
                            padding: const EdgeInsets.only(left: 25.0),
                            height: 17 * SizeConfig.heightMultiplier,
                            child: Center(
                              child: CircularProgressIndicator(),
                            ),
                          );
                          break;

                        case ConnectionState.active:
                          return Container(
                            padding: const EdgeInsets.only(left: 25.0),
                            height: 17 * SizeConfig.heightMultiplier,
                            child: Center(
                              child: CircularProgressIndicator(),
                            ),
                          );
                          break;
                        case ConnectionState.done:
                          if (snapshot.hasError) {
                            return Container(
                              padding: const EdgeInsets.only(left: 25.0),
                              height: 17 * SizeConfig.heightMultiplier,
                              child: Center(
                                child: Text("Something Wrong"),
                              ),
                            );
                          } else {
                            var pendingFarmer = snapshot.data
                                .where((element) => (!(element
                                            .bankdetailsStatus ==
                                        'completed') &&
                                    (element.adharcardStatus == 'completed') &&
                                    (element.pandcardStatus == 'completed')))
                                .toList();
                            return Container(
                              padding: const EdgeInsets.only(left: 25.0),
                              height: 17 * SizeConfig.heightMultiplier,
                              child: ListView.builder(
                                  scrollDirection: Axis.horizontal,
                                  itemCount: 3,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    print(pendingFarmer[index].farmer.fullName);

                                    FarmerInfo _farmerData =
                                        pendingFarmer[index];
                                    print(
                                        'snapshot.data1::${_farmerData.farmer.farmerImage}');
                                    return FarmerCard(farmerData: _farmerData);
                                  }),
                            );
                          }
                          break;
                      }
                      return Container();
                    }),
                Expanded(child: Footer()),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class FarmerCard extends StatelessWidget {
  const FarmerCard({
    Key key,
    @required FarmerInfo farmerData,
  })  : _farmerData = farmerData,
        super(key: key);

  final FarmerInfo _farmerData;

  @override
  Widget build(BuildContext context) {
    print(_farmerData);
    return Card(
      child: Container(
          width: 72 * SizeConfig.widthMultiplier,
          padding: EdgeInsets.only(top: 13.0, left: 14.0),
          decoration: BoxDecoration(
            border: Border(
              left: BorderSide(color: Color(0xffF9C93A), width: 3.0),
            ),
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CircleAvatar(
                  radius: 3.3 * SizeConfig.heightMultiplier,
                  backgroundColor: Color(0xffEDEDED),
                  child:
                      // _farmerData.farmer.farmerImage!=null?Image.network(
                      //   'https://app.mycllc.ca/ombtbackend/images/${_farmerData.farmer.farmerImage}',
                      //   height: 5.7 *
                      //       SizeConfig
                      //           .imageSizeMultiplier,
                      // ):
                      Image.asset(
                    'assets/icons/user.png',
                    height: 5.7 * SizeConfig.imageSizeMultiplier,
                  )),
              SizedBox(
                width: 2.5 * SizeConfig.widthMultiplier,
              ),
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      _farmerData.farmer.fullName,
                      style: TextStyle(
                          fontSize: appHeadingFontSize, color: appTextColor),
                    ),
                    SizedBox(
                      height: 0.6 * SizeConfig.heightMultiplier,
                    ),
                    Row(
                      children: [
                        Container(
                          child: Row(
                            children: [
                              _farmerData.adharcardStatus == 'completed'
                                  ? Icon(
                                      Icons.check_circle,
                                      color: Colors.lightGreen,
                                      size:
                                          3.3 * SizeConfig.imageSizeMultiplier,
                                    )
                                  : Icon(
                                      Icons.cancel,
                                      color: Colors.red,
                                      size:
                                          3.3 * SizeConfig.imageSizeMultiplier,
                                    ),
                              SizedBox(
                                width: 1.5 * SizeConfig.widthMultiplier,
                              ),
                              Text(
                                'Adhar Card',
                                style: TextStyle(
                                  fontSize: appNormalTextSize,
                                  color: appTextColor,
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          width: 1.5 * SizeConfig.widthMultiplier,
                        ),
                        Container(
                          child: Row(
                            children: [
                              _farmerData.pandcardStatus == 'completed'
                                  ? Icon(
                                      Icons.check_circle,
                                      color: Colors.lightGreen,
                                      size:
                                          3.3 * SizeConfig.imageSizeMultiplier,
                                    )
                                  : Icon(
                                      Icons.cancel,
                                      color: Colors.red,
                                      size:
                                          3.3 * SizeConfig.imageSizeMultiplier,
                                    ),
                              SizedBox(
                                width: 2 * SizeConfig.widthMultiplier,
                              ),
                              Text(
                                'Pan Card',
                                style: TextStyle(
                                  fontSize: appNormalTextSize,
                                  color: appTextColor,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 1.5 * SizeConfig.widthMultiplier,
                    ),
                    Container(
                      child: Row(
                        children: [
                          _farmerData.bankdetailsStatus == 'completed'
                              ? Icon(
                                  Icons.check_circle,
                                  color: Colors.lightGreen,
                                  size: 3.3 * SizeConfig.imageSizeMultiplier,
                                )
                              : Icon(
                                  Icons.cancel,
                                  color: Colors.red,
                                  size: 3.3 * SizeConfig.imageSizeMultiplier,
                                ),
                          SizedBox(
                            width: 2 * SizeConfig.widthMultiplier,
                          ),
                          Text(
                            'Bank Details',
                            style: TextStyle(
                              fontSize: appNormalTextSize,
                              color: appTextColor,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 1.5 * SizeConfig.heightMultiplier,
                    ),
                    Container(
                      child: Row(
                        children: [
                          Text(
                            'Status : ',
                            style: TextStyle(
                              fontSize: appNormalTextSize,
                              color: appTextColor,
                            ),
                          ),
                          Text(
                            'Pending',
                            style: TextStyle(
                              fontSize: appSubHeadingFontSize,
                              color: Color(0xffF9C93A),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              )
            ],
          )),
    );
  }
}
