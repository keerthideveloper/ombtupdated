import 'package:fluttertoast/fluttertoast.dart';
import 'package:ombt/pageMap.dart';
import 'package:ombt/widgets/common_SnackBar.dart';

class FarmerBankDetail extends StatefulWidget {

  @override
  _FarmerBankDetailState createState() => _FarmerBankDetailState();
}

class _FarmerBankDetailState extends State<FarmerBankDetail>
    with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldkey = new GlobalKey<ScaffoldState>();
  bool isLoading = false;

  TextEditingController bankNameController = TextEditingController();
  TextEditingController fullNameController = TextEditingController();
  TextEditingController accountNumberController = TextEditingController();
  TextEditingController ifscCodeController = TextEditingController();
  File _image;

  List<dynamic> passBookImageData;
  final _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  void postbanklDetail() async {
    if (imageUploadValidation() == true) {
      showDialog(
          context: context, child: Center(child: CircularProgressIndicator()));
      setState(() {
        isLoading = true;
      });
      final bank = {
        'BankFullName': bankNameController.text,
        'BankName': fullNameController.text,
        'AccountNumber': accountNumberController.text,
        'IFSCCode': ifscCodeController.text,
        'FarmerId': '1',
        'BankBooKImage': passBookImageData,
      };

      await bankapi().createAlbum(bank).then((value) {
        print(value);
        if (value['status'] == 'True') {
          Navigator.of(context).pop();
          setState(() {
            isLoading = false;
            bankNameController.text = "";
            fullNameController.text = "";
            accountNumberController.text = '';
            ifscCodeController.text = "";
            _image = null;
          });
          toast('Farmer Bank Details Updated');
          // InSnackBar('Farmer Bank Details Updated');

          // showInSnackBar(_scaffoldkey, 'Farmer Bank Details Added');
          // InSnackBar('send Successfully');
          // customSnacBar('Send Successfully');
          Future.delayed(Duration(seconds: 2), () => Navigator.pop(context));
        }
      }).catchError((error) {
        Navigator.of(context).pop();
        showInSnackBarOne(error);
      });
    }
    // SnackBar
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: CommonBody(
        appTitle: "Bank Details",
        onPressed: () => Navigator.pop(context),
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Stack(
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    // Text('${7.3 * SizeConfig.heightMultiplier}'),
                    Container(
                      // height: 7.3 * SizeConfig.heightMultiplier,
                      child: Listener(
                        onPointerDown: (_) => FocusScope.of(context).unfocus(),
                        child: AppTextField(
                          textInputAction: TextInputAction.next,
                          controllerName: bankNameController,
                          validateFunction: (v) =>
                              validateFiled(v, 'bank name'),
                          hintName: 'Bank Name',
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 3 * SizeConfig.heightMultiplier,
                    ),
                    Container(
                      // height: 7.3 * SizeConfig.heightMultiplier,
                      child: Listener(
                        onPointerDown: (_) => FocusScope.of(context).unfocus(),
                        child: AppTextField(
                          textInputAction: TextInputAction.next,
                          controllerName: fullNameController,
                          validateFunction: (v) =>
                              validateFiled(v, 'full name'),
                          hintName: 'Full Name',
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 3 * SizeConfig.heightMultiplier,
                    ),
                    Container(
                      // height: 7.3 * SizeConfig.heightMultiplier,
                      child: Listener(
                        onPointerDown: (_) => FocusScope.of(context).unfocus(),
                        child: AppTextField(
                          textInputAction: TextInputAction.next,
                          controllerName: accountNumberController,
                          validateFunction: (v) =>
                              validateFiled(v, 'account number'),
                          keyboardType: TextInputType.number,
                          hintName: 'Account Number',
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 3 * SizeConfig.heightMultiplier,
                    ),
                    Container(
                      // height: 7.3 * SizeConfig.heightMultiplier,
                      child: Listener(
                        onPointerDown: (_) => FocusScope.of(context).unfocus(),
                        child: AppTextField(
                          textInputAction: TextInputAction.done,
                          controllerName: ifscCodeController,
                          validateFunction: (value) =>
                              validateFiled(value, 'ifsc code'),
                          hintName: 'IFSC Code',
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 4 * SizeConfig.heightMultiplier,
                    ),
                    GestureDetector(
                      onTap: () async {
                        FocusScope.of(context).requestFocus(FocusNode());
                        var image = await showDialog(
                            context: context, child: UploadOptionDialog());
                        setState(() {
                          passBookImageData = image;
                          _image = image[0];
                        });
                      },
                      child: Container(
                          height: 16.3 * SizeConfig.heightMultiplier,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: _image == null
                                  ? AssetImage(
                                      'assets/images/sbi-passbook-500x500@2x.png')
                                  : FileImage(_image),
                              fit: BoxFit.fill,
                            ),
                            border: Border.all(
                                color: Color(0xff58BC7A).withOpacity(0.2),
                                width: 2.0),
                          ),
                          child: Center(
                            child: Image.asset(
                              'assets/icons/cameraWhilte1.png',
                              height: 8 * SizeConfig.imageSizeMultiplier,
                            ),
                          )),
                    ),
                    SizedBox(
                      height: 4 * SizeConfig.heightMultiplier,
                    ),
                    AppButton(
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          postbanklDetail();
                        }
                      },
                      name: 'Submit',
                    ),
                  ],
                ),
                // Container(
                //   height: 70 * SizeConfig.heightMultiplier,
                //   width: 100 * SizeConfig.widthMultiplier,
                //   child: isLoading
                //       ? SpinKitCubeGrid(
                //           // color: Color(0xff58BC7A),
                //           color: Colors.lightGreen,
                //           size: 50.0,
                //           controller: AnimationController(
                //               vsync: this,
                //               duration: const Duration(milliseconds: 1200)),
                //         )
                //       : Container(),
                // ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  validateFiled(value, data) {
    if (value.isEmpty) {
      return 'Please enter ${data}';
    }
    return null;
  }

  imageUploadValidation() {
    if (passBookImageData == null) {
      showInSnackBarOne('Pass Book');
    } else {
      return true;
    }
  }

  void showInSnackBarOne(value) {
    Fluttertoast.showToast(
        msg: 'Please Upload ${value} Image',
        backgroundColor: Colors.black,
        // fontSize: 25
        gravity: ToastGravity.TOP,
        textColor: Colors.white
    );


  }


  void toast (tomsg){
    Fluttertoast.showToast(

        msg: tomsg,
        backgroundColor: Colors.black,
        // fontSize: 25
        gravity: ToastGravity.TOP,
        textColor: Colors.white
    );}
}



