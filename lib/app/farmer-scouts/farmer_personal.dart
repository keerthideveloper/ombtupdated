import 'package:fluttertoast/fluttertoast.dart';
import 'package:ombt/pageMap.dart';
import 'package:ombt/widgets/common_SnackBar.dart';

class FarmerScoutsProfile extends StatefulWidget {
  final String fullnamein;
  final String villagein;
  final String farmAreain;
  final String pinCodein;
  final String mobilenoin;
  final int idin;
  FarmerScoutsProfile(
      {this.idin,
      this.fullnamein,
      this.villagein,
      this.farmAreain,
      this.pinCodein,
      this.mobilenoin});
  @override
  _FarmerScoutsProfileState createState() => _FarmerScoutsProfileState();
}

class _FarmerScoutsProfileState extends State<FarmerScoutsProfile>
    with SingleTickerProviderStateMixin {
  File _image;
  File panImage;
  File adharCardImage;
  File farmImage;

  List<dynamic> panImageData;
  List<dynamic> adharCardImageData;
  List<dynamic> farmImageData;
  List<dynamic> farmerImageData;

  TextEditingController nameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController villageController = TextEditingController();
  TextEditingController farmAreaController = TextEditingController();
  TextEditingController pinCodeController = TextEditingController();

  var locationData;
  final GlobalKey<ScaffoldState> _scaffoldkey = new GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  String verifiedNumber;

  @override
  void initState() {
    verifiedNumber = '+91-123456789';
    setState(() {
      phoneController.text = verifiedNumber;
    });
    super.initState();
  }

  void postPersonalDetail() async {
    if (imageUploadValidation() == true) {
      showDialog(
          context: context, child: Center(child: CircularProgressIndicator()));
      final farmer = {
        'FullName': nameController.text,
        'PhoneNumber': phoneController.text,
        'village': villageController.text,
        'PinCode': villageController.text,
        'farmArea': farmAreaController.text,
        'AdharCardImage': adharCardImageData,
        'PanCardImage': panImageData,
        'FarmImage': farmImageData,
        'LocationLatitude': locationData['lat'],
        'LocationLongitude': locationData['long'],
        'FarmerImage': farmerImageData,
      };

      await FarmerApi().createPersonalDetail(farmer).then((value) {
        print(value);
        if (value['status'] == 'True') {
          Navigator.of(context).pop();
          toastmsg('Farmer Personal Details Updated');
          Future.delayed(Duration(seconds: 3), () {

            Navigator.pop(context);
          });
          // Navigator.of(context).pop();
          // Navigator.pop(context);
        }
      }).catchError((error) {
        Navigator.of(context).pop();
        customSnackBar(error);
      });
    }
    // SnackBar
  }

  @override
  Widget build(BuildContext context) {
    nameController.text = widget.fullnamein;
    villageController.text = widget.villagein;
    pinCodeController.text = widget.pinCodein;
    phoneController.text = widget.mobilenoin;
    farmAreaController.text = widget.farmAreain;
    return Scaffold(
      key: _scaffoldkey,
      body: CommonBody(
        appTitle: "Personal Details",
        onPressed: () => Navigator.pop(context),
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Stack(
              children: [
                Column(
                  children: [
                    GestureDetector(
                      onTap: () async {
                        FocusScope.of(context).requestFocus(FocusNode());
                        var image = await showDialog(
                            context: context, child: UploadOptionDialog());
                        print(image);
                        setState(() {
                          farmerImageData = image;
                          _image = image[0];
                        });
                      },
                      child: Column(
                        children: [
                          Visibility(
                            visible: _image != null,
                            child: Container(
                              height: 21 * SizeConfig.heightMultiplier,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                image: DecorationImage(
                                  image: _image == null
                                      ? AssetImage('assets\\test.png')
                                      : FileImage(_image),
                                  fit: BoxFit.fill,
                                ),
                                border: Border.all(
                                    color: Color(0xff58BC7A).withOpacity(0.2),
                                    width: 2.0),
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Center(
                                    child: Image.asset(
                                      'assets/icons/camera.png',
                                      height:
                                          5.8 * SizeConfig.imageSizeMultiplier,
                                      color: Colors.white,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 0.9 * SizeConfig.heightMultiplier,
                                  ),
                                  Text(
                                    'Take Photo',
                                    style: TextStyle(
                                      fontSize: 1.8 * SizeConfig.textMultiplier,
                                      color: Colors.white,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Visibility(
                            visible: _image == null,
                            child: Container(
                              height: 21 * SizeConfig.heightMultiplier,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.all(
                                  Radius.circular(5.0),
                                ),
                                border: Border.all(
                                  color: Color(0xff58BC7A).withOpacity(0.2),
                                  width: 2.0,
                                ),
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Center(
                                    child: Image.asset(
                                      'assets/icons/camera.png',
                                      height:
                                          5.8 * SizeConfig.imageSizeMultiplier,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 0.9 * SizeConfig.heightMultiplier,
                                  ),
                                  Text(
                                    'Take Photo',
                                    style: TextStyle(
                                      fontSize: 1.8 * SizeConfig.textMultiplier,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 3.3 * SizeConfig.heightMultiplier,
                    ),

                    Container(
                      child: AppTextField(



                        controllerName: nameController,
                        textInputAction: TextInputAction.next,
                        hintName: 'Full Name',
                        validateFunction: (v) => validateFiled(v, 'name'),
                      ),
                    ),
                    SizedBox(
                      height: 2.2 * SizeConfig.heightMultiplier,
                    ),
                    Row(
                      children: [

                        new Flexible(

                          child: Container(
                            // height: 7.3 * SizeConfig.heightMultiplier,
                            child: AppTextField(

                              readOnly: true,
                              controllerName: phoneController,
                              hintName: 'Mobile Number',
                              enableInteractiveSelection: true,
                              textInputAction: TextInputAction.next,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 20.0,
                        ),
                        new Flexible(
                          child: Container(
                            // height: 7.3 * SizeConfig.heightMultiplier,
                            child: AppTextField(
                              controllerName: villageController,
                              validateFunction: (value) =>
                                  validateFiled(value, 'Village'),
                              textInputAction: TextInputAction.next,
                              hintName: 'Village',
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 2.2 * SizeConfig.heightMultiplier,
                    ),
                    Row(
                      children: [
                        new Flexible(
                          child: Container(
                            // height: 7.3 * SizeConfig.heightMultiplier,
                            child: AppTextField(
                              controllerName: farmAreaController,
                              textInputAction: TextInputAction.next,
                              validateFunction: (value) =>
                                  validateFiled(value, 'Farm Area'),
                              hintName: 'Farm Area (acres)',
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 20.0,
                        ),
                        new Flexible(
                          child: Container(
                            // height: 7.3 * SizeConfig.heightMultiplier,
                            child: AppTextField(
                              controllerName: pinCodeController,
                              textInputAction: TextInputAction.next,
                              validateFunction: (v) =>
                                  validateFiled(v, 'Pin code'),
                              hintName: 'Pin Code',
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 1.3 * SizeConfig.heightMultiplier,
                    ),
                    Container(
                      child: Row(
                        children: [
                          new Flexible(
                            child: InkWell(
                              onTap: () async {
                                FocusScope.of(context)
                                    .requestFocus(FocusNode());
                                var image = await showDialog(
                                    context: context,
                                    child: UploadOptionDialog());
                                setState(() {
                                  print(image);
                                  panImageData = image;
                                  panImage = image[0];
                                });
                              },
                              child: Container(
                                height: 9.8 * SizeConfig.heightMultiplier,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Color(0xff58BC7A).withOpacity(0.2),
                                    width: 2.0,
                                  ),
                                  image: DecorationImage(
                                    image: panImage == null
                                        ? AssetImage(
                                            'assets/images/pancard.png')
                                        : FileImage(panImage),
                                    fit: BoxFit.fill,
                                  ),
                                ),
                                child: Container(
                                  color: Colors.green.withOpacity(0.5),
                                  child: Center(
                                    child: Image.asset(
                                      'assets/icons/camera.png',
                                      height:
                                          5.8 * SizeConfig.imageSizeMultiplier,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 4.8 * SizeConfig.widthMultiplier,
                          ),
                          new Flexible(
                            child: InkWell(
                              onTap: () async {
                                FocusScope.of(context)
                                    .requestFocus(FocusNode());
                                var image = await showDialog(
                                    context: context,
                                    child: UploadOptionDialog());
                                setState(() {
                                  adharCardImageData = image;
                                  adharCardImage = image[0];
                                  print(image[0]);
                                });
                              },
                              child: Container(
                                height: 9.8 * SizeConfig.heightMultiplier,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Color(0xff58BC7A).withOpacity(0.2),
                                    width: 2.0,
                                  ),
                                  image: DecorationImage(
                                    image: adharCardImage == null
                                        ? AssetImage(
                                            'assets/images/adharcard.png')
                                        : FileImage(adharCardImage),
                                    fit: BoxFit.fill,
                                  ),
                                ),
                                child: Container(
                                  color: Colors.green.withOpacity(0.5),
                                  child: Center(
                                    child: Image.asset(
                                      'assets/icons/camera.png',
                                      height:
                                          5.8 * SizeConfig.imageSizeMultiplier,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 4.8 * SizeConfig.widthMultiplier,
                          ),
                          new Flexible(
                            child: InkWell(
                              onTap: () async {
                                FocusScope.of(context)
                                    .requestFocus(FocusNode());
                                var image = await showDialog(
                                    context: context,
                                    child: UploadOptionDialog());
                                setState(() {
                                  farmImageData = image;
                                  farmImage = image[0];
                                });
                              },
                              child: Container(
                                height: 9.8 * SizeConfig.heightMultiplier,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Color(0xff58BC7A).withOpacity(0.2),
                                    width: 2.0,
                                  ),
                                  image: DecorationImage(
                                    image: farmImage == null
                                        ? AssetImage('assets/images/farm.png')
                                        : FileImage(farmImage),
                                    fit: BoxFit.fill,
                                  ),
                                ),
                                child: Container(
                                  color: Colors.green.withOpacity(0.5),
                                  child: Center(
                                    child: Image.asset(
                                      'assets/icons/camera.png',
                                      height:
                                          5.8 * SizeConfig.imageSizeMultiplier,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 3 * SizeConfig.heightMultiplier,
                    ),
                    Row(
                      children: [
                        new Flexible(
                          child: Container(
                            height: 7 * SizeConfig.heightMultiplier,
                            width: double.infinity,
                            child: RaisedButton(
                              onPressed: () async {
                                FocusScope.of(context)
                                    .requestFocus(FocusNode());
                                var _location = await showDialog(
                                    context: context, child: LiveMapDialog());
                                setState(() {
                                  locationData = _location;
                                  // print('test ${locationData['long']}');
                                });
                              },
                              child: Text(
                                'Capture Location',
                                style: TextStyle(
                                    fontSize: 1.8 * SizeConfig.textMultiplier,
                                    color: Colors.white),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 20.0,
                        ),
                        new Flexible(
                          child: Container(
                            height: 7 * SizeConfig.heightMultiplier,
                            width: double.infinity,
                            child: RaisedButton(
                              onPressed: () {
                                if (_formKey.currentState.validate()) {
                                  // print('test');
                                  postPersonalDetail();
                                }
                              },
                              child: Text(
                                'Submit',
                                style: TextStyle(
                                    fontSize: 1.8 * SizeConfig.textMultiplier,
                                    color: Colors.white),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                // Container(
                //   height: 70 * SizeConfig.heightMultiplier,
                //   width: 100 * SizeConfig.widthMultiplier,
                //   child: isLoading
                //       ? SpinKitCubeGrid(
                //           // color: Color(0xff58BC7A),
                //           color: Colors.lightGreen,
                //           size: 50.0,
                //           controller: AnimationController(
                //               vsync: this,
                //               duration: const Duration(milliseconds: 1200)),
                //         )
                //       : Container(),
                // ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  validateFiled(value, data) {
    if (value.isEmpty) {
      return 'Please enter ${data}';
    }
    return null;
  }

  imageUploadValidation() {
    if (farmerImageData == null) {
      customSnackBar('Farmer');
    } else if (panImageData == null) {
      customSnackBar('Pan Card');
    } else if (adharCardImageData == null) {
      customSnackBar('Adhar Card');
    } else if (farmImageData == null) {
      customSnackBar('Farm');
    } else if (locationData == null) {
      customSnackBar('Capture Location');
    } else {
      return true;
    }
  }

  void customSnackBar(data) {
    Fluttertoast.showToast(
        msg: 'Please Upload ${data} Image',
        backgroundColor: Colors.black,
        // fontSize: 25
        gravity: ToastGravity.TOP,
        textColor: Colors.white
    );

    // _scaffoldKey.currentState.showSnackBar(new SnackBar(
    //   content: new Text("Please Upload ${value} Image"),
    //   action: SnackBarAction(
    //       label: 'Undo',
    //       onPressed: _scaffoldKey.currentState.hideCurrentSnackBar),
    // ));
  }
  void toastmsg (tostmsg){
    Fluttertoast.showToast(

        msg: tostmsg,
        backgroundColor: Colors.black,
        // fontSize: 25
        gravity: ToastGravity.TOP,
        textColor: Colors.white
    );}
}
