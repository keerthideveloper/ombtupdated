import 'package:ombt/pageMap.dart';

class OMBTDashBoard extends StatefulWidget {
  @override
  _OMBTDashBoardState createState() => _OMBTDashBoardState();
}

class _OMBTDashBoardState extends State<OMBTDashBoard> {
  List<dynamic> dashboard = [
    {'image': 'assets/icons/farmers@2x.png', 'name': 'Farmer Scouts'},
    {'image': 'assets/icons/inventory.png', 'name': 'Quality Manager'},
    {'image': 'assets/icons/fc_dispatch.png', 'name': 'Picking Delivery'},
    {'image': 'assets/icons/order.png', 'name': 'Procurement\nManager'},
    {'image': 'assets/icons/delivery_agent.png', 'name': 'Fulfillment Center '},
    {'image': 'assets/icons/worker@2x.png', 'name': 'Sales'},
  ];
  @override
  Widget build(BuildContext context) {
    var landscape = MediaQuery.of(context).orientation == Orientation.landscape;
    return CommonBody(
      appTitle: 'DashBoard',
      onPressed: () => Navigator.pop(context),
      child: GridView.builder(
        physics: ScrollPhysics(),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: landscape ? 3 : 2,
            childAspectRatio: 1 / 1,
            crossAxisSpacing: 2.0,
            mainAxisSpacing: 2.0),
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            onTap: () {
              switch (index) {
                case 0:
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => FarmerScoutsHome(),
                    ),
                  );
                  break;
                case 1:
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Indent(),
                    ),
                  );
                  break;
                case 2:
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Logesticmanage(),
                    ),
                  );
                  break;
                case 3:
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ProcurementManagement(),
                    ),
                  );
                  break;
                case 4:
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Reach(),
                    ),
                  );
                  break;
                case 5:
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Salesdash(),
                    ),
                  );
                  break;
              }
            },
            child: AppCard(
              child: Container(
                padding: EdgeInsets.all(10.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(dashboard[index]['image']),
                    SizedBox(
                      height: 20.0,
                    ),
                    Text(
                      dashboard[index]['name'],
                      style: TextStyle(fontSize: appSubHeadingFontSize),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
        itemCount: dashboard.length,
      ),
    );
  }
}
