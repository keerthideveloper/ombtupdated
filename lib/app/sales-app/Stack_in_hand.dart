import 'package:intl/intl.dart';
import 'package:ombt/modules/Stack_in_hand_list.dart';
import 'package:ombt/pageMap.dart';

class Stackhand extends StatefulWidget {
  @override
  _StackhandState createState() => _StackhandState();
}

class _StackhandState extends State<Stackhand> {
  DateTime selectedDate = DateTime.now();
  double textsizein = SizeConfig.textMultiplier;
  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

  @override
  Widget build(BuildContext context) {
    print(SizeConfig.textMultiplier * 0.8);
    return CommonBody(
      appLogo: 'salesLogo.png',
      appTitle: 'Stock in Hand',
      onPressed: () => Navigator.pop(context),
      child: Column(
        children: [
          Flexible(
            child: Row(
              children: [
                Text(
                  'Date : ',
                  style: TextStyle(
                    fontSize: appSubHeadingFontSize,
                  ),
                ),
                GestureDetector(
                  child: Text(
                    '${DateFormat('dd-MM-yyyy').format(selectedDate.toLocal())}',
                    style: TextStyle(
                      fontSize: appSubHeadingFontSize,
                    ),
                  ),
                  onTap: () => _selectDate(context),
                ),
              ],
            ),
            flex: 2,
          ),
          Spacer(
            flex: 1,
          ),
          Flexible(
            child: Row(
              children: [
                Text('Product',
                    style: TextStyle(
                      fontSize: appSubHeadingFontSize,
                    )),
                Spacer(
                  flex: 3,
                ),
                Text('Unit',
                    style: TextStyle(
                      fontSize: appSubHeadingFontSize,
                    )),
                Spacer(
                  flex: 1,
                ),
                Text('Price',
                    style: TextStyle(
                      fontSize: appSubHeadingFontSize,
                    )),
                Spacer(flex: 1)
              ],
            ),
            flex: 2,
          ),
          Spacer(
            flex: 1,
          ),
          Flexible(
            child: Divider(
              color: Colors.orange,
              thickness: 1,
            ),
            flex: 1,
          ),
          Spacer(
            flex: 1,
          ),
          Flexible(
            child: listcard(),
            flex: 18,
          )
        ],
      ),
    );
  }

  Widget listcard() {
    return ListView.builder(
        itemCount: stackhand.length,
        itemBuilder: (context, index) {
          return AppCard(
            child: IntrinsicHeight(
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Row(
                  children: [
                    Container(
                      height: 30,
                      width: 30,
                      child: Image.asset(stackhand[index]['images']),
                    ),
                    Spacer(flex: 1),
                    Text(
                      stackhand[index]['productname'],
                      style: TextStyle(fontSize: appNormalTextSize),
                    ),
                    Spacer(flex: 4),
                    VerticalDivider(
                      color: Colors.greenAccent,
                      thickness: 1,
                    ),
                    Spacer(flex: 1),
                    Text(
                      '100 kg',
                      style: TextStyle(fontSize: appNormalTextSize),
                    ),
                    Spacer(flex: 1),
                    VerticalDivider(
                      color: Colors.greenAccent,
                      thickness: 1,
                    ),
                    Spacer(flex: 1),
                    Text(
                      '\u20B9 15.00',
                      style: TextStyle(fontSize: appNormalTextSize),
                    ),
                    Spacer(flex: 1),
                  ],
                ),
              ),
            ),
          );
        });
  }
}
