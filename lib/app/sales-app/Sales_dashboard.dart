import 'package:ombt/pageMap.dart';

class Salesdash extends StatefulWidget {
  @override
  _SalesdashState createState() => _SalesdashState();
}

class _SalesdashState extends State<Salesdash> {
  double imagesize = 10 * SizeConfig.imageSizeMultiplier;
  double textsize = 2.2;
  double spacein = 1.2;
  @override
  Widget build(BuildContext context) {
    return CommonBody(
      onPressed: () => Navigator.of(context).pop(),
      appLogo: 'salesLogo.png',
      appTitle: "Welcome to ekkam",
      child: dashboardin(),
    );
  }

  Widget dashboardin() {
    return StaggeredGridView.count(
      crossAxisCount: 2,
      crossAxisSpacing: 15.0,
      mainAxisSpacing: 15.0,
      children: <Widget>[
        _buildTile(
            Padding(
              padding: const EdgeInsets.all(24.0),
              child: IntrinsicHeight(
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        children: [
                          CircleAvatar(
                            backgroundColor: Colors.white,
                            radius: 25,
                            child: Container(
                                width: 30,
                                height: 30,
                                child: Image.asset('assets/icons/user.png')),
                          ),
                          SizedBox(
                            width: SizeConfig.widthMultiplier / spacein,
                          ),
                          Text(
                            '1500/2500',
                            style: TextStyle(
                                fontSize: appSubHeadingFontSize,
                                color: Colors.white),
                          ),
                        ],
                      ),
                      VerticalDivider(
                        color: Colors.white,
                        thickness: 1,
                      ),
                      Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Text('60%',
                                style: TextStyle(
                                    fontSize:
                                        SizeConfig.textMultiplier * textsize,
                                    color: Colors.white)),
                            Text('1500 kg /2500',
                                style: TextStyle(
                                    fontSize: appSubHeadingFontSize,
                                    color: Colors.white))
                          ])
                    ]),
              ),
            ),
            colorin: Colors.pink,
            onTap: () => Navigator.push(context,
                MaterialPageRoute(builder: (context) => Targetpage()))),
        _buildTile(
            salesCard(
                imagesize: imagesize,
                textsize: appSubHeadingFontSize,
                image: 'assets/icons/inventory.png',
                title: 'Stock In Hand'), onTap: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => Stackhand()));
        }),
        _buildTile(
            salesCard(
                imagesize: imagesize,
                textsize: appSubHeadingFontSize,
                image: 'assets/icons/logistics.png',
                title: 'Order For Day'),
            onTap: () => Navigator.push(context,
                MaterialPageRoute(builder: (context) => Orderofday()))),
        _buildTile(
            salesCard(

                imagesize: imagesize,
                textsize: appSubHeadingFontSize,
                image: 'assets/icons/Plan-Business.png',
                title: 'Outstanding'), onTap: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => Outstanding()));
        }),
        _buildTile(
            salesCard(
                imagesize: imagesize,
                textsize: appSubHeadingFontSize,
                image: 'assets/icons/visitplan.png',
                title: 'Visit Plan'), onTap: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => Visitplan()));
        }),
        _buildTile(
            salesCard(
                imagesize: imagesize,
                textsize: appSubHeadingFontSize,
                image: 'assets/icons/order.png',
                title: 'Previous Day \nOrder Details'),
            onTap: () => Navigator.push(
                context, MaterialPageRoute(builder: (context) => Order()))),
        _buildTile(
            salesCard(
                imagesize: imagesize,
                textsize: appSubHeadingFontSize,
                image: 'assets/icons/Layer_3.png',
                title: 'Communication'),
            onTap: () => Navigator.push(context,
                MaterialPageRoute(builder: (context) => Communication()))),
      ],
      staggeredTiles: [
        StaggeredTile.extent(2, 110.0),
        StaggeredTile.extent(1, 170.0),
        StaggeredTile.extent(1, 170.0),
        StaggeredTile.extent(1, 170.0),
        StaggeredTile.extent(1, 170.0),
        StaggeredTile.extent(1, 170.0),
        StaggeredTile.extent(1, 170.0),
      ],
    );
  }

  Widget _buildTile(Widget child, {Function() onTap, MaterialColor colorin}) {
    return
      AppCard(
        color: colorin,
        elevation: 0.0,
          child: InkWell(
            onTap: onTap != null
                ? () => onTap()
                : () {
                    print('Not set yet');
                  },
            child: child));
  }
}

class salesCard extends StatelessWidget {
  const salesCard({
    Key key,
    @required this.imagesize,
    @required this.textsize,
    @required this.image,
    @required this.title,
  }) : super(key: key);

  final double imagesize;
  final double textsize;
  final String image;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              width: imagesize,
              height: imagesize,
              child: Image.asset(image),
            ),
            FittedBox(
              fit: BoxFit.contain,
              child: Text(title, style: TextStyle(fontSize: textsize)),
            ),
          ]),
    );
  }
}
