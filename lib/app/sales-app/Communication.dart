import 'package:ombt/pageMap.dart';

class Communication extends StatefulWidget {
  @override
  _CommunicationState createState() => _CommunicationState();
}

class _CommunicationState extends State<Communication> {
  double spacein = 15;
  String selected = '';

  @override
  Widget build(BuildContext context) {
    return CommonBody(
        appLogo: 'salesLogo.png',
        appTitle: 'Communication',
        onPressed: () => Navigator.pop(context),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: spacein * 5,
              ),
              cardRow(),
              SizedBox(
                height: spacein * 2,
              ),
              Text(
                "Message",
                style: TextStyle(
                  fontSize: appHeadingFontSize,
                ),
              ),
              SizedBox(
                height: spacein,
              ),
              TextFormField(
                maxLines: 6,
                decoration: InputDecoration(
                  fillColor: Colors.white,
                  filled: true,
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Color.fromRGBO(109, 195, 135, 1),
                    ),
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Color.fromRGBO(109, 195, 135, 1),
                    ),
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                ),
              ),
              SizedBox(
                height: spacein,
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                child: AppButton(onPressed: () {}, name: "Submit"),
              ),
              SizedBox(
                height: spacein,
              ),
            ],
          ),
        ));
  }

  Widget cardRow() {
    double paddingin = 15.0;
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        AppCard(
          color: selected == 'Ops Team'
              ? Color(0xff58BC7A)
              : Colors.white,

          onTap: () {
            setState(() {
              selected = 'Ops Team';
            });

          },


          child: Padding(
            padding: EdgeInsets.all(paddingin),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                FittedBox(
                  fit: BoxFit.contain,
                  child: Container(
                    height: 50,
                    child: Image.asset("assets/icons/work-team.png",color: selected == 'Ops Team' ? Colors.white : null,),
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                FittedBox(
                    fit: BoxFit.fitHeight,
                    child: Text("Ops Team",
                        style: TextStyle(
                          color:
                          selected == 'Ops Team' ? Colors.white : null,
                          fontSize: appSubHeadingFontSize,
                        ))),
              ],
            ),
          ),
        ),
        AppCard(
          color: selected == 'Procurement'
              ? Color(0xff58BC7A)
              : Colors.white,

          onTap: () {
            setState(() {
              selected = 'Procurement';
            });

          },
          child: Padding(
            padding: EdgeInsets.all(paddingin),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                FittedBox(
                  fit: BoxFit.contain,
                  child: Container(
                    height: 50,
                    child: Image.asset("assets/icons/transfer.png",color: selected == 'Procurement' ? Colors.white : null,),
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                FittedBox(
                    fit: BoxFit.fitHeight,
                    child: Text("Procurement",
                        style: TextStyle(
                          color:
                          selected == 'Procurement' ? Colors.white : null,
                          fontSize: appSubHeadingFontSize,
                        ))),
              ],
            ),
          ),
        ),
        AppCard(
          color: selected == 'HR Team'
              ? Color(0xff58BC7A)
              : Colors.white,

          onTap: () {
            setState(() {
              selected = 'HR Team';
            });

          },
          child: Padding(
            padding: EdgeInsets.all(paddingin),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                FittedBox(
                  fit: BoxFit.contain,
                  child: Container(
                    height: 50,
                    child: Image.asset("assets/icons/support.png",
                      color: selected == 'HR Team' ? Colors.white : null,),
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                FittedBox(
                    fit: BoxFit.fitHeight,
                    child: Text("HR Team",
                        style: TextStyle(
                          color:
                          selected == 'HR Team' ? Colors.white : null,
                          fontSize: appSubHeadingFontSize,
                        ))),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
