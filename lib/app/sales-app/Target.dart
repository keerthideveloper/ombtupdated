import 'package:intl/intl.dart';
import 'package:ombt/modules/Target_list.dart';
import 'package:ombt/pageMap.dart';

class Targetpage extends StatefulWidget {
  @override
  _TargetpageState createState() => _TargetpageState();
}

class _TargetpageState extends State<Targetpage> {
  int selecteRadioButton;

  DateTime selectedDate = DateTime.now();
  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      selecteRadioButton = 0;
    });
  }

  Widget choice(val) {
    switch (val) {
      case 0:
        return dailyradio();
        break;
      case 1:
        return weeklyradio();
        break;
      case 2:
        return weeklyradio();
        break;
      default:
        return dailyradio();
    }
  }

  setSelectedRadio(int val) {
    setState(() {
      selecteRadioButton = val;
    });
  }

  Widget build(BuildContext context) {
    return CommonBody(
      appTitle: 'Target',
      onPressed: () => Navigator.pop(context),
      child: Padding(
        padding: EdgeInsets.all(20.0),
        child: Column(
          // mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Flexible(
              child: Row(
                children: [
                  Text(
                    'Date: ',
                    style: TextStyle(fontSize: appSubHeadingFontSize),
                  ),
                  GestureDetector(
                    child: Text(
                      '${DateFormat('dd-MM-yyyy').format(selectedDate.toLocal())}',
                      style: TextStyle(fontSize: appSubHeadingFontSize),
                    ),
                    onTap: () => _selectDate(context),
                  ),
                ],
              ),
              flex: 3,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Radio(
                  value: 0,
                  groupValue: selecteRadioButton,
                  activeColor: Color.fromRGBO(109, 195, 135, 1),
                  onChanged: (val) {
                    setSelectedRadio(val);
                  },
                ),
                Spacer(
                  flex: 1,
                ),
                Radio(
                  value: 1,
                  groupValue: selecteRadioButton,
                  activeColor: Color.fromRGBO(109, 195, 135, 1),
                  onChanged: (val) {
                    setSelectedRadio(val);
                  },
                ),
                Spacer(
                  flex: 1,
                ),
                Radio(
                  value: 2,
                  groupValue: selecteRadioButton,
                  activeColor: Color.fromRGBO(109, 195, 135, 1),
                  onChanged: (val) {
                    setSelectedRadio(val);
                  },
                ),
              ],
            ),
            Container(
              margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
              child: Row(
                children: <Widget>[
                  FittedBox(
                    child: Text("Daily"),
                  ),
                  Spacer(
                    flex: 1,
                  ),
                  FittedBox(
                    child: Text("Weekly"),
                  ),
                  Spacer(
                    flex: 1,
                  ),
                  FittedBox(
                    child: Text("Monthly"),
                  ),
                ],
              ),
            ),
            Flexible(
              child: Divider(
                color: Color.fromRGBO(109, 195, 135, 1),
                thickness: 1,
              ),
            ),
            Spacer(
              flex: 1,
            ),
            Expanded(
              child: choice(selecteRadioButton),
              flex: 20,
            ),
          ],
        ),
      ),
    );
  }

  Widget dailyradio() {
    return ListView.builder(
        itemCount: dailytarget.length,
        itemBuilder: (context, index) {
          return AppCard(
            elevation: 5.0,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    '${dailytarget[index]['targetname']}',
                    style: TextStyle(
                        fontSize: appSubHeadingFontSize,
                        color: dailytarget[index]['textcolor']),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    '${dailytarget[index]['count']}',
                    style: TextStyle(
                        fontSize: appSubHeadingFontSize,
                        color: dailytarget[index]['textcolor']),
                  ),
                ],
              ),
            ),
          );
        });
  }

  Widget weeklyradio() {
    String target = 'Target';
    String achieved = 'Achieved';
    bool sizeoff = SizeConfig.isMobilePortrait;
    return GridView(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2, childAspectRatio: sizeoff ? 0.45 : 0.9),
      children: [cardmake(target), cardmake(achieved)],
    );
  }

  Widget cardmake(data) {
    return ListView.builder(
        physics: NeverScrollableScrollPhysics(),
        itemCount: 3,
        itemBuilder: (context, index) {
          return Column(children: [
            weeklytarget[0][data][index]['choice']
                ? Container(
                    margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
                    child: Text(
                      data,
                      style: TextStyle(
                        fontSize: appSubHeadingFontSize,
                      ),
                    ),
                  )
                : SizedBox.shrink(),
            Container(
              width: MediaQuery.of(context).size.width,
              child: AppCard(
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        '${weeklytarget[0][data][index]['targetname']}',
                        style: TextStyle(
                            fontSize: appSubHeadingFontSize,
                            color: weeklytarget[0][data][index]['textcolor']),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        '${weeklytarget[0][data][index]['count']}',
                        style: TextStyle(
                            fontSize: appSubHeadingFontSize,
                            color: weeklytarget[0][data][index]['textcolor']),
                      ),
                    ],
                  ),
                ),
              ),
            )
          ]);
        });
  }
}
