import 'package:ombt/modules/Previous_day_details_list.dart';
import 'package:ombt/pageMap.dart';

class Order extends StatefulWidget {
  @override
  _OrderState createState() => _OrderState();
}

class _OrderState extends State<Order> {
  var textin = appNormalTextSize;
  @override
  Widget build(BuildContext context) {
    return CommonBody(
      appLogo: 'salesLogo.png',
      appTitle: 'Yesterday’s orders',
      onPressed: () => Navigator.pop(context),
      child: Padding(
        padding: const EdgeInsets.all(0),
        child: Container(
          child: Column(
            children: <Widget>[
              Flexible(
                child: Row(
                  children: <Widget>[
                    Row(
                      children: [
                        Icon(
                          Icons.date_range,
                          color: Color.fromRGBO(109, 195, 135, 1),
                          size: 20,
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          "21/August/2020",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: appSubHeadingFontSize),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          "11.47AM",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: appSubHeadingFontSize),
                        ),
                      ],
                    ),
                  ],
                ),
                flex: 2,
              ),
              Flexible(
                child: Divider(
                  color: Color.fromRGBO(109, 195, 135, 1),
                  thickness: 1,
                ),
                flex: 1,
              ),
              Spacer(
                flex: 1,
              ),
              Flexible(
                child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: 3,
                    itemBuilder: (context, index) {
                      return orderlistcard(index);
                    }),
                flex: 34,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget orderlistcard(index) {
    return AppCard(
      child: InkWell(
        splashColor: Colors.orange[200],
        onTap: () {
          print('test');
        },
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Row(
            children: <Widget>[
              Text(
                '${orderlist[index]['ordertitle']}',
                style: TextStyle(
                    color: orderlist[index]['colortext'],
                    fontWeight: FontWeight.bold,
                    fontSize: textin),
              ),
              Spacer(
                flex: 1,
              ),
              Container(
                // height: SizeConfig.heightMultiplier * 30,
                width: SizeConfig.widthMultiplier * 20,
                child: AppTextField(
                    focusEnableColor:
                      orderlist[index]['colortext'],
                      hintName: '${orderlist[index]['kilo']}',


                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
