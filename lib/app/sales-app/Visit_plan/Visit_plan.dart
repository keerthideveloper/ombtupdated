import 'package:intl/intl.dart';
import 'package:ombt/modules/Visit_plan_list.dart';
import 'package:ombt/pageMap.dart';

class Visitplan extends StatefulWidget {
  @override
  _VisitplanState createState() => _VisitplanState();
}

class _VisitplanState extends State<Visitplan> {
  Color textcolor = Color.fromRGBO(255, 133, 77, 1);
  double textsize = appNormalTextSize;
  DateTime selectedDate = DateTime.now();
  TimeOfDay timein = TimeOfDay.now();
  TextEditingController datatimein = TextEditingController();
  TextEditingController locationin = TextEditingController();
  Future<void> _selectDateTime(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    final TimeOfDay time = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.fromDateTime(selectedDate ?? DateTime.now()),
    );
    if (picked != null && time != null)
      setState(() {
        selectedDate = picked;
        timein = time;
        datatimein.text =
            "${DateFormat('dd/MMM/yyyy').format(selectedDate)} ${timein.format(context)}";
      });
  }

  @override
  Widget build(BuildContext context) {
    datatimein.text =
        "${DateFormat('dd/MMM/yyyy').format(selectedDate)}   ${timein.format(context)}";
    locationin.text = 'Baner, Pune';
    // var widthin = MediaQuery.of(context).size.width;
    // var heightin = MediaQuery.of(context).size.height;
    return CommonBody(
      appTitle: "Visit Plan",
      appLogo: 'salesLogo.png',
      onPressed: () => Navigator.of(context).pop(),
      child: Padding(
        padding: const EdgeInsets.all(0),
        child: Column(
          children: [
            Flexible(
              child: TextFormField(
                controller: locationin,
                style: TextStyle(color: Colors.green),
                readOnly: true,
                decoration: InputDecoration(
                    prefixIcon: Icon(
                      Icons.place,
                      color: Colors.green,
                    ),
                    prefixText: 'GPS Destination : ',
                    prefixStyle: TextStyle(
                        color: Colors.green, fontSize: appNormalTextSize),
                    focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.green)),
                    disabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.green)),
                    border: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.green)),
                    enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.green))),
                onTap: () {
                  // setState(() {
                  //   _selectDateTime(context);
                  //   FocusScope.of(context).requestFocus(new FocusNode());
                  // });
                },
              ),
              flex: 3,
            ),
            Spacer(
              flex: 1,
            ),
            Flexible(
              child: TextFormField(
                controller: datatimein,
                style: TextStyle(color: Colors.green),
                readOnly: true,
                decoration: InputDecoration(
                    prefixIcon: Icon(
                      Icons.date_range,
                      color: Colors.green,
                    ),
                    focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.green)),
                    disabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.green)),
                    border: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.green)),
                    enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.green))),
                onTap: () {
                  setState(() {
                    _selectDateTime(context);
                    FocusScope.of(context).requestFocus(new FocusNode());
                  });
                },
              ),
              flex: 3,
            ),
            Spacer(
              flex: 1,
            ),
            Expanded(
              child: outcustomer(),
              flex: 23,
            )
          ],
        ),
      ),
    );
  }

  Widget outcustomer() {
    return OrientationBuilder(builder: (context, orientation) {
      bool chance = orientation == Orientation.landscape;
      return GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            childAspectRatio: chance ? 2.3 : 1,
            mainAxisSpacing: 2),
        itemCount: visitplanlist.length,
        itemBuilder: (context, index) {
          return customercard(index, chance);
        },
      );
    });
  }

  Widget customercard(index, chance) {
    return Stack(
      children: [
        AppCard(
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Flexible(
                  child: Text(
                    visitplanlist[index]['customname'],
                    style: TextStyle(fontSize: textsize, color: textcolor),
                  ),
                  flex: 3,
                ),
                Spacer(
                  flex: 1,
                ),
                Flexible(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Flexible(
                        child: Image.asset(
                          'assets/icons/Place Icon.png',
                          color: Colors.deepPurple,
                        ),
                        flex: 2,
                      ),
                      Spacer(
                        flex: 1,
                      ),
                      Flexible(
                        child: Wrap(
                          children: [
                            Text(
                              visitplanlist[index]['address'],
                              style: TextStyle(fontSize: textsize),
                            ),
                          ],
                        ),
                        flex: 8,
                      ),
                    ],
                  ),
                  flex: 7,
                ),
                Spacer(
                  flex: 1,
                ),
              ],
            ),
          ),
        ),
        Positioned(
          bottom: 0,
          right: 10,
          top: chance ? 50 : 100,
          child: Row(
            children: [
              Image.asset(
                'assets/icons/phone.png',
              ),
              SizedBox(
                width: 20.0,
              ),
              Container(
                  width: 40,
                  height: 40,
                  child: FloatingActionButton(
                      heroTag: null,
                      backgroundColor: Color.fromRGBO(109, 195, 135, 1),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => Visitplanmap(
                                      widget.key,
                                      index,
                                    )));
                      },
                      elevation: 3,
                      child: Icon(
                        Icons.navigation,
                        color: Colors.white,
                        size: 20,
                      ))),
            ],
          ),
        )
      ],
    );
  }
}
