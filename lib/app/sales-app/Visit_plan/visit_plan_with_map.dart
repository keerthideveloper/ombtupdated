import 'package:location/location.dart';
import 'package:ombt/modules/Visit_plan_list.dart';
import 'package:ombt/pageMap.dart';

class Visitplanmap extends StatefulWidget {
  final int index;
  Visitplanmap(Key key, this.index) : super(key: key);
  @override
  _VisitplanmapState createState() => _VisitplanmapState();
}

class _VisitplanmapState extends State<Visitplanmap> {
  LatLng _initialPostion = LatLng(18.548269, 73.921143);
  Completer<GoogleMapController> _controller = Completer();
  GoogleMapController mapcontroller;
  final Map<String, Marker> _markers = {};

  void getlocation() async {
    LocationData currentlocation;
    mapcontroller = await _controller.future;
    var location = new Location();
    try {
      currentlocation = await location.getLocation();
    } on Exception {
      currentlocation = null;
    }
    mapcontroller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        target: LatLng(currentlocation.latitude, currentlocation.longitude),
        zoom: 25)));

    setState(() {
      _markers.clear();
      final marker = Marker(
        markerId: MarkerId('My Location'),
        position: LatLng(currentlocation.latitude, currentlocation.longitude),
        infoWindow: InfoWindow(title: 'my location'),
      );
      _markers['Current Location'] = marker;
    });
  }

  bool thinker = true;

  @override
  Widget build(BuildContext context) {
    var widthin = MediaQuery.of(context).size.width;
    var heightin = MediaQuery.of(context).size.height;
    return CommonBody(
      appLogo: 'salesLogo.png',
      appTitle: visitplanlist[widget.index]['customname'],
      onPressed: () => Navigator.pop(context),
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: 70,
            ),
            Stack(
              children: [
                Container(
                  width: widthin,
                  height: heightin / 2.5,
                  child: GoogleMap(
                    initialCameraPosition:
                        CameraPosition(target: _initialPostion, zoom: 15),
                    mapType: MapType.normal,
                    onMapCreated: (GoogleMapController controller) {
                      _controller.complete(controller);
                    },
                    myLocationEnabled: true,
                    myLocationButtonEnabled: false,
                    zoomControlsEnabled: false,
                    markers: _markers.values.toSet(),
                  ),
                ),
                Positioned(
                    bottom: 15,
                    right: 15,
                    child: FloatingActionButton(
                      backgroundColor: Colors.white,
                      onPressed: getlocation,
                      child: Icon(
                        Icons.gps_fixed,
                        color: Colors.grey[600],
                      ),
                    ))
              ],
            ),
            thinker
                ? AppCard(
                    child: cardboy(widthin),
                  )
                : SizedBox(
                    height: 0,
                  )
          ],
        ),
      ),
    );
  }

  Widget cardboy(widthin) {
    return Padding(
      padding: const EdgeInsets.all(18.0),
      child: Column(children: [
        Row(children: [
          Text(
            '4 min',
            style: TextStyle(fontSize: 25),
          ),
          SizedBox(width: 15),
          Text(
            '(220 m)',
            style: TextStyle(fontSize: 25),
          )
        ]),
        SizedBox(height: 10),
        Row(children: [
          Flexible(
            child: Wrap(
              children: [Text('${visitplanlist[widget.index]['address']}')],
            ),
            flex: 6,
          ),
          Spacer(
            flex: 1,
          ),
          Flexible(
            child: Image.asset('assets/icons/location.png'),
            flex: 1,
          )
        ]),
        SizedBox(height: 10),
        Container(
          width: widthin,
          child: AppButton(
            onPressed: () {
              setState(() {
                thinker = false;
                showDialog(
                    context: context,
                    builder: (context) {
                      return OrientationBuilder(
                          builder: (context, orientation) {
                        bool judge = orientation == Orientation.landscape;
                        return judge
                            ? responsecard(judge, widthin)
                            : responsecard(judge, widthin);
                      });
                    });
              });
            },
            name: 'END RIDE',
          ),
        )
      ]),
    );
  }

  Widget responsecard(judge, widthin) {
    TextEditingController responcedata = TextEditingController();
    responcedata.text = '';

    return Stack(
      children: [
        Positioned(
            bottom: judge ? 10 : 50,
            top: judge ? 10 : null,
            right: judge ? widthin / 5 : null,
            left: judge ? widthin / 5 : null,
            child: Container(
                width: judge ? widthin / 2 : widthin,
                child: AppCard(
                  child: Padding(
                    padding: EdgeInsets.all(30.0),
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [

                              RaisedButton(
                                padding: EdgeInsets.all(0),
                                color: Colors.white,
                                onPressed: () {
                                  responcedata.text = "Shop Closed";
                                },
                                child: Text(
                                  'Shop Closed',
                                  style: TextStyle(
                                      color:
                                          Color.fromRGBO(109, 195, 135, 1)),
                                ),
                              ),
                              SizedBox(
                                width: SizeConfig.widthMultiplier*3,
                              ),
                              RaisedButton(
                                padding: EdgeInsets.all(0),
                                color: Colors.white,
                                onPressed: () {
                                  responcedata.text = "Credit Issue";
                                },
                                child: Text(
                                  'Credit Issue',
                                  style: TextStyle(
                                      color:
                                          Color.fromRGBO(109, 195, 135, 1)),
                                ),
                              ),
                              SizedBox(
                                width: SizeConfig.widthMultiplier*3,
                              ),
                              RaisedButton(
                                padding: EdgeInsets.all(0),
                                color: Colors.white,
                                onPressed: () {
                                  responcedata.text = "Quality Issue";
                                },
                                child: Text(
                                  'Quality Issue',
                                  style: TextStyle(
                                      color:
                                          Color.fromRGBO(109, 195, 135, 1)),
                                ),
                              ),


                            ],
                          ),
                          SizedBox(
                            height: SizeConfig.heightMultiplier * 2,
                          ),
                          Row(
                            children: [
                              RaisedButton(
                                padding: EdgeInsets.all(4),
                                color: Colors.white,
                                onPressed: () {
                                  responcedata.text = "Tele Caller Follow Up";
                                },
                                child: Row(
                                  children: [
                                    Text(
                                      'Tele Caller Follow Up',
                                      style: TextStyle(
                                          color: Color.fromRGBO(
                                              109, 195, 135, 1)),
                                    ),
                                    SizedBox(
                                      width: 5.0,
                                    ),
                                    Image.asset('assets/icons/phone.png'),
                                  ],
                                ),
                              ),
                              Flexible(
                                child: Container(),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: SizeConfig.heightMultiplier * 2,
                          ),
                          Container(
                              child: Text(
                            'Resolutions',
                            style: TextStyle(
                                fontSize: SizeConfig.textMultiplier * 2.7),
                          )),
                          SizedBox(
                            height: SizeConfig.heightMultiplier * 2,
                          ),
                          Container(
                              child: TextFormField(
                            readOnly: true,
                            style: TextStyle(color: appTextColor),
                            controller: responcedata,
                            maxLines: 3,
                            decoration: InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Color(0xff58BC7A),
                                  width: 2.0,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Color(0xff58BC7A).withOpacity(0.2),
                                  width: 2.0,
                                ),
                              ),
                            ),
                          )),
                          SizedBox(
                            height: SizeConfig.heightMultiplier * 2,
                          ),
                          Container(
                            width: judge ? widthin * 2 : widthin,
                            child: AppButton(
                              color: Color.fromRGBO(109, 195, 135, 1),
                              name: 'Submit',
                              onPressed: () {
                                // Navigator.of(context).pop();
                                // Scaffold.of(context).showSnackBar(SnackBar(
                                //   content: Text(
                                //       '${responcedata.text} Submitted Successfully'),
                                // ));
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => Salesdash()));
                              },
                            ),
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(
                                judge ? widthin / 6 : widthin / 4,
                                0,
                                judge ? widthin / 6 : widthin / 4,
                                0),
                            height: 5,
                            width: widthin,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                color: Color.fromRGBO(221, 221, 221, 0.8)),
                          )
                        ],
                      ),
                    ),
                  ),
                )))
      ],
    );
  }
}
