import 'package:ombt/modules/order_of_day/Order_list_ofday_details.dart';
import 'package:ombt/modules/order_of_day/Order_list_ofday_details_popup.dart';
import 'package:ombt/pageMap.dart';

// ignore: must_be_immutable
class Popupin extends StatefulWidget {
  int index;
  double fontsize;
  Popupin(Key key, this.index, this.fontsize) : super(key: key);
  @override
  _PopupinState createState() => _PopupinState();
}

class _PopupinState extends State<Popupin> {
  TextEditingController responcedata = TextEditingController();
  @override
  Widget build(BuildContext context) {
    // var heightin = MediaQuery.of(context).size.height;
    return GestureDetector(
      child: Text(
        orderlistdetails[widget.index]['orderid'],
        style: TextStyle(
            fontSize: widget.fontsize,
            decoration: TextDecoration.underline,
            color: orderlistdetails[widget.index]['text2color']),
      ),
      onTap: () {
        showDialog(
            context: context,
            builder: (context) {
              return OrientationBuilder(builder: (context, orientation) {
                bool judge = orientation == Orientation.landscape;
                return judge ? cardboy(judge) : cardboy(judge);
              });
            });
      },
    );
  }

  Widget cardboy(judge) {
    var widthin = MediaQuery.of(context).size.width;
    double headcard = SizeConfig.textMultiplier * 2.5;
    return Stack(
      children: [
        Positioned(
            bottom: judge ? 10 : 50,
            top: judge ? 10 : null,
            right: judge ? widthin / 5 : null,
            left: judge ? widthin / 5 : null,
            child: Container(
              width: judge ? widthin / 2 : widthin,
              child: Card(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30)),
                child: Padding(
                  padding: const EdgeInsets.all(30.0),
                  child: SingleChildScrollView(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Text(
                                'Order ID: ${orderlistdetails[widget.index]['orderid']}',
                                style: TextStyle(fontSize: headcard),
                              ),
                              Spacer(flex: 1),
                              Text(
                                'Date: 21/05/2020',
                                style: TextStyle(fontSize: headcard),
                              )
                            ],
                          ),
                          Divider(
                            color: Colors.yellow,
                            thickness: 1,
                          ),
                          Row(children: [
                            Text('Product'),
                            Spacer(flex: 2),
                            Text('Quantity'),
                            Spacer(flex: 1),
                            Text('Order Value')
                          ]),
                          Divider(
                            color: Colors.yellow,
                            thickness: 1,
                          ),
                          for (var item in detailspopup)
                            Padding(
                              padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                              child: Row(
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  ImageText(widget.key, item['orderimage'],
                                      item['orderitem']),
                                  Card(
                                    child: Container(
                                        padding: EdgeInsets.all(10),
                                        child: Text("${item['quantity']} kg")),
                                  ),
                                  Text('Rs. ${item['ordervalue']}'),
                                ],
                              ),
                            ),
                          Row(
                            children: [
                              GestureDetector(
                                child: Text(
                                  'Total Order',
                                  style: TextStyle(fontSize: headcard),
                                ),
                                onTap: () {
                                  Navigator.pop(context);
                                  showDialog(
                                      context: context,
                                      builder: (context) {
                                        return OrientationBuilder(
                                            builder: (context, orientataion) {
                                              bool judge = orientataion ==
                                                  Orientation.landscape;
                                              return judge
                                                  ? responsecard(judge, widthin)
                                                  : responsecard(judge, widthin);
                                            });
                                      });
                                },
                              ),
                              Spacer(flex: 1),
                              Text('230 kg',
                                  style: TextStyle(fontSize: headcard)),
                              Spacer(flex: 1),
                              InkWell(
                                onTap: () {
                                  Navigator.pop(context);
                                },
                                child: Text('Rs. 4500',
                                    style: TextStyle(fontSize: headcard)),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          Container(
                            width: judge ? widthin * 2 : widthin,
                            child: AppButton(
                              color: Color.fromRGBO(109, 195, 135, 1),
                              onPressed: () {
                                Navigator.pop(context);
                                showDialog(
                                    context: context,
                                    builder: (context) {
                                      return OrientationBuilder(
                                          builder: (context, orientataion) {
                                            bool judge = orientataion ==
                                                Orientation.landscape;
                                            return judge
                                                ? responsecard(judge, widthin)
                                                : responsecard(judge, widthin);
                                          });
                                    });
                              },
                              name: 'Submit',
                            ),
                          ),
                          SizedBox(
                            height: 25,
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(
                                judge ? widthin / 6 : widthin / 4,
                                0,
                                judge ? widthin / 6 : widthin / 4,
                                0),
                            height: 5,
                            width: widthin,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                color: Color.fromRGBO(221, 221, 221, 0.8)),
                          )
                        ]),
                  ),
                ),
              ),
            ))
      ],
    );
  }

  Widget responsecard(judge, widthin) {
    responcedata.text = '';

    return Stack(
      children: [
        Positioned(
            bottom: judge ? 10 : 50,
            top: judge ? 10 : null,
            right: judge ? widthin / 5 : null,
            left: judge ? widthin / 5 : null,
            child: Container(
                width: judge ? widthin / 2 : widthin,
                child: Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30)),
                  child: Padding(
                    padding: EdgeInsets.all(30.0),
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(

                            children: [
                              RaisedButton(
                                padding: EdgeInsets.all(0),
                                color: Colors.white,
                                onPressed: () {
                                  responcedata.text = "Shop Closed";
                                },
                                child: Text(
                                  'Shop Closed',
                                  style: TextStyle(
                                      color:
                                      Color.fromRGBO(109, 195, 135, 1)),
                                ),
                              ),
                              SizedBox(
                                width: SizeConfig.widthMultiplier*3,
                              ),
                              RaisedButton(
                                padding: EdgeInsets.all(0),
                                color: Colors.white,
                                onPressed: () {
                                  responcedata.text = "Credit Issue";
                                },
                                child: Text(
                                  'Credit Issue',
                                  style: TextStyle(
                                      color:
                                      Color.fromRGBO(109, 195, 135, 1)),
                                ),
                              ),
                              SizedBox(
                                width: SizeConfig.widthMultiplier*3,
                              ),
                              RaisedButton(
                                padding: EdgeInsets.all(0),
                                color: Colors.white,
                                onPressed: () {
                                  responcedata.text = "Quality Issue";
                                },
                                child: Text(
                                  'Quality Issue',
                                  style: TextStyle(
                                      color:
                                      Color.fromRGBO(109, 195, 135, 1)),
                                ),
                              )
                            ],
                          ),
                          SizedBox(
                            height: SizeConfig.heightMultiplier*2,

                          ),
                          Container(
                            width: judge ? widthin / 2 : widthin / 2,
                            child: RaisedButton(
                              padding: EdgeInsets.all(0),
                              color: Colors.white,
                              onPressed: () {
                                responcedata.text = "Tele Caller Follow up";
                              },
                              child: Row(
                                mainAxisAlignment:
                                MainAxisAlignment.spaceAround,
                                children: [
                                  Text('Tele Caller Follow up',
                                      style: TextStyle(
                                          color: Color.fromRGBO(
                                              109, 195, 135, 1))),
                                  Container(
                                      width: SizeConfig.imageSizeMultiplier * 5,
                                      height:
                                      SizeConfig.imageSizeMultiplier * 5,
                                      child: Image.asset(
                                          'assets/icons/phone_g.png'))
                                ],
                              ),
                            ),
                          ),
                          SizedBox(
                            height: SizeConfig.heightMultiplier * 2,
                          ),
                          Container(
                              child: Text(
                                'Resolutions',
                                style: TextStyle(
                                    fontSize: SizeConfig.textMultiplier * 2.7),
                              )),
                          SizedBox(
                            height: SizeConfig.heightMultiplier * 2,
                          ),
                          Container(
                              child: TextFormField(
                                readOnly: true,
                                controller: responcedata,
                                maxLines: 3,
                                decoration: InputDecoration(
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Color(0xff58BC7A),
                                      width: 2.0,
                                    ),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Color(0xff58BC7A).withOpacity(0.2),
                                      width: 2.0,
                                    ),
                                  ),
                                ),
                              )),
                          SizedBox(
                            height: SizeConfig.heightMultiplier * 2,
                          ),
                          Container(
                            width: judge ? widthin * 2 : widthin,
                            child: AppButton(

                              onPressed: () {
                                Navigator.of(context).pop();
                                Scaffold.of(context).showSnackBar(SnackBar(
                                  content: Text(
                                      '${responcedata.text} Submitted Successfully'),
                                ));
                              },
                              name:'Submit',
                            ),
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(
                                judge ? widthin / 6 : widthin / 4,
                                0,
                                judge ? widthin / 6 : widthin / 4,
                                0),
                            height: 5,
                            width: widthin,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                color: Color.fromRGBO(221, 221, 221, 0.8)),
                          )
                        ],
                      ),
                    ),
                  ),
                )))
      ],
    );
  }
}
