import 'package:intl/intl.dart';
import 'package:ombt/modules/order_of_day/Order_of_day_list.dart';
import 'package:ombt/pageMap.dart';

class Orderofday extends StatefulWidget {
  @override
  _OrderofdayState createState() => _OrderofdayState();
}

class _OrderofdayState extends State<Orderofday> {
  bool choice;
  Color textcolor = Colors.white;
  double textsizein = SizeConfig.textMultiplier;
  DateTime selectedDate = DateTime.now();
  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

  @override
  Widget build(BuildContext context) {
    return CommonBody(
      appLogo: 'salesLogo.png',
      appTitle: 'Today Order Status',
      onPressed: () => Navigator.of(context).pop(),
      child: Column(
        children: [
          Flexible(
            child: Row(
              children: [
                Text(
                  'Date: ',
                  style: TextStyle(
                      fontWeight: FontWeight.bold, fontSize: appNormalTextSize),
                ),
                GestureDetector(
                  child: Text(
                    '${DateFormat('dd-MM-yyyy').format(selectedDate.toLocal())}',
                    style: TextStyle(fontSize: appSubHeadingFontSize),
                  ),
                  onTap: () => _selectDate(context),
                ),
              ],
            ),
            flex: 2,
          ),
          Spacer(
            flex: 1,
          ),
          Flexible(
            child: Row(
              children: [
                Text("Customer's",
                    style: TextStyle(fontSize: appSubHeadingFontSize)),
              ],
            ),
            flex: 2,
          ),
          Spacer(
            flex: 1,
          ),
          Flexible(
            child: Divider(
              color: Colors.orange,
              thickness: 1,
            ),
            flex: 1,
          ),
          Spacer(
            flex: 1,
          ),
          Flexible(
            child: orderday(),
            flex: 25,
          )
        ],
      ),
    );
  }

  Widget orderday() {
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2, childAspectRatio: 1.5 / 1.1),
      itemCount: customerday.length,
      itemBuilder: (context, index) {
        choice = customerday[index]['judge'];
        return ordercard(index);
      },
    );
  }

  Widget ordercard(index) {
    return Hero(
      tag: "num$index",
      child: AppCard(
        onTap: () => Navigator.push(
            (context),
            MaterialPageRoute(
                builder: (context) => OrderDetails(
                      num: index,
                    ))),
        color: customerday[index]['colorcard'],
        child: Padding(
          padding: EdgeInsets.all(8),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Flexible(
                child: Text(
                  customerday[index]['customername'],
                  style: TextStyle(
                      fontSize: appSubHeadingFontSize, color: textcolor),
                ),
                flex: 3,
              ),
              Flexible(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    choice
                        ? Flexible(
                            child: Row(
                              children: [
                                Flexible(
                                  child: Text(
                                    'Rs. ${customerday[index]['rupees']}',
                                    style: TextStyle(
                                        fontSize: appNormalTextSize,
                                        color: textcolor),
                                  ),
                                  flex: 3,
                                ),
                                Spacer(
                                  flex: 1,
                                )
                              ],
                            ),
                            flex: 8,
                          )
                        : Flexible(
                            child: Row(
                              children: [
                                Flexible(
                                  child: Container(
                                      width: 20,
                                      height: 20,
                                      child: Image.asset(
                                          'assets/icons/error-1.png')),
                                  flex: 3,
                                ),
                                Spacer(
                                  flex: 1,
                                ),
                                Flexible(
                                  child: Text(
                                    'Not Ordered',
                                    style: TextStyle(
                                        fontSize: appNormalTextSize,
                                        color: textcolor),
                                  ),
                                  flex: 10,
                                ),
                              ],
                            ),
                            flex: 8,
                          ),
                    Spacer(
                      flex: 1,
                    ),
                    Flexible(
                      child: Container(
                        height: 30,
                        child: VerticalDivider(
                          color: textcolor,
                          thickness: 1,
                        ),
                      ),
                      flex: 1,
                    ),
                    Spacer(
                      flex: 1,
                    ),
                    Flexible(
                      child: Container(
                          width: 22,
                          height: 22,
                          child: Image.asset('assets/icons/phone-1.png')),
                      flex: 2,
                    )
                  ],
                ),
                flex: 1,
              )
            ],
          ),
        ),
      ),
    );
  }
}
