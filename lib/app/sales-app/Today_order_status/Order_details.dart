import 'package:ombt/modules/order_of_day/Order_list_ofday_details.dart';
import 'package:ombt/modules/order_of_day/Order_of_day_list.dart';
import 'package:ombt/pageMap.dart';

// ignore: must_be_immutable
class OrderDetails extends StatefulWidget {
  int num;
  OrderDetails({Key key, this.num}) : super(key: key);
  @override
  _OrderDetailsState createState() => _OrderDetailsState();
}

class _OrderDetailsState extends State<OrderDetails> {
  double textsizein = SizeConfig.textMultiplier;

  @override
  Widget build(BuildContext context) {
    return CommonBody(
      appLogo: 'salesLogo.png',
      appTitle: 'Order Details',
      onPressed: () => Navigator.pop(context),
      child: Column(
        children: [
          Spacer(
            flex: 1,
          ),
          Flexible(
            child: Row(
              children: [
                CircleAvatar(
                  backgroundColor: Color.fromRGBO(237, 237, 237, 1),
                  radius: 25,
                  child: Container(
                      width: 30,
                      height: 30,
                      child: Image.asset('assets/icons/user.png')),
                ),
                Spacer(
                  flex: 1,
                ),
                Text(
                  customerday[widget.num]['customername'],
                  style: TextStyle(fontSize: appSubHeadingFontSize),
                ),
                Spacer(
                  flex: 5,
                ),
              ],
            ),
            flex: 2,
          ),
          Flexible(
            child: Divider(
              color: Colors.green,
              thickness: 1,
            ),
            flex: 1,
          ),
          Spacer(
            flex: 1,
          ),
          Flexible(
            child: orderdetailin(),
            flex: 18,
          )
        ],
      ),
    );
  }

  Widget orderdetailin() {
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2, childAspectRatio: 1.3),
      itemCount: orderlistdetails.length,
      itemBuilder: (context, index) {
        return cardmaking(index);
      },
    );
  }

  Widget cardmaking(index) {
    bool judge = MediaQuery.of(context).orientation == Orientation.landscape;
    double heighton = SizeConfig.heightMultiplier;
    return Hero(
      tag: "num$index",
      child: AppCard(
        onTap: () {
          // Navigator.push(context,
          //     MaterialPageRoute(builder: (context) => Popupin(index: index)));
        },
        child: Padding(
            padding: EdgeInsets.all(8),
            child: orderlistdetails[index]['makechange']
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Date      :',
                            style: TextStyle(
                                fontSize: appNormalTextSize,
                                color: orderlistdetails[index]['text1color']),
                          ),
                          SizedBox(
                            height: judge ? (heighton * 4) : heighton,
                          ),
                          Text(
                            'Order Id :',
                            style: TextStyle(
                                fontSize: appNormalTextSize,
                                color: orderlistdetails[index]['text2color']),
                          ),
                          SizedBox(
                            height: judge ? (heighton * 4) : heighton,
                          ),
                          Text(
                            'Quantity :',
                            style: TextStyle(
                                fontSize: appNormalTextSize,
                                color: orderlistdetails[index]['text3color']),
                          ),
                        ],
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            orderlistdetails[index]['orderDate'],
                            style: TextStyle(
                                fontSize: appNormalTextSize,
                                color: orderlistdetails[index]['text1color']),
                          ),
                          SizedBox(
                            height: judge ? (heighton * 4) : heighton,
                          ),
                          Popupin(widget.key, index, appNormalTextSize),
                          SizedBox(
                            height: judge ? (heighton * 4) : heighton,
                          ),
                          Text(
                            '${orderlistdetails[index]['quantity']} kg',
                            style: TextStyle(
                                fontSize: appNormalTextSize,
                                color: orderlistdetails[index]['text3color']),
                          )
                        ],
                      ),
                    ],
                  )
                : Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      SizedBox(
                        height: judge ? (heighton * 4) : heighton,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Date      :',
                            style: TextStyle(
                                fontSize: textsizein * 2,
                                color: orderlistdetails[index]['text1color']),
                          ),
                          SizedBox(
                            width: judge ? (heighton * 4) : heighton,
                          ),
                          Text(
                            orderlistdetails[index]['orderDate'],
                            style: TextStyle(
                                fontSize: textsizein * 2.5,
                                color: orderlistdetails[index]['text1color']),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: judge ? (heighton * 4) : heighton,
                      ),
                      Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Reason',
                              style: TextStyle(
                                  fontSize: textsizein * 2,
                                  color: orderlistdetails[index]['text3color']),
                            ),
                            SizedBox(
                              height: judge ? (heighton * 4) : heighton,
                            ),
                            Text(
                              'No Order Today',
                              style: TextStyle(
                                  fontSize: textsizein * 2,
                                  color: orderlistdetails[index]['text3color']),
                            ),
                          ])
                    ],
                  )),
      ),
    );
  }
}
