import 'package:ombt/modules/Outstanding_payments.dart';
import 'package:ombt/pageMap.dart';

// ignore: must_be_immutable
class Outstandpayment extends StatefulWidget {
  var outstandpay;
  Outstandpayment({Key key, this.outstandpay}) : super(key: key);
  @override
  _OutstandpaymentState createState() => _OutstandpaymentState();
}

class _OutstandpaymentState extends State<Outstandpayment> {
  var textin = SizeConfig.textMultiplier;
  @override
  Widget build(BuildContext context) {
    var widthin = MediaQuery.of(context).size.width;
    return CommonBody(
        appLogo: 'salesLogo.png',
        appTitle: 'Outstanding Payment',
        onPressed: () => Navigator.of(context).pop(),
        child: Padding(
          padding: const EdgeInsets.all(0),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  widget.outstandpay['customer'],
                  style: TextStyle(fontSize: appSubHeadingFontSize),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'Baner',
                  style: TextStyle(fontSize: appSubHeadingFontSize),
                ),
                SizedBox(
                  height: 10,
                ),
                Divider(color: Colors.green, thickness: 1),
                SizedBox(
                  height: 5,
                ),
                Container(
                    width: widthin, height: 210, child: outdatatable(widthin)),
                nextcontent1(),
                nextcontent2(),
                SizedBox(
                  height: 15,
                ),
                nextcontent3(),
                SizedBox(
                  height: 15,
                ),
                nextcontent4()
              ],
            ),
          ),
        ));
  }

  int _radiobutton = 0;
  void choiceradio(int value) {
    setState(() {
      _radiobutton = value;
    });
  }

  Widget outdatatable(widthin) {
    TextStyle customize = TextStyle(
        color: Color.fromRGBO(88, 188, 122, 1), fontSize: appNormalTextSize);
    TextStyle customize1 = TextStyle(
        color: Color.fromRGBO(248, 186, 0, 1), fontSize: appNormalTextSize);
    return ListView.builder(
        physics: NeverScrollableScrollPhysics(),
        itemCount: outpay.length,
        itemBuilder: (context, outitem) {
          return Container(
            decoration: BoxDecoration(
                border: Border(bottom: BorderSide(color: Colors.yellow))),
            child: ListTile(
              dense: true,
              contentPadding: EdgeInsets.all(2),
              leading: Radio(
                value: outitem,
                groupValue: _radiobutton,
                onChanged: choiceradio,
              ),
              title: Row(
                mainAxisAlignment:MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Order ID',
                    style: customize,
                  ),

                  Text('Date', style: customize),

                  Text('Order Value', style: customize),
                ],
              ),
              subtitle: Row(
                mainAxisAlignment:MainAxisAlignment.spaceBetween,
                children: [
                  Text(outpay[outitem].orderid, style: customize1),

                  Text(outpay[outitem].orderdate, style: customize),

                  Text('${outpay[outitem].ordervalue}', style: customize),
                ],
              ),
            ),
          );
        });
  }

  Widget nextcontent1() {
    return Row(
      children: [
        Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  fit: BoxFit.contain,
                  image: AssetImage('assets/icons/cash.png'))),
          width: 70,
          height: 70,
        ),
        Container(child: Text('Cash Collected:',style: TextStyle(fontSize:appNormalTextSize),)),
       
        Container(
           padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
          child: Card(
            elevation: 3,
            shadowColor: Colors.green,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
              child: Container(
                child: Center(
                  child: Text('775'),
                ),
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget nextcontent2() {
    return Row(
      children: [
        Text('Total Amount To be Paid: Rs 775',style: TextStyle(fontSize:appNormalTextSize)),
        Spacer(flex: 1),
        Row(children: [
          Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.contain,
                    image: AssetImage('assets/icons/fast.png'))),
            width: 25,
            height: 25,
          ),
          SizedBox(
            width: 10,
          ),
          Container(child: Text('1:55 Pm',style: TextStyle(fontSize:appNormalTextSize))),

        ])
      ],
    );
  }

  bool _enable1 = false;
  Widget nextcontent3() {
    return Row(
      children: [
        Text('Payment Received',style: TextStyle(fontSize:appNormalTextSize)),
        SizedBox(
          width: 10,
        ),
        CustomSwitch(
          value: _enable1,
          leftyes: 'Yes',
          rightno: 'No',
          onChanged: (bool val) {
            setState(() {
              _enable1 = val;
            });
          },
        ),
      ],
    );
  }

  bool _enable2 = false;
  Widget nextcontent4() {
    return Row(
      children: [
        Row(
          children: [
            Text(
              'Pending Crates : ',
              style: TextStyle(color: Color.fromRGBO(255, 133, 77, 1),fontSize:appNormalTextSize),
            ),
            SizedBox(
              width: 10,
            ),
            Container(
              width: 50,
              height: 35,
              child: Card(
                elevation: 3,
                shadowColor: Color.fromRGBO(255, 133, 77, 1),
                child: Center(
                    child: Text(
                  '5',
                  style: TextStyle(color: Color.fromRGBO(255, 133, 77, 1)),
                )),
              ),
            ),
          ],
        ),
        Spacer(flex: 1),
        Row(
          children: [
            Text(
              'Reminder',
              style: TextStyle(color: Color.fromRGBO(255, 133, 77, 1),fontSize:appNormalTextSize),
            ),
            SizedBox(
              width: 10,
            ),
            CustomSwitch(
              value: _enable2,
              leftyes: 'Yes',
              rightno: 'No',
              swhcolorbodyes: Color.fromRGBO(243, 101, 35, 1),
              swhcoloritemyes: Color.fromRGBO(255, 133, 77, 1),
              onChanged: (bool val) {
                setState(() {
                  _enable2 = val;
                });
              },
            ),
          ],
        )
      ],
    );
  }
}
