import 'package:intl/intl.dart';
import 'package:ombt/modules/Outstanding_list.dart';
import 'package:ombt/pageMap.dart';

class Outstanding extends StatefulWidget {
  @override
  _OutstandingState createState() => _OutstandingState();
}

class _OutstandingState extends State<Outstanding> {
  Color textcolor = Colors.white;
  double textsize = 11;
  DateTime selectedDate = DateTime.now();
  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

  @override
  Widget build(BuildContext context) {
    return CommonBody(
      appLogo: 'salesLogo.png',
      appTitle: "Outstanding Customer's",
      onPressed: () => Navigator.of(context).pop(),
      child: Column(
        children: [
          Flexible(
            child: Row(
              children: [
                Text(
                  '  Date : ',
                  style: TextStyle(fontSize: appSubHeadingFontSize),
                ),
                GestureDetector(
                  child: Text(
                    '${DateFormat('dd-MM-yyyy').format(selectedDate.toLocal())}',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: appSubHeadingFontSize),
                  ),
                  onTap: () => _selectDate(context),
                ),
              ],
            ),
            flex: 2,
          ),
          Spacer(
            flex: 1,
          ),
          Flexible(
            child: outcustomer(),
            flex: 18,
          )
        ],
      ),
    );
  }

  Widget outcustomer() {
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2, childAspectRatio: 1.1, mainAxisSpacing: 2),
      itemCount: outstanding.length,
      itemBuilder: (context, index) {
        return customercard(index);
      },
    );
  }

  Widget customercard(index) {
    return Stack(
      children: [
        AppCard(
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => Outstandpayment(
                          outstandpay: outstanding[index],
                        )));
          },
          color: Color.fromRGBO(255, 133, 77, 1),
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Flexible(
                  child: Text(
                    outstanding[index]['customer'],
                    style: TextStyle(
                        fontSize: appNormalTextSize, color: textcolor),
                  ),
                  flex: 2,
                ),
                Flexible(
                  child: Row(
                    children: [
                      Flexible(
                        child: Text(
                          'Payment Due:',
                          style: TextStyle(
                              fontSize: appNormalTextSize, color: textcolor),
                        ),
                        flex: 8,
                      ),
                      Spacer(
                        flex: 1,
                      ),
                      Flexible(
                        child: Text(
                          'Rs. ${outstanding[index]['payment']}',
                          style: TextStyle(
                              fontSize: appNormalTextSize, color: textcolor),
                        ),
                        flex: 6,
                      ),
                    ],
                  ),
                  flex: 1,
                ),
                Flexible(
                  child: Row(
                    children: [
                      Flexible(
                        child: Text(
                          'Due Crates:',
                          style: TextStyle(
                              fontSize: appNormalTextSize, color: textcolor),
                        ),
                        flex: 8,
                      ),
                      Spacer(
                        flex: 1,
                      ),
                      Flexible(
                        child: Text(
                          outstanding[index]['crates'],
                          style: TextStyle(
                              fontSize: appNormalTextSize, color: textcolor),
                        ),
                        flex: 3,
                      ),
                    ],
                  ),
                  flex: 1,
                ),
                Spacer(
                  flex: 1,
                ),
              ],
            ),
          ),
        ),
        Positioned(
          bottom: 0,
          right: 0,
          child: Container(
              width: 50,
              height: 50,
              child: Card(
                  elevation: 3,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: FittedBox(
                        child: Image.asset('assets/icons/phone_g.png')),
                  ))),
        )
      ],
    );
  }
}
