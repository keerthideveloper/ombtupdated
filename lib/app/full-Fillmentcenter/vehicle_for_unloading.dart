import 'package:ombt/modules/vehicle_unloading_list.dart';
import 'package:ombt/pageMap.dart';

class unloading extends StatefulWidget {
  @override
  _unloadingState createState() => _unloadingState();
}

class _unloadingState extends State<unloading> {
  @override
  Widget build(BuildContext context) {
    return CommonBody(
      appTitle: "Select Vehicle for Unloading",
      onPressed: () => Navigator.pop(context),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Image.asset(
                    'assets/icons/drive.png',
                    width: 30,
                    height: 30,
                  ),
                  SizedBox(width: 10),
                  Text(
                    "MH 14 PB 1234",
                    style: TextStyle(fontSize: appSubHeadingFontSize),
                  ),
                ],
              ),
              Row(
                children: [
                  Image.asset(
                    'assets/icons/date.png',
                    width: 30,
                    height: 30,
                  ),
                  SizedBox(width: 10),
                  Text(
                    "21/August/2020",
                    style: TextStyle(fontSize: appSubHeadingFontSize),
                  ),
                  SizedBox(width: 5),
                ],
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: [
                  Image.asset(
                    'assets/icons/usergreen.png',
                    width: 30,
                    height: 30,
                  ),
                  SizedBox(width: 10),
                  Text(
                    "Babu Bhal",
                    style: TextStyle(fontSize: appSubHeadingFontSize),
                  ),
                ],
              ),
              Row(
                children: [
                  Image.asset(
                    'assets/icons/phone.png',
                    width: 30,
                    height: 30,
                  ),
                  SizedBox(width: 10),
                  Text(
                    "+91 98765420898",
                    style: TextStyle(fontSize: appSubHeadingFontSize),
                  ),
                ],
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: [
                  Image.asset(
                    'assets/icons/crate2.png',
                    width: 30,
                    height: 30,
                  ),
                  SizedBox(width: 10),
                  Text(
                    "10",
                    style: TextStyle(fontSize: appSubHeadingFontSize),
                  ),
                ],
              ),
              Row(
                children: [
                  Image.asset(
                    'assets/icons/location.png',
                    width: 30,
                    height: 30,
                  ),
                  SizedBox(width: 10),
                  Text(
                    "Banner,Pune",
                    style: TextStyle(fontSize: appSubHeadingFontSize),
                  ),
                  SizedBox(width: 30),
                ],
              )
            ],
          ),
          Container(
            margin: EdgeInsets.fromLTRB(3, 0, 3, 0),
            child: Divider(
              color: Color.fromRGBO(109, 195, 135, 1),
              thickness: 1,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Container(
            height: SizeConfig.heightMultiplier * 48,
            child: ListView.builder(
                itemCount: 3,
                itemBuilder: (context, index) {
                  return fruitlist(index);
                }),
          ),
        ],
      ),
    );
  }

  Widget fruitlist(index) {
    double textsizes = 2;
    return SingleChildScrollView(
      child: AppCard(
        // elevation: 3.0,
        child: Padding(
          padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
          child: Column(
            children: <Widget>[
              IntrinsicHeight(
                child: Row(
                  children: <Widget>[
                    Spacer(
                      flex: 1,
                    ),
                    Container(
                      height: SizeConfig.heightMultiplier * 5,
                      width: SizeConfig.widthMultiplier * 10,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: AssetImage('${select[index]['image']}'),
                        ),
                      ),
                    ),
                    Spacer(
                      flex: 5,
                    ),
                    Column(
                      children: <Widget>[
                        Text(
                          "wt.at FC",
                          style: TextStyle(fontSize: appNormalTextSize),
                        ),
                        SizedBox(
                          height: SizeConfig.heightMultiplier * 1,
                        ),
                        Row(
                          children: <Widget>[
                            Text("150 kg",
                                style: TextStyle(fontSize: appNormalTextSize)),
                          ],
                        ),
                      ],
                    ),
                    Spacer(
                      flex: 5,
                    ),
                    VerticalDivider(
                      color: Color.fromRGBO(109, 195, 135, 1),
                      thickness: 1,
                    ),
                    Spacer(
                      flex: 1,
                    ),
                    Container(
                      child: CircleAvatar(
                        backgroundColor: Color.fromRGBO(242, 242, 242, 1),
                        radius: 20,
                        child: Container(
                            width: 20,
                            height: 20,
                            child: Image.asset('assets/icons/user.png')),
                      ),
                    ),
                    Spacer(
                      flex: 1,
                    ),
                    Text('${select[index]['farname']}', style: TextStyle(color:Colors.black,fontSize: appNormalTextSize)),
                    Spacer(
                      flex: 1,
                    ),
                  ],
                ),
              ),
              Row(
                children: <Widget>[
                  Spacer(
                    flex: 7,
                  ),
                  Text(
                    "Manchar",
                    style: TextStyle(fontSize: appNormalTextSize),
                  ),
                  Spacer(
                    flex: 1,
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Spacer(
                    flex: 1,
                  ),
                  Text('${select[index]['names']}',
                      style: TextStyle(fontSize: appNormalTextSize)),
                  Spacer(
                    flex: 8,
                  ),
                  Text('MOQ = 20kgs',
                      style: TextStyle(fontSize: appNormalTextSize)),
                  Spacer(
                    flex: 40,
                  ),
                ],
              ),
              SizedBox(
                height: SizeConfig.heightMultiplier * 2,
              ),
              Row(
                children: <Widget>[
                  Spacer(
                    flex: 700,
                  ),
                  Container(
                    child: RaisedButton(
                        color: Color.fromRGBO(0, 149, 59, 1),
                        child: Text(
                          "Approved Invoice",
                          style: TextStyle(
                              fontSize: appButtonTextSize, color: Colors.white),
                        ),
                        onPressed: () {}),
                  ),
                  Spacer(
                    flex: 100,
                  ),
                  Container(
                    child: RaisedButton(
                        color: Color.fromRGBO(243, 101, 35, 1),
                        child: Text(
                          "Raise Issue",
                          style: TextStyle(
                              fontSize: appButtonTextSize, color: Colors.white),
                        ),
                        onPressed: () {}),
                  ),
                  Spacer(
                    flex: 1,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
