// import 'package:ombt/modules/Vehicle_list.dart';
// import 'package:ombt/pageMap.dart';
//
// export 'package:ombt/app/full-Fillmentcenter/vehicle_for_unloading.dart';
//
// class reach extends StatefulWidget {
//   @override
//   _reachState createState() => _reachState();
// }
//
// class _reachState extends State<reach> {
//   @override
//   Widget build(BuildContext context) {
//     return CommonBody(
//       appTitle: 'Vehicles with Produce reaching FC',
//       onPressed: () => Navigator.pop(context),
//       child: Column(
//         children: <Widget>[
//           Row(
//             children: <Widget>[
//               Spacer(
//                 flex: 1,
//               ),
//               Container(
//                 height: SizeConfig.heightMultiplier * 3,
//                 width: SizeConfig.widthMultiplier * 9,
//                 decoration: BoxDecoration(
//                   image: DecorationImage(
//                       fit: BoxFit.cover,
//                       image: AssetImage("assets/icons/truck.png")),
//                 ),
//               ),
//               Spacer(
//                 flex: 1,
//               ),
//               Text(
//                 "Vehicle List",
//                 style: TextStyle(fontSize: appSubHeadingFontSize),
//               ),
//               Spacer(
//                 flex: 9,
//               ),
//               Icon(
//                 Icons.date_range,
//                 color: Color.fromRGBO(109, 195, 135, 1),
//               ),
//               Spacer(
//                 flex: 1,
//               ),
//               Text(
//                 "21/August/2020",
//                 style: TextStyle(fontSize: appSubHeadingFontSize),
//               ),
//               Spacer(
//                 flex: 1,
//               ),
//             ],
//           ),
//           Divider(
//             thickness: 1,
//             color: Color.fromRGBO(109, 195, 135, 1),
//           ),
//           SizedBox(
//             height: SizeConfig.heightMultiplier * 10,
//           ),
//           Container(
//             child: reaching(),
//           ),
//         ],
//       ),
//     );
//   }
//
//   Widget reaching() {
//     return SingleChildScrollView(
//       child: Container(
//         height: SizeConfig.heightMultiplier * 40,
//         child: GridView.builder(
//           gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
//             childAspectRatio: 1.2,
//             crossAxisCount: 2,
//             crossAxisSpacing: 7.0,
//             mainAxisSpacing: 7.0,
//           ),
//           itemCount: vehicle.length,
//           itemBuilder: (context, index) {
//             return GestureDetector(
//               onTap: () => Navigator.push(context,
//                   MaterialPageRoute(builder: (context) => unloading())),
//               child: Container(
//                 child: AppCard(
//                   color: vehicle[index]['carcolor'],
//                   child: Padding(
//                     padding: const EdgeInsets.all(8.0),
//                     child: Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                       children: <Widget>[
//                         Column(
//                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                           children: <Widget>[
//                             Icon(
//                               Icons.local_shipping,
//                               color: Colors.white,
//                             ),
//                             Icon(
//                               Icons.perm_identity,
//                               color: Colors.white,
//                             ),
//                             Icon(
//                               Icons.phone_in_talk,
//                               color: Colors.white,
//                             ),
//                             Icon(
//                               Icons.view_comfy,
//                               color: Colors.white,
//                             ),
//                           ],
//                         ),
//                         Column(
//                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                           crossAxisAlignment: CrossAxisAlignment.start,
//                           children: <Widget>[
//                             SizedBox(
//                               height: SizeConfig.heightMultiplier * 0.5,
//                             ),
//                             Text(
//                               '${vehicle[index]['vehno']}',
//                               style: TextStyle(
//                                   color: Colors.white,
//                                   fontSize: appNormalTextSize),
//                             ),
//                             SizedBox(
//                               height: SizeConfig.heightMultiplier * 1.5,
//                             ),
//                             Text(
//                               '${vehicle[index]['drivername']}',
//                               style: TextStyle(
//                                   color: Colors.white,
//                                   fontSize: appNormalTextSize),
//                             ),
//                             SizedBox(
//                               height: SizeConfig.heightMultiplier * 1.5,
//                             ),
//                             Text(
//                               '${vehicle[index]['phone']}',
//                               style: TextStyle(
//                                   color: Colors.white,
//                                   fontSize: appNormalTextSize),
//                             ),
//                             SizedBox(
//                               height: SizeConfig.heightMultiplier ,
//                             ),
//                             Text(
//                               '${vehicle[index]['carate']}',
//                               style: TextStyle(
//                                   color: Colors.white,
//                                   fontSize: appNormalTextSize),
//                             ),
//                           ],
//                         ),
//                       ],
//                     ),
//                   ),
//                 ),
//               ),
//             );
//           },
//         ),
//       ),
//     );
//   }
// }


// import 'package:ombt/modules/Vehicle_list.dart';
// import 'package:ombt/pageMap.dart';
//
// import 'Check_vehicle.dart';
//
// class Travel extends StatefulWidget {
//   @override
//   _TravelState createState() => _TravelState();
// }
//
// class _TravelState extends State<Travel> {
//   @override
//   Widget build(BuildContext context) {
//     return CommonBody(
//       appLogo: 'quallity_manager_logo.png',
//       appTitle: 'Select Vehicle for Loading',
//       onPressed: () => Navigator.pop(context),
//       child: Column(
//         children: <Widget>[
//           Flexible(
//             child: Row(
//               children: <Widget>[
//                 Row(
//                   children: <Widget>[
//                     Image.asset(
//                       "assets/icons/drive.png",
//                       width: 30,
//                       height: 30,
//                     ),
//                     SizedBox(
//                       width: 10,
//                     ),
//                     Text(
//                       "Vehicle List ",
//                       style: TextStyle(fontSize: appSubHeadingFontSize),
//                     ),
//                   ],
//                 ),
//                 Spacer(
//                   flex: 1,
//                 ),
//                 Row(
//                   children: <Widget>[
//                     Image.asset(
//                       "assets/icons/date.png",
//                       width: 30,
//                       height: 30,
//                     ),
//                     SizedBox(
//                       width: 10,
//                     ),
//                     Text(
//                       "21/August/2020",
//                       style: TextStyle(fontSize: appSubHeadingFontSize),
//                     ),
//                   ],
//                 )
//               ],
//             ),
//             flex: 4,
//           ),
//           Spacer(
//             flex: 1,
//           ),
//           Flexible(
//             child: Container(
//               margin: EdgeInsets.fromLTRB(6, 0, 6, 0),
//               child: Divider(
//                 color: Color.fromRGBO(109, 195, 135, 1),
//                 thickness: 1,
//               ),
//             ),
//             flex: 1,
//           ),
//           Spacer(
//             flex: 1,
//           ),
//           Expanded(
//             child: loading(),
//             flex: 40,
//           ),
//           Spacer(
//             flex: 1,
//           ),
//           Container(
//             margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
//             child: AppButton(
//               onPressed: () {
//                 Navigator.push(context,
//                     MaterialPageRoute(builder: (context) => Checkload()));
//               },
//               name: 'SUBMIT',
//             ),
//           ),
//         ],
//       ),
//     );
//   }
//
//   Widget loading() {
//     return GridView.builder(
//       gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
//         crossAxisCount: 2,
//         crossAxisSpacing: 10.0,
//         mainAxisSpacing: 10.0,
//       ),
//       itemCount: vehicle.length,
//       itemBuilder: (context, index) {
//         return AppCard(
//           onTap: () {
//             showDialog(
//               context: context,
//               builder: (BuildContext context) => _showpopupbox(context),
//             );
//           },
//           elevation: 0,
//           color: vehicle[index]['carcolor'],
//           child: Padding(
//             padding: const EdgeInsets.fromLTRB(5, 20, 5, 5),
//             child: Column(
//               children: <Widget>[
//                 Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                   children: <Widget>[
//                     Spacer(
//                       flex: 1,
//                     ),
//                     Icon(
//                       Icons.local_shipping,
//                       color: Colors.white,
//                     ),
//                     Spacer(
//                       flex: 1,
//                     ),
//                     FittedBox(
//                         child: Text(
//                       '${vehicle[index]['vehno']}',
//                       style: TextStyle(
//                           fontSize: appNormalTextSize, color: Colors.white),
//                     )),
//                     Spacer(
//                       flex: 1,
//                     ),
//                   ],
//                 ),
//                 SizedBox(
//                   height: 4,
//                 ),
//                 Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                   children: <Widget>[
//                     Spacer(
//                       flex: 2,
//                     ),
//                     Icon(
//                       Icons.perm_identity,
//                       color: Colors.white,
//                     ),
//                     Spacer(
//                       flex: 3,
//                     ),
//                     Text(
//                       '${vehicle[index]['drivername']}',
//                       style: TextStyle(
//                           fontSize: appNormalTextSize, color: Colors.white),
//                     ),
//                     Spacer(
//                       flex: 5,
//                     ),
//
//                     //
//                   ],
//                 ),
//                 SizedBox(
//                   height: 4,
//                 ),
//                 Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                   children: <Widget>[
//                     Spacer(
//                       flex: 1,
//                     ),
//                     Icon(
//                       Icons.phone_in_talk,
//                       color: Colors.white,
//                     ),
//                     Spacer(
//                       flex: 1,
//                     ),
//                     Text(
//                       '${vehicle[index]['phone']}',
//                       style: TextStyle(
//                           fontSize: appNormalTextSize, color: Colors.white),
//                     ),
//                     Spacer(
//                       flex: 1,
//                     ),
//                   ],
//                 ),
//                 SizedBox(
//                   height: 4,
//                 ),
//                 Row(
//                   children: <Widget>[
//                     Spacer(
//                       flex: 1,
//                     ),
//                     Icon(
//                       Icons.view_comfy,
//                       color: Colors.white,
//                     ),
//                     Spacer(
//                       flex: 1,
//                     ),
//                     Text(
//                       '${vehicle[index]['carate']}',
//                       style: TextStyle(
//                           fontSize: appNormalTextSize, color: Colors.white),
//                     ),
//                     Spacer(
//                       flex: 10,
//                     ),
//                   ],
//                 ),
//               ],
//             ),
//           ),
//         );
//       },
//     );
//   }
// }
//
// Widget _showpopupbox(BuildContext context) {
//   return Container(
//     margin: EdgeInsets.fromLTRB(0, MediaQuery.of(context).size.width, 0, 65),
//     child: new AlertDialog(
//       titlePadding: EdgeInsets.all(0),
//       contentPadding: EdgeInsets.all(0),
//       insetPadding: EdgeInsets.all(0),
//       shape: RoundedRectangleBorder(
//           borderRadius: BorderRadius.all(Radius.circular(30.0))),
//       content: Container(
//         width: MediaQuery.of(context).size.width,
//         padding: EdgeInsets.all(20),
//         child: Column(
//           children: <Widget>[
//             Row(
//               children: <Widget>[
//                 Spacer(
//                   flex: 6,
//                 ),
//                 // Icon(Icons.local_shipping),
//                 Image.asset(
//                   "assets/icons/drive.png",
//                   height: 50,
//                   width: 50,
//                 ),
//                 Spacer(
//                   flex: 1,
//                 ),
//                 Text(
//                   "MH14 PB 1245",
//                   style: TextStyle(fontSize: appNormalTextSize),
//                 ),
//                 Spacer(
//                   flex: 6,
//                 ),
//               ],
//             ),
//             Spacer(
//               flex: 1,
//             ),
//             Row(
//               children: <Widget>[
//                 Spacer(
//                   flex: 14,
//                 ),
//                 Image.asset(
//                   "assets/icons/crate1.png",
//                   height: 50,
//                   width: 50,
//                 ),
//                 Spacer(
//                   flex: 2,
//                 ),
//                 Container(
//                   margin: EdgeInsets.fromLTRB(0, 12, 0, 0),
//                   height: 45,
//                   width: 60,
//                   child: AbsorbPointer(
//                     absorbing: true,
//                     child: AppTextField(
//                       hintName: '7',
//                       focusEnableColor: appPrimaryColor.withOpacity(0.4),
//                     ),
//                   ),
//                 ),
//                 Spacer(
//                   flex: 1,
//                 ),
//                 GestureDetector(
//                   child: Icon(Icons.arrow_forward,
//                       color: Color.fromRGBO(109, 195, 135, 1)),
//                   onTap: () => Navigator.pop(context),
//                 ),
//                 Spacer(
//                   flex: 15,
//                 ),
//               ],
//             ),
//             Spacer(
//               flex: 5,
//             ),
//             Container(
//               decoration: BoxDecoration(
//                 borderRadius: BorderRadius.circular(60.0),
//               ),
//               margin: EdgeInsets.fromLTRB(110, 0, 110, 0),
//               child: Divider(
//                 thickness: 6,
//               ),
//             ),
//           ],
//         ),
//       ),
//     ),
//   );
// }


import 'package:intl/intl.dart';
import 'package:ombt/app/quality-manager/Check_vehicle.dart';
import 'package:ombt/pageMap.dart';

class Reach extends StatefulWidget {
  @override
  _ReachState createState() => _ReachState();
}

class _ReachState extends State<Reach> {
  DateTime selectedDate = DateTime.now();
  double textsizein = SizeConfig.textMultiplier;
  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

  @override
  Widget build(BuildContext context) {
    return CommonBody(
      appLogo: 'logisticManager.png',
      appTitle: 'Select Vehicle for Loading',
      onPressed: () => Navigator.pop(context),
      child: Column(children: [
        Row(
          children: [
            Row(
              children: [
                Container(
                  width: 30,
                  child: Image.asset('assets/icons/logistics.png'),
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  'Vehicle List',
                  style: TextStyle(fontSize: appSubHeadingFontSize),
                )
              ],
            ),
            Spacer(flex: 1),
            GestureDetector(
              child: Row(
                children: [
                  Icon(
                    Icons.today,
                    color: Colors.green,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Container(
                    child: Text(
                      '${DateFormat('dd/MMM/yyyy').format(selectedDate.toLocal())}',
                      style: TextStyle(fontSize: appSubHeadingFontSize),
                    ),
                  )
                ],
              ),
              onTap: () => _selectDate(context),
            ),
          ],
        ),
        Divider(color: Colors.green, thickness: 1),
        Expanded(child: cardcreate()),
        SizedBox(
          height: 20,
        ),
        Container(
          width: MediaQuery.of(context).size.width,
          child:  AppButton(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => unloading()));
            },
            name: 'SUBMIT',
          ),
        ),
        SizedBox(
          height: 20,
        )
      ]),
    );
  }

  Widget cardcreate() {
    var widthin = MediaQuery.of(context).size.width;
    Function king = () => Navigator.push(
        context, MaterialPageRoute(builder: (context) => DispatchEDC()));
    double widthspace = 10;
    return GridView.builder(
        gridDelegate:
        SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
        itemCount: selectVehicle.length,
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: () {
              showDialog(
                  context: context,
                  builder: (context) {
                    return popupin(index, widthin, king);
                  });
            },
            child: AppCard(
              elevation: 0.0,
              color: selectVehicle[index]['cardcolor'],
              child: Padding(
                padding: EdgeInsets.all(20),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Container(
                            width: 20,
                            child: Image.asset('assets/icons/logistics-w.png'),
                          ),
                          SizedBox(
                            width: widthspace,
                          ),
                          Text(
                            selectVehicle[index]['vehiclenumber'],
                            style: TextStyle(
                                fontSize: appNormalTextSize,
                                color: Colors.white),
                          )
                        ],
                      ),
                      Row(
                        children: [
                          Container(
                            width: 15,
                            child: Image.asset('assets/icons/user.png'),
                          ),
                          SizedBox(
                            width: widthspace,
                          ),
                          Text(
                            selectVehicle[index]['personname'],
                            style: TextStyle(
                                fontSize: appNormalTextSize,
                                color: Colors.white),
                          )
                        ],
                      ),
                      Row(
                        children: [
                          Container(
                            width: 15,
                            child: Image.asset('assets/icons/phone-1.png'),
                          ),
                          SizedBox(
                            width: widthspace,
                          ),
                          Text(
                            selectVehicle[index]['callnnumber'],
                            style: TextStyle(
                                fontSize: appNormalTextSize,
                                color: Colors.white),
                          )
                        ],
                      ),
                      Row(
                        children: [
                          Container(
                            width: 20,
                            child: Image.asset('assets/icons/tool-box-w.png'),
                          ),
                          SizedBox(
                            width: widthspace,
                          ),
                          Text(
                            selectVehicle[index]['cartnumber'],
                            style: TextStyle(
                                fontSize: appNormalTextSize,
                                color: Colors.white),
                          )
                        ],
                      ),
                    ]),
              ),
            ),
          );
        });
  }
}
