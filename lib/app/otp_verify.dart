import 'package:ombt/Api/LoginVerification.dart';
import 'package:ombt/pageMap.dart';
import 'package:ombt/widgets/common_SnackBar.dart';

import '../background.dart';

class OtpVerify extends StatefulWidget {
  final String mobno;
  OtpVerify(this.mobno);
  @override
  _OtpVerifyState createState() => _OtpVerifyState();
}

class _OtpVerifyState extends State<OtpVerify> {
  double fontSize = 2;
  ApiLogincall logincall = ApiLogincall();
  TextEditingController otpController = TextEditingController();
  GlobalKey<ScaffoldState> _scaffoldkey = new GlobalKey<ScaffoldState>();

  bool success = false;
  @override
  void initState() {
    super.initState();
    otpVerifyNode = FocusNode();
  }

  FocusScopeNode currentFocus;
  FocusNode otpVerifyNode;
  bool highlight = true;

  void dispose() {
    otpVerifyNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // var landscape = MediaQuery.of(context).orientation == Orientation.landscape;
    currentFocus = FocusScope.of(context);
    return Scaffold(
      key: _scaffoldkey,
      body: SingleChildScrollView(
        child: Stack(
          children: [
            Container(
              height: 100 * SizeConfig.heightMultiplier,
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/logos/ekkam-logo-small.png'),
                    fit: BoxFit.none),
              ),
              child: Column(
                children: [
                  SizedBox(
                    height: 9.4 * SizeConfig.heightMultiplier,
                  ),
                  Container(
                    width: 35 * SizeConfig.widthMultiplier,
                    child: Image.asset(
                      'assets/logos/ekkam.png',
                      // height: 16.2 * SizeConfig.heightMultiplier,
                      // width: 12.8 * SizeConfig.widthMultiplier,
                    ),
                  ),
                  SizedBox(
                    height: 3.6 * SizeConfig.heightMultiplier,
                  ),
                  Container(
                    child: Image.asset(
                      'assets/logos/otpVerify.png',
                      width: 26.7 * SizeConfig.heightMultiplier,
                      height: 38.4 * SizeConfig.widthMultiplier,
                    ),
                  ),
                  SizedBox(
                    height: 10.3 * SizeConfig.heightMultiplier,
                  ),
                  Container(
                    margin: const EdgeInsets.symmetric(horizontal: 26.5),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Align(
                          alignment: Alignment.center,
                          child: Text(
                            'Enter OTP Sent to Your Mobile Number',
                            style: TextStyle(fontSize: appSubHeadingFontSize),
                          ),
                        ),
                        SizedBox(
                          height: 2.5 * SizeConfig.heightMultiplier,
                        ),
                        Align(
                          alignment: Alignment.center,
                          child: Text(
                            '+91-${widget.mobno}',
                            style: TextStyle(fontSize: appSubHeadingFontSize),
                          ),
                        ),
                        SizedBox(
                          height: 2.5 * SizeConfig.heightMultiplier,
                        ),
                        Container(
                          height: 8.4 * SizeConfig.heightMultiplier,
                          padding: EdgeInsets.only(
                            left: 20.0,
                            right: 20.0,
                            bottom: 10.0,
                          ),
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: highlight
                                  ? Color(0xff62c483).withOpacity(1)
                                  : Color(0xff62c483).withOpacity(0.3),
                              width: 2.0,
                            ),
                          ),
                          child: PinFieldAutoFill(
                            onCodeSubmitted: (_) =>
                                otpVerifyNode.requestFocus(),
                            textInputAction: TextInputAction.send,
                            controller: otpController,
                            autofocus: false,
                            keyboardType: TextInputType.phone,
                            decoration: UnderlineDecoration(
                                colorBuilder: FixedColorBuilder(
                                    Color(0xff62c483).withOpacity(0.8))),
                            codeLength: 4,
                            onCodeChanged: (v) {
                              print(v);
                            },
                          ),
                        ),
                        SizedBox(
                          height: 4 * SizeConfig.heightMultiplier,
                        ),
                        AppButton(
                          focusNode: otpVerifyNode,
                          // padding: EdgeInsets.symmetric(vertical: 14.0),
                          onPressed: () {
                            setState(() {
                              success = !success;
                            });
                            // Navigator.push(
                            //     context,
                            //     MaterialPageRoute(
                            //         builder: (context) => OMBTDashBoard()));

                            showDialog(
                                context: context,
                                child:
                                    Center(child: CircularProgressIndicator()));
                            logincall
                                .createLoginVerifyOTP(
                                    widget.mobno, otpController.text)
                                .then((value) {
                              if (value.body.contains('true')) {
                                Navigator.of(context).pop();
                                setState(() {
                                  success = !success;
                                });

                                Future.delayed(Duration(seconds: 3), () {
                                  setState(() {
                                    otpController.text = '';
                                  });
                                  Navigator.of(context).pop();
                                });
                                Future.delayed(
                                    Duration(seconds: 5),
                                    () => Navigator.pushAndRemoveUntil(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => OMBTDashBoard(),
                                        ),
                                        (route) => false));
                              } else {
                                Navigator.of(context).pop();
                                setState(() {
                                  otpController.text = '';
                                  success = !success;
                                });

                                Future.delayed(Duration(seconds: 3),
                                    () => Navigator.of(context).pop());
                              }
                            });
                          },
                          name: 'Verify & Proceed',
                        ),
                        SizedBox(
                          height: 3 * SizeConfig.heightMultiplier,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Didn\'t Receive the OTP ?',
                              style: TextStyle(
                                fontSize: appNormalTextSize,
                              ),
                            ),
                            SizedBox(
                              width: 13.0,
                            ),
                            Text(
                              'Resend Code',
                              style: TextStyle(
                                color: Color(0xffF36523),
                                decoration: TextDecoration.underline,
                                fontSize: appNormalTextSize,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Footer(),
                  ),
                ],
              ),
            ),
            Visibility(
              visible: success,
              child: Container(
                  height: 100 * SizeConfig.heightMultiplier,
                  width: 100 * SizeConfig.widthMultiplier,
                  child: CheckAnimation()),
            ),
          ],
        ),
      ),
    );
  }
}
