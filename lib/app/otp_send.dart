import 'package:ombt/Api/LoginVerification.dart';
import 'package:ombt/pageMap.dart';


class OtpSend extends StatefulWidget {
  @override
  _OtpSendState createState() => _OtpSendState();
}

class _OtpSendState extends State<OtpSend> {
  FocusScopeNode currentFocus;
  ApiLogincall logincall = ApiLogincall();
  GlobalKey<ScaffoldState> _scaffoldkey = new GlobalKey<ScaffoldState>();
  TextEditingController mobileNumber = TextEditingController();
  @override
  Widget build(BuildContext context) {
    // var landscape = MediaQuery.of(context).orientation == Orientation.landscape;
    currentFocus = FocusScope.of(context);
    String notemob = mobileNumber.text;
    return Scaffold(
      key: _scaffoldkey,
      body: GestureDetector(
        onTap: () {
          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: SingleChildScrollView(
          child: Container(
            height: 100 * SizeConfig.heightMultiplier,
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/logos/ekkam-logo-small.png'),
                  fit: BoxFit.none),
            ),
            child: Column(
              children: [
                SizedBox(
                  height: 9.4 * SizeConfig.heightMultiplier,
                ),
                Container(
                  width: 35 * SizeConfig.widthMultiplier,
                  child: Image.asset(
                    'assets/logos/ekkam.png',
                    // height: 16.2 * SizeConfig.heightMultiplier,
                    // width: 12.8 * SizeConfig.widthMultiplier,
                  ),
                ),
                SizedBox(
                  height: 4.1 * SizeConfig.heightMultiplier,
                ),
                Container(
                  child: Image.asset(
                    'assets/logos/otpSend.png',
                    width: 26.7 * SizeConfig.heightMultiplier,
                    height: 38.4 * SizeConfig.widthMultiplier,
                  ),
                ),
                SizedBox(
                  height: 17.5 * SizeConfig.heightMultiplier,
                ),
                Container(
                  margin: const EdgeInsets.symmetric(horizontal: 26.5),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Align(
                        alignment: Alignment.center,
                        child: Text(
                          'Your Phone Number',
                          style: TextStyle(
                            color: appTextColor,
                            fontSize: 2 * SizeConfig.textMultiplier,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 3 * SizeConfig.heightMultiplier,
                      ),
                      Container(
                        height: 7.4 * SizeConfig.heightMultiplier,
                        child: AppTextField(
                          controllerName: mobileNumber,
                          hintName: '       Enter the Mobile Number',
                          keyboardType: TextInputType.phone,
                          prefixText: '+91-',
                          maxLength: 10,
                        ),
                      ),
                      SizedBox(
                        height: 4.1 * SizeConfig.heightMultiplier,
                      ),
                      AppButton(
                        onPressed: () {
                          Navigator.push(
                              context, MaterialPageRoute(builder: (context) => OtpVerify(notemob.toString())));

                          print(mobileNumber.text);
                          // showDialog(
                          //     context: context,
                          //     child:
                          //         Center(child: CircularProgressIndicator()));
                          // logincall
                          //     .createLoginOTP(mobileNumber.text)
                          //     .then((value) {
                          //   print(value.body[0]);
                            // if (value.body.contains('true')) {
                            //   Navigator.of(context).pop();
                            //   showDialog(
                            //       context: context,
                            //       child: AlertDialog(
                            //         contentPadding: EdgeInsets.all(15),
                            //         content: Row(
                            //           children: [
                            //             CircleAvatar(
                            //               radius: 3 *
                            //                   SizeConfig
                            //                       .heightMultiplier,
                            //               backgroundColor:
                            //               Color(0xff58BC7A),
                            //               child: Icon(
                            //                 Icons.done,
                            //                 color: Colors.white,
                            //                 size: 6 *
                            //                     SizeConfig
                            //                         .imageSizeMultiplier,
                            //               ),
                            //             ),
                            //             SizedBox(
                            //               width: 10,
                            //             ),
                            //             Text(
                            //                 'OTP SEND '),
                            //           ],
                            //         )
                            //         ,
                            //       ));
                            //   Future.delayed(Duration(seconds: 1), () {
                            //     setState(() {
                            //       mobileNumber.text = '';
                            //     });
                            //     Navigator.of(context).pop();
                            //   });
                            //   Future.delayed(
                            //       Duration(seconds: 2),
                            //           () => Navigator.pushReplacement(
                            //         context,
                            //         MaterialPageRoute(
                            //             builder: (context) =>
                            //                 OtpVerify(
                            //                     notemob.toString())),
                            //       ));
                            // }
                            // else {
                            //   Navigator.of(context).pop();
                            //   setState(() {
                            //     mobileNumber.text = '';
                            //   });
                            //   showDialog(
                            //       context: context,
                            //       child: AlertDialog(
                            //           contentPadding: EdgeInsets.all(10),
                            //           content: Row(
                            //             children: [
                            //               CircleAvatar(
                            //                 radius: 3 *
                            //                     SizeConfig
                            //                         .heightMultiplier,
                            //                 backgroundColor: Colors.red,
                            //                 child: Icon(
                            //                   Icons.error,
                            //                   color: Colors.white,
                            //                   size: 6 *
                            //                       SizeConfig
                            //                           .imageSizeMultiplier,
                            //                 ),
                            //               ),
                            //               SizedBox(
                            //                 width: 10,
                            //               ),
                            //               Text('Please Enter Registered FS Mobile Number')
                            //             ],
                            //           )));
                            //   Future.delayed(Duration(seconds: 1),
                            //           () => Navigator.of(context).pop());
                            // }

                          // });
                        },
                        name: 'GET OTP',
                      )
                    ],
                  ),
                ),
                Expanded(child: Footer()),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
// if (value.body.contains('true')) {
//   Navigator.of(context).pop();
//   showInSnackBar(
//       _scaffoldkey, 'OTP Send Successful');
//   Future.delayed(
//       Duration(seconds: 3),
//       () => Navigator.push(
//             context,
//             MaterialPageRoute(
//                 builder: (context) =>
//                     OtpVerify(notemob.toString())),
//           ));
// } else {
//   Navigator.of(context).pop();
//   showInSnackBar(_scaffoldkey,
//       'Please Enter Registered FS Mobile Number');
// }
