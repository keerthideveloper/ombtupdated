import 'package:ombt/modules/Check_list.dart';
import 'package:ombt/pageMap.dart';

import 'Indent_Quality.dart';

class Checkload extends StatefulWidget {
  @override
  _CheckloadState createState() => _CheckloadState();
}

class _CheckloadState extends State<Checkload> {
  @override
  Widget build(BuildContext context) {
    return CommonBody(
      appLogo: 'quallity_manager_logo.png',
      appTitle: 'Check Vehicle Loaded',
      onPressed: () => Navigator.pop(context),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Image.asset(
                    'assets/icons/drive.png',
                    width: 30,
                    height: 30,
                  ),
                  SizedBox(width: 10),
                  Text(
                    "MH 14 PB 1234",
                    style: TextStyle(fontSize: appSubHeadingFontSize),
                  ),
                ],
              ),
              Row(
                children: [
                  Image.asset(
                    'assets/icons/date.png',
                    width: 30,
                    height: 30,
                  ),
                  SizedBox(width: 10),
                  Text(
                    "21/August/2020",
                    style: TextStyle(fontSize: appSubHeadingFontSize),
                  ),
                  SizedBox(width: 5),
                ],
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: [
                  Image.asset(
                    'assets/icons/usergreen.png',
                    width: 30,
                    height: 30,
                  ),
                  SizedBox(width: 10),
                  Text(
                    "Babu Bhal",
                    style: TextStyle(fontSize: appSubHeadingFontSize),
                  ),
                ],
              ),
              Row(
                children: [
                  Image.asset(
                    'assets/icons/phone.png',
                    width: 30,
                    height: 30,
                  ),
                  SizedBox(width: 10),
                  Text(
                    "+91 98765420898",
                    style: TextStyle(fontSize: appSubHeadingFontSize),
                  ),
                ],
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: [
                  Image.asset(
                    'assets/icons/crate2.png',
                    width: 30,
                    height: 30,
                  ),
                  SizedBox(width: 10),
                  Text(
                    "10",
                    style: TextStyle(fontSize: appSubHeadingFontSize),
                  ),
                ],
              ),
              SizedBox(
                width: 110,
              ),
              Row(
                children: [
                  Image.asset(
                    'assets/icons/location.png',
                    width: 30,
                    height: 30,
                  ),
                  SizedBox(width: 10),
                  Text(
                    "Banner,Pune",
                    style: TextStyle(fontSize: appSubHeadingFontSize),
                  ),
                  SizedBox(width: 30),
                ],
              )
            ],
          ),
          Container(
            margin: EdgeInsets.fromLTRB(3, 0, 3, 0),
            child: Divider(
              color: Color.fromRGBO(109, 195, 135, 1),
              thickness: 1,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Expanded(
            child: ListView.builder(
                itemCount: 3,
                itemBuilder: (context, index) {
                  return loadcheck(index);
                }),
            flex: 5,
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Expanded(
                child: AppButton(
                  onPressed: () {},
                  name: "REDO",
                ),
              ),
              SizedBox(
                width: 10.0,
              ),
              Expanded(
                child: AppButton(
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Indent()));
                  },
                  name: "Done",
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget loadcheck(index) {
    return AppCard(
      child: Container(
        margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: Column(
          children: <Widget>[
            IntrinsicHeight(
              child: Row(
                children: <Widget>[
                  SizedBox(
                    width: 15,
                  ),
                  SizedBox(
                    child: DropdownButton<Test>(
                      itemHeight: 40,
                      iconSize: 35,
                      hint: drop1(selectedItemValue[0]),
                      underline: SizedBox(
                        width: 0,
                      ),
                      iconEnabledColor: Color.fromRGBO(109, 195, 135, 1),
                      value: dropdownValue[index],
                      items: selectedItemValue
                          .map((Test e) => DropdownMenuItem<Test>(
                                value: e,
                                child: drop(e),
                              ))
                          .toList(),
                      onChanged: (value) {
                        dropdownValue[index] = value;
                        setState(() {
                          print('$value test');
                        });
                      },
                    ),
                  ),
                  VerticalDivider(
                    thickness: 1,
                    color: Color.fromRGBO(109, 195, 135, 1),
                  ),
                  Container(
                    child: CircleAvatar(
                      backgroundColor: Color.fromRGBO(242, 242, 242, 1),
                      radius: 20,
                      child: Container(
                          width: 20,
                          height: 20,
                          child: Image.asset('assets/icons/user.png')),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    '${loaddetails[index]['names']}',
                    style: TextStyle(fontSize: appNormalTextSize),
                  ),
                  Spacer(
                    flex: 1,
                  ),
                ],
              ),
            ),
            Row(
              children: <Widget>[
                Spacer(
                  flex: 300,
                ),
                FittedBox(
                    fit: BoxFit.contain,
                    child: Text(
                      '${loaddetails[index]['cates']}',
                      style: TextStyle(fontSize: appNormalTextSize),
                    )),
                Spacer(
                  flex: 20,
                ),
                Image.asset(
                  'assets/icons/crate1.png',
                  width: 30,
                  height: 20,
                ),
                FittedBox(
                    fit: BoxFit.contain,
                    child: Text(
                      '${loaddetails[index]['count']}',
                      style: TextStyle(fontSize: appNormalTextSize),
                    )),
                Spacer(
                  flex: 20,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget drop(e) {
    return Container(

      child: Column(
        children: [
          Container(
            child: e.image,
          ),
          Text(
            e.valuein,
            style: TextStyle(fontSize: appNormalTextSize),
          )
        ],
      ),
    );
  }

  Widget drop1(e) {
    return Column(children: [
      Container(
        child: e.image,
      ),
      Text(
        e.valuein,
        style: TextStyle(fontSize: appNormalTextSize),
      )
    ]);
  }

  List<Test> selectedItemValue = <Test>[
    Test(
        'Tomato',
        Image.asset(
          'assets/icons/tomato.png',
          height: 25.0,
        )),
    Test(
        'Potato',
        Image.asset(
          'assets/icons/potatoes.png',
          height: 25.0,
        )),
    Test(
        'Banana',
        Image.asset(
          'assets/icons/banana.png',
          height: 25.0,
        )),
    Test(
        'Cauliflower',
        Image.asset(
          'assets/icons/cauliflower.png',
          height: 25.0,
        )),
    Test(
        'Brinjal',
        Image.asset(
          "assets/icons/brinjal.png",
          height: 25.0,
        )),
    Test(
        'Onion',
        Image.asset(
          'assets/icons/onion.png',
          height: 25.0,
        ))
  ];

  List<Test> dropdownValue = [null, null, null, null, null, null];
}

class Test {
  Test(this.valuein, this.image);
  final String valuein;
  final Widget image;
}
