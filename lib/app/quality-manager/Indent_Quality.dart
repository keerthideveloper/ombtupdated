import 'package:ombt/modules/Crate_details.dart';
import 'package:ombt/pageMap.dart';

class Indent extends StatefulWidget {
  @override
  _IndentState createState() => _IndentState();
}

class _IndentState extends State<Indent> {
  @override
  Widget build(BuildContext context) {
    return CommonBody(
      appLogo: 'quallity_manager_logo.png',
      appTitle: 'Indent Quantity',
      onPressed: () => Navigator.pop(context),
      child: Column(
        children: <Widget>[
          Flexible(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Spacer(
                  flex: 1,
                ),
                Image.asset(
                  "assets/icons/crate.png",
                  height: 30,
                  width: 50,
                ),
                Spacer(
                  flex: 1,
                ),
                Text(
                  "Crate List",
                  style: TextStyle(fontSize: appSubHeadingFontSize),
                ),
                Spacer(
                  flex: 5,
                ),
                Image.asset(
                  "assets/icons/date.png",
                  height: 30,
                  width: 50,
                ),
                Spacer(
                  flex: 1,
                ),
                Text(
                  "11/August/2020",
                  style: TextStyle(fontSize: appSubHeadingFontSize),
                ),
                Spacer(
                  flex: 1,
                ),
              ],
            ),
            flex: 2,
          ),
          Flexible(
            child: Container(
              margin: EdgeInsets.fromLTRB(12, 0, 12, 0),
              child: Divider(
                color: Color.fromRGBO(109, 195, 135, 1),
                thickness: 1,
              ),
            ),
            flex: 1,
          ),
          Flexible(
            child: ListView.builder(
                itemCount: 6,
                itemBuilder: (context, index) {
                  return crate(index);
                }),
            flex: 30,
          ),
        ],
      ),
    );
  }

  Widget crate(index) {
    print(index);
    double textsize = 14;
    return AppCard(
      onTap: () => Navigator.push(context,
          MaterialPageRoute(builder: (context) => QualityOtpValidate())),
      child: Container(
        margin: EdgeInsets.fromLTRB(0, 12, 0, 12),
        child: IntrinsicHeight(
          child: Row(
            children: <Widget>[
              SizedBox(
                width: 20,
              ),
              Container(
                child: DropdownButtonHideUnderline(

                  child: DropdownButton<Test>(
                    // itemHeight: 40,
                    iconSize: 35,
                    hint: drop1(selectedItemValue[0]),
                    iconEnabledColor: Color.fromRGBO(109, 195, 135, 1),
                    value: dropdownValue[index],
                    items: selectedItemValue
                        .map((Test e) => DropdownMenuItem<Test>(
                              value: e,
                              child: drop(e),
                            ))
                        .toList(),
                    onChanged: (value) {
                      dropdownValue[index] = value;
                      setState(() {
                        print('$value test');
                      });
                    },
                  ),
                ),
              ),
              VerticalDivider(
                thickness: 1,
                color: Color.fromRGBO(109, 195, 135, 1),
              ),
              Flexible(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      // mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Container(
                          child: CircleAvatar(
                            backgroundColor: Color.fromRGBO(242, 242, 242, 1),
                            radius: 20,
                            child: Container(
                                width: 20,
                                height: 20,
                                child: Image.asset('assets/icons/user.png')),
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),

                        Text(
                          '${item[index]['names']}',
                          style: TextStyle(
                              color: Colors.black, fontSize: appNormalTextSize),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Text(
                          '${item[index]['kilos']}',
                          style: TextStyle(
                              color: appTextColor, fontSize: appNormalTextSize),
                        ),
                        SizedBox(
                          width: 30,
                        ),
                        Container(
                          height: SizeConfig.heightMultiplier * 3,
                          width: SizeConfig.widthMultiplier * 9,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              fit: BoxFit.cover,
                              image: AssetImage('${item[index]['crateicon']}'),
                            ),
                          ),
                        ),
                        Text(
                          '${item[index]['numbers']}',
                          style: TextStyle(fontSize: appNormalTextSize),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget drop(e) {
    return Container(
      child: Column(
        children: [
          Container(
            child: e.image,
          ),
          Text(
            e.valuein,
            style: TextStyle(fontSize: appNormalTextSize,color: appTextColor),
          )
        ],
      ),
    );
  }

  Widget drop1(e) {
    return Column(children: [
      Container(
        child: e.image,
      ),
      Text(
        e.valuein,
        style: TextStyle(fontSize: appNormalTextSize,color: appTextColor),
      )
    ]);
  }

  List<Test> selectedItemValue = <Test>[
    Test(
        'Tomato',
        Image.asset(
          'assets/icons/tomato.png',
          height: 25.0,
        )),
    Test(
        'Potato',
        Image.asset(
          'assets/icons/potatoes.png',
          height: 25.0,
        )),
    Test(
        'Banana',
        Image.asset(
          'assets/icons/banana.png',
          height: 25.0,
        )),
    Test(
        'Cauliflower',
        Image.asset(
          'assets/icons/cauliflower.png',
          height: 25.0,
        )),
    Test(
        'Brinjal',
        Image.asset(
          "assets/icons/brinjal.png",
          height: 25.0,
        )),
    Test(
        'Onion',
        Image.asset(
          'assets/icons/onion.png',
          height: 25.0,
        ))
  ];

  List<Test> dropdownValue = [null, null, null, null, null, null];
}

class Test {
  Test(this.valuein, this.image);
  final String valuein;
  final Widget image;
}
