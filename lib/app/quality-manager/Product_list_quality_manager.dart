import 'package:flutter/cupertino.dart';
import 'package:ombt/app/quality-manager/Vehicle_for_loading.dart';
import 'package:ombt/modules/Product_details.dart';
import 'package:ombt/pageMap.dart';

class QualityManagerProductList extends StatefulWidget {
  QualityManagerProductList({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _QualityManagerProductListState createState() =>
      _QualityManagerProductListState();
}

class _QualityManagerProductListState extends State<QualityManagerProductList> {
  @override
  Widget build(BuildContext context) {
    return CommonBody(
      appTitle: 'Product List',
      onPressed: () => Navigator.pop(context),
      child: Container(
        height: 65 * SizeConfig.heightMultiplier,
        child: Column(
          // crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Flexible(
              child: ListView.builder(
                  itemCount: fruits.length,
                  itemBuilder: (context, index) {
                    return fruitlist(index);
                  }),
              flex: 50,
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
              child: AppButton(
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Travel()));
                },
                name: "Generate Invoice",
              ),
            ),
          ],
        ),
      ),
    );
  }

  bool isSelected = false;

  Widget fruitlist(index) {
    Gallery _galleryData = galleryData[index];
    double textsizes = 1.6;
    print(fruits[index]['grade']);
    return AppCard(
      onTap: () {
        setState(() {
          fruits[index]['selected'] = !fruits[index]['selected'];
        });
      },
      child: Padding(
        padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 2,
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          _galleryData.isSelected = !_galleryData.isSelected;
                        });
                      },
                      child: Container(
                        width: 10 * SizeConfig.widthMultiplier,
                        height: 8 * SizeConfig.heightMultiplier,
                        child: Column(
                          children: <Widget>[
                            _galleryData.isSelected
                                ? Align(
                                    alignment: Alignment.topLeft,
                                    child: Icon(
                                      Icons.check_circle,
                                      color: Colors.green[800],
                                    ),
                                  )
                                : Container(),
                            Expanded(
                              child: Image.asset(
                                _galleryData.imgUrl,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: SizeConfig.heightMultiplier * 2,
                    ),
                    Text(
                      '${fruits[index]['vegname']}',
                      style: TextStyle(fontSize: appNormalTextSize),
                    ),
                    SizedBox(
                      height: SizeConfig.heightMultiplier * 2,
                    ),
                    Text(
                      '${fruits[index]['stay']}',
                      style: TextStyle(
                        color: fruits[index]['staycolor'],
                        fontSize: appNormalTextSize,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 6,
              child: Container(
                child: Column(
                  children: [
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          Column(
                            children: [
                              Text(
                                "Exp.Qty",
                                style: TextStyle(fontSize: appNormalTextSize),
                              ),
                              SizedBox(
                                height: SizeConfig.heightMultiplier * 1,
                              ),
                              Text(
                                '${fruits[index]['exptqty']}',
                                style: TextStyle(fontSize: appNormalTextSize),
                              ),
                            ],
                          ),
                          Container(
                            height: 35.0,
                            child: VerticalDivider(
                              width: 3.0,
                              color: Color(0xff58BC7A).withOpacity(0.3),
                            ),
                          ),
                          Column(
                            children: [
                              Text(
                                "Rec.Qty",
                                style: TextStyle(fontSize: appNormalTextSize),
                              ),
                              SizedBox(
                                height: SizeConfig.heightMultiplier * 1,
                              ),
                              Text(
                                '${fruits[index]['recqty']}',
                                style: TextStyle(fontSize: appNormalTextSize),
                              ),
                            ],
                          ),
                          Container(
                            height: 35.0,
                            child: VerticalDivider(
                              width: 3.0,
                              color: Color(0xff58BC7A).withOpacity(0.3),
                            ),
                          ),
                          Column(
                            children: [
                              GestureDetector(
                                onTap: () {
                                  showDialog(
                                    context: context,
                                    builder: (BuildContext context) =>
                                        _showpopup(context, fruits[index]),
                                  );
                                },
                                child: Container(
                                  width: 10 * SizeConfig.widthMultiplier,
                                  height: 3 * SizeConfig.heightMultiplier,
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image: AssetImage(
                                            '${fruits[index]['boxImg']}')),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: SizeConfig.heightMultiplier * 1,
                              ),
                              Text(
                                "10",
                                style: TextStyle(fontSize: appNormalTextSize),
                              ),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              GestureDetector(
                                onTap: () {
                                  showDialog(
                                    context: context,
                                    builder: (BuildContext context) =>
                                        _showpopupbutton(context),
                                  );
                                },
                                child: Icon(
                                  Icons.add_circle,
                                  color: Color.fromRGBO(139, 199, 59, 1),
                                  size: 20,
                                ),
                              ),
                              SizedBox(
                                height: SizeConfig.heightMultiplier * 1,
                              ),
                              Text(""),
                            ],
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: SizeConfig.heightMultiplier * 1,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        SizedBox(width: 2.5 * SizeConfig.widthMultiplier),
                        Text(
                          "(MOQ-20kg/crate)",
                          style: TextStyle(fontSize: appNormalTextSize),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        ButtonBar(
                          children: [
                            Container(
                              child: CustomSwitch(
                                value: fruits[index]['grade'],
                                leftyes: 'Grade1/2',
                                rightno: 'Grade3',
                                onChanged: (bool val) {
                                  setState(() {
                                    fruits[index]['grade'] = val;
                                  });
                                },
                              ),
                            ),
                            Container(
                              height: 28.0,
                              child: RaisedButton(
                                onPressed: () {},
                                color: fruits[index]['rasiecolor'],
                                child: Text(
                                  '${fruits[index]['raise2']}',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 10,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Widget _showpopup(BuildContext context, fruits) {
  bool landscape = Orientation.landscape == MediaQuery.of(context).orientation;
  return SingleChildScrollView(
    child: Container(
      height: MediaQuery.of(context).size.height,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          AlertDialog(
            titlePadding: EdgeInsets.all(0),
            contentPadding: EdgeInsets.all(0),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(30.0))),
            content: Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: const EdgeInsets.only(bottom: 10.0),
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: SizeConfig.heightMultiplier * 3,
                    ),
                    Row(
                      children: <Widget>[
                        SizedBox(
                          width: SizeConfig.widthMultiplier * 30,
                        ),
                        Container(
                          width: 10 * SizeConfig.widthMultiplier,
                          height: 3 * SizeConfig.heightMultiplier,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage('${fruits['vegimg']}')),
                          ),
                        ),
                        SizedBox(
                          width: SizeConfig.widthMultiplier * 4,
                        ),
                        Text(
                          '${fruits['vegname']}',
                          style: TextStyle(
                            fontSize: appSubHeadingFontSize,
                          ),
                        ),
                        SizedBox(
                          width: SizeConfig.widthMultiplier,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: SizeConfig.heightMultiplier * 3,
                    ),
                    Row(
                      children: <Widget>[
                        SizedBox(
                          width: SizeConfig.widthMultiplier * 30,
                        ),
                        Container(
                          width: 10 * SizeConfig.widthMultiplier,
                          height: 3 * SizeConfig.heightMultiplier,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage('${fruits['boxImg']}')),
                          ),
                        ),
                        SizedBox(
                          width: SizeConfig.widthMultiplier * 4,
                        ),
                        Container(
                          margin: EdgeInsets.fromLTRB(0, 12, 0, 0),
                          height: SizeConfig.heightMultiplier * 6,
                          width: SizeConfig.widthMultiplier * 15,
                          child: AppTextField(
                            focusEnableColor: appPrimaryColor.withOpacity(0.4),
                          ),
                        ),
                        SizedBox(
                          width: SizeConfig.widthMultiplier * 3,
                        ),
                        GestureDetector(
                          child: Icon(Icons.arrow_forward,
                              color: Color.fromRGBO(109, 195, 135, 1)),
                          onTap: () => Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Selectfruit())),
                        ),
                        SizedBox(
                          width: SizeConfig.widthMultiplier,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: SizeConfig.heightMultiplier,
                    ),
                    Center(
                      child: Container(
                        height: SizeConfig.heightMultiplier - 2,
                        width: SizeConfig.widthMultiplier * 30,
                        decoration: BoxDecoration(
                          color: Color.fromRGBO(221, 221, 221, 1),
                          borderRadius: BorderRadius.circular(60.0),
                        ),
                        margin: EdgeInsets.fromLTRB(115, 27, 100, 0),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    ),
  );
}

Widget _showpopupbutton(BuildContext context) {
  return Container(
    margin: EdgeInsets.fromLTRB(0, MediaQuery.of(context).size.width, 0, 0),
    child: SingleChildScrollView(
      child: new AlertDialog(
        titlePadding: EdgeInsets.all(0),
        contentPadding: EdgeInsets.all(0),
        insetPadding: EdgeInsets.all(0),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(30.0))),
        content: Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.all(20),
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Text(
                    "click on + Button show this popup",
                    style: TextStyle(fontSize: appSubHeadingFontSize),
                  ),
                ],
              ),
              SizedBox(
                height: SizeConfig.heightMultiplier * 2,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    height: SizeConfig.heightMultiplier * 11,
                    width: SizeConfig.widthMultiplier * 70,
                    child: AppTextField(
                      hintName: '4.99',
                      focusEnableColor: appPrimaryColor.withOpacity(0.4),
                    ),
                  ),
                  GestureDetector(
                    child: Container(
                        margin: EdgeInsets.fromLTRB(15, 8, 0, 0),
                        child: Icon(
                          Icons.arrow_forward,
                          color: Color.fromRGBO(109, 195, 135, 1),
                        )),
                    onTap: () => Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Selectfruit())),
                  ),
                ],
              ),
              SizedBox(
                height: SizeConfig.heightMultiplier * 2,
              ),
              Center(
                child: Container(
                  height: SizeConfig.heightMultiplier - 2,
                  width: SizeConfig.widthMultiplier * 30,
                  decoration: BoxDecoration(
                    color: Color.fromRGBO(221, 221, 221, 1),
                    borderRadius: BorderRadius.circular(60.0),
                  ),
                  margin: EdgeInsets.fromLTRB(115, 27, 100, 0),
                ),
              ),
            ],
          ),
        ),
      ),
    ),
  );
}
