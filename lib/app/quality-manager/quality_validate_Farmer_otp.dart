import 'package:ombt/pageMap.dart';

import 'Product_list_quality_manager.dart';

class QualityOtpValidate extends StatefulWidget {
  final int num;
  QualityOtpValidate({Key key, this.num}) : super(key: key);
  @override
  _QualityOtpValidateState createState() => _QualityOtpValidateState();
}

class _QualityOtpValidateState extends State<QualityOtpValidate> {
  @override
  Widget build(BuildContext context) {
    return CommonBody(
      appLogo: 'quallity_manager_logo.png',
      appTitle: 'Validate Farmer OTP',
      onPressed: () => Navigator.pop(context),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                        child: FittedBox(
                            child: Text(
                      "Sonnu Melava",
                      style: TextStyle(fontSize: appHeadingFontSize),
                    ))),
                    SizedBox(
                      height: 10.0,
                    ),
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.location_on,
                          color: Colors.yellow,
                        ),
                        FittedBox(
                            child: Text(
                          "Vimannagar",
                          style: TextStyle(
                            fontSize: appSubHeadingFontSize,
                            color: Colors.yellow,
                          ),
                        )),
                        Spacer(
                          flex: 1,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Container(
                child: CircleAvatar(
                  radius: 20,
                  backgroundColor: Color.fromRGBO(242, 242, 242, 1),
                  child: Container(
                    width: 20,
                    height: 20,
                    child: Image.asset('assets/icons/user.png'),
                  ),
                ),
              ),
            ],
          ),
          Spacer(
            flex: 7,
          ),
          Container(
            height: 7 * SizeConfig.heightMultiplier,
            padding: EdgeInsets.only(
              left: 20.0,
              right: 20.0,
              bottom: 10.0,
            ),
            decoration: BoxDecoration(
              border: Border.all(
                color: appPrimaryColor,
                width: 2.0,
              ),
            ),
            child: PinFieldAutoFill(
              keyboardType: TextInputType.phone,
              decoration: UnderlineDecoration(
                  colorBuilder:
                      FixedColorBuilder(Color(0xff62c483).withOpacity(0.8))),
              // decoration: UnderlineDecoration(
              //   color: Color(0xff62c483),
              // ),
              codeLength: 4,
              onCodeChanged: (v) {
                print(v);
              },
            ),
          ),
          Spacer(
            flex: 2,
          ),
          GestureDetector(
            child: Text(
              "OR",
              style: TextStyle(fontSize: appNormalTextSize),
            ),
            onTap: () => Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => QualityManagerProductList())),
          ),
          Spacer(
            flex: 2,
          ),
          AbsorbPointer(
            absorbing: true,
            child: Container(
              height: 7.5 * SizeConfig.heightMultiplier,
              child: AppTextField(
                focusEnableColor: Color.fromRGBO(109, 195, 135, 1),
                hintName: "Search Farmer by Name/Id",
                prefixIcon: Icon(
                  Icons.search,
                  color: Color.fromRGBO(109, 195, 135, 1),
                ),
              ),
            ),
          ),
          Spacer(
            flex: 20,
          ),
        ],
      ),
    );
  }
}
