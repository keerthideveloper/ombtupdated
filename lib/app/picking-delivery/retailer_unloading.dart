import 'package:ombt/modules/delivery.dart';
import 'package:ombt/pageMap.dart';

class RetailerUnloading extends StatefulWidget {
  final int vehicleId;
  final int storeId;

  const RetailerUnloading({Key key, this.vehicleId, this.storeId})
      : super(key: key);
  @override
  _RetailerUnloadingState createState() => _RetailerUnloadingState();
}

class _RetailerUnloadingState extends State<RetailerUnloading> {
  bool toggleValue = false;
  @override
  Widget build(BuildContext context) {
    return CommonBody(
      appLogo: 'logisticManager.png',
      appTitle: 'Retailer Unloading',
      onPressed: () {
        Navigator.pop(context);
      },
      child: Column(
        children: [
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Row(
                  children: [
                    Image.asset('assets/icons/logistics.png'),
                    SizedBox(width: 10.0),
                    Text(vehicleList[widget.vehicleId]['vehicleNo'],
                        style: TextStyle(fontSize: appSubHeadingFontSize)),
                  ],
                ),
                SizedBox(
                  height: 7.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                        vehicleList[widget.vehicleId]['storeList']
                            [widget.storeId]['storeName'],
                        style: TextStyle(fontSize: appSubHeadingFontSize)),
                    Container(
                      child: Row(
                        children: [
                          Image.asset(vehicleList[widget.vehicleId]['storeList']
                              [widget.storeId]['cratesImg']),
                          SizedBox(width: 10.0),
                          Text(vehicleList[widget.vehicleId]['crates'],
                              style:
                                  TextStyle(fontSize: appSubHeadingFontSize)),
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 7.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                        'Customer Id: ${vehicleList[widget.vehicleId]['storeList'][widget.storeId]['customerId']}',
                        style: TextStyle(fontSize: appSubHeadingFontSize)),
                    Text(
                        vehicleList[widget.vehicleId]['storeList']
                            [widget.storeId]['location'],
                        style: TextStyle(fontSize: appSubHeadingFontSize)),
                    Text(''),
                  ],
                ),
                SizedBox(
                  height: 7.0,
                ),
                Divider(
                  color: Color(0xff6BCC8C),
                ),
                Expanded(
                  child: ListView.builder(
                      itemCount: vehicleList[widget.vehicleId]['storeList']
                              [widget.storeId]['productList']
                          .length,
                      itemBuilder: (BuildContext context, index) {
                        return AppCard(
                          child: Container(
                            padding: EdgeInsets.all(10.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  width: SizeConfig.widthMultiplier * 7,
                                  child: Radio(
                                      value: null,
                                      groupValue: null,
                                      onChanged: (v) {}),
                                ),
                                Container(
                                  width: SizeConfig.widthMultiplier * 36,
                                  child: Row(
                                    children: [
                                      Image.asset(vehicleList[widget.vehicleId]
                                              ['storeList'][widget.storeId]
                                          ['productList'][index]['productImg']),
                                      SizedBox(
                                        width: 10.0,
                                      ),
                                      Text(
                                          vehicleList[widget.vehicleId]
                                                          ['storeList']
                                                      [widget.storeId]
                                                  ['productList'][index]
                                              ['productName'],
                                          style: TextStyle(
                                              fontSize: appNormalTextSize)),
                                    ],
                                  ),
                                ),
                                Container(
                                  height: 25,
                                  child: VerticalDivider(
                                    color: Colors.green,
                                    thickness: 0.5,
                                  ),
                                ),
                                Text(
                                    vehicleList[widget.vehicleId]['storeList']
                                            [widget.storeId]['productList']
                                        [index]['productQty'],
                                    style:
                                        TextStyle(fontSize: appNormalTextSize)),
                                Container(
                                  height: 25,
                                  child: VerticalDivider(
                                    color: Colors.green,
                                    thickness: 0.5,
                                  ),
                                ),
                                Container(
                                  child: Row(
                                    children: [
                                      Image.asset(vehicleList[widget.vehicleId]
                                              ['storeList'][widget.storeId]
                                          ['productList'][index]['crates']),
                                      SizedBox(
                                        width: 10.0,
                                      ),
                                      Text(
                                          vehicleList[widget.vehicleId]
                                                          ['storeList']
                                                      [widget.storeId]
                                                  ['productList'][index]
                                              ['cratesQty'],
                                          style: TextStyle(
                                              fontSize: appNormalTextSize)),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      }),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: Row(
                        children: [
                          Text('Crates Deliver',
                              style: TextStyle(fontSize: appNormalTextSize)),
                          SizedBox(
                            width: 10.0,
                          ),
                          GestureDetector(
                            onTap: () {
                              toggleValue == false
                                  ? Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              RetailerInvoicing(
                                                vehicleId: widget.vehicleId,
                                                storeId: widget.storeId,
                                              )))
                                  : null;
                              setState(() {
                                toggleValue = !toggleValue;
                              });
                            },
                            child: Container(
                              height: SizeConfig.heightMultiplier * 4.2,
                              width: SizeConfig.widthMultiplier * 17.3,
                              decoration: BoxDecoration(
                                color: toggleValue
                                    ? Color(0xff58BC7A)
                                    : Color(0xff00953B),
                                borderRadius: BorderRadius.circular(2.0),
                              ),
                              child: Row(
                                children: [
                                  Expanded(
                                    flex: toggleValue ? 4 : 2,
                                    child: Container(
                                      decoration: BoxDecoration(
                                        color: toggleValue
                                            ? Color(0xff00953B)
                                            : Color(0xff58BC7A),
                                        borderRadius:
                                            BorderRadius.circular(2.0),
                                      ),
                                      child: Center(
                                        child: Visibility(
                                          visible: toggleValue,
                                          child: Text(
                                            'Yes',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: appNormalTextSize),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    flex: toggleValue ? 2 : 4,
                                    child: Container(
                                      child: Center(
                                        child: Visibility(
                                          visible: !toggleValue,
                                          child: Text(
                                            'No',
                                            style: TextStyle(
                                                fontSize: appNormalTextSize,
                                                color: Colors.white),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 5.0,
                          ),
                          Image.asset('assets/icons/camera.png'),
                        ],
                      ),
                    ),
                    Container(
                      child: Row(
                        children: [
                          Image.asset('assets/icons/fast.png'),
                          SizedBox(
                            width: 5.0,
                          ),
                          Text('1:45 pm',
                              style: TextStyle(fontSize: appNormalTextSize)),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
            flex: 8,
          ),
          Spacer(
            flex: 1,
          ),
        ],
      ),
    );
  }
}
