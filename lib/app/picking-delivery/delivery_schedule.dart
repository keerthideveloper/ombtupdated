import 'package:ombt/modules/delivery.dart';
import 'package:ombt/pageMap.dart';

class DeliverySchedule extends StatefulWidget {
  final bool status;
  final int id;

  const DeliverySchedule({Key key, this.status, this.id}) : super(key: key);
  @override
  _DeliveryScheduleState createState() => _DeliveryScheduleState();
}

class _DeliveryScheduleState extends State<DeliverySchedule> {
  bool paidStatus;
  @override
  void initState() {
    print(widget.status == null);
    if (widget.status == null) {
      paidStatus = false;
    } else {
      paidStatus = true;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return CommonBody(
      appLogo: 'logisticManager.png',
      appTitle: "Delivery Schedule",
      onPressed: () {
        Navigator.pop(context);
      },
      child: Column(
        children: [
          SizedBox(
            height: 10.0,
          ),
          Expanded(
            child: ListView.builder(
                itemCount: vehicleList.length,
                itemBuilder: (BuildContext context, index) {
                  return Column(
                    children: [
                      SizedBox(
                        height: 10.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              Image.asset(
                                'assets/icons/logistics.png',
                              ),
                              SizedBox(
                                width: 10.0,
                              ),
                              Text(
                                vehicleList[index]['vehicleNo'],
                                style:
                                    TextStyle(fontSize: appSubHeadingFontSize),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Image.asset(
                                'assets/icons/tool-box3.png',
                              ),
                              SizedBox(
                                width: 10.0,
                              ),
                              Text(
                                vehicleList[index]['crates'],
                                style:
                                    TextStyle(fontSize: appSubHeadingFontSize),
                              ),
                            ],
                          ),
                        ],
                      ),
                      Divider(
                        color: appPrimaryColor,
                      ),
                      Container(
                        child: vehicleData(
                            context, vehicleList[index]['storeList'], index),
                      ),
                    ],
                  );
                }),
          ),
        ],
      ),
    );
  }

  vehicleData(context, store, i) {
    return ListView.builder(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemCount: store.length,
        itemBuilder: (context, index) {
          print(i);
          print(store[index]['selected']);
          return AppCard(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          RetailerLoading(vehicleId: i, storeId: index)));
              setState(() {
                store[index]['selected'] = !store[index]['selected'];
              });
            },
            child: Container(
              padding: const EdgeInsets.all(10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        store[index]['storeName'],
                        style: TextStyle(fontSize: appNormalTextSize),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Text(
                        'Customer ID :${store[index]['customerId']}',
                        style: TextStyle(fontSize: appNormalTextSize),
                      ),
                    ],
                  ),
                  Visibility(
                    visible: paidStatus
                        ? index == widget.id && store[index]['selected']
                        : false,
                    child: Image.asset('assets/icons/paid.png'),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(''),
                      SizedBox(
                        height: 10.0,
                      ),
                      Text(
                        store[index]['location'],
                        style: TextStyle(fontSize: appNormalTextSize),
                      ),
                    ],
                  ),
                  Container(
                    child: Column(
                      children: [
                        Image.asset(
                          store[index]['cratesImg'],
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Text(store[index]['crates'],style: TextStyle(fontSize: appNormalTextSize),),
                        SizedBox(
                          width: 10.0,
                        ),
                      ],
                    ),
                  ),
                  Visibility(
                    visible: store[index]['selected'],
                    child: Icon(
                      Icons.check_circle,
                      size: 18,
                      color: Colors.green,
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }
}

// vehicleData(context, store, i) {
//   return ListView.builder(
//       shrinkWrap: true,
//       physics: const NeverScrollableScrollPhysics(),
//       itemCount: store.length,
//       itemBuilder: (context, index) {
//         return GestureDetector(
//           onTap: () {
//             Navigator.push(
//                 context,
//                 MaterialPageRoute(
//                     builder: (context) =>
//                         RetailerLoading(vehicleId: i, storeId: index)));
//             setState(() {
//               store[index]['selected'] = !store[index]['selected'];
//             });
//           },
//           child: Card(
//             child: Container(
//               padding: const EdgeInsets.all(10.0),
//               child: Column(
//                 children: [
//                   Row(
//                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                     children: [
//                       Text(store[index]['storeName']),
//                       Container(
//                         child: Row(
//                           children: [
//                             Image.asset(
//                               store[index]['cratesImg'],
//                             ),
//                             SizedBox(
//                               width: 10.0,
//                             ),
//                             Text(store[index]['crates']),
//                             SizedBox(
//                               width: 10.0,
//                             ),
//                             Visibility(
//                               visible: store[index]['selected'],
//                               child: Icon(
//                                 Icons.check_circle,
//                                 size: 18,
//                                 color: Colors.green,
//                               ),
//                             ),
//                           ],
//                         ),
//                       ),
//                     ],
//                   ),
//                   SizedBox(
//                     height: 10.0,
//                   ),
//                   Row(
//                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                     children: [
//                       Text(store[index]['customerId']),
//                       // Visibility(
//                       //   visible: i == 0,
//                       //   child: Image.asset('assets/icons/paid.png'),
//                       // ),
//                       Text(store[index]['location']),
//                       Text(''),
//                       Text(''),
//                     ],
//                   ),
//                 ],
//               ),
//             ),
//           ),
//         );
//       });
// }
