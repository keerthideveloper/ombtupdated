import 'package:intl/intl.dart';
import 'package:ombt/pageMap.dart';

class Pickingatfc extends StatefulWidget {
  @override
  _PickingatfcState createState() => _PickingatfcState();
}

class _PickingatfcState extends State<Pickingatfc> {
  Color textcolor = Colors.green;
  double textsize = SizeConfig.textMultiplier;
  DateTime selectedDate = DateTime.now();
  TimeOfDay timein = TimeOfDay.now();
  TextEditingController datatimein = TextEditingController();
  Future<void> _selectDateTime(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    final TimeOfDay time = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.fromDateTime(selectedDate ?? DateTime.now()),
    );
    if (picked != null && time != null)
      setState(() {
        selectedDate = picked;
        timein = time;
        datatimein.text =
            "${DateFormat('dd/MMM/yyyy').format(selectedDate)} ${timein.format(context)}";
      });
  }

  @override
  Widget build(BuildContext context) {
    datatimein.text =
        "${DateFormat('dd/MMM/yyyy').format(selectedDate)}   ${timein.format(context)}";
    return CommonBody(
      appTitle: 'Picking List at FC',
      appLogo: 'logisticManager.png',
      onPressed: () => Navigator.pop(context),
      child: Column(
        children: [
          Container(
            child: TextFormField(
              controller: datatimein,
              style: TextStyle(color: Colors.green),
              readOnly: true,
              decoration: InputDecoration(
                  prefixIcon: Row(
                    children: [
                      Icon(
                        Icons.date_range,
                        color: Colors.green,
                      ),
                      SizedBox(
                        width: 10.0,
                      ),
                      Text(
                        '${DateFormat('dd/MMM/yyyy').format(selectedDate)}',
                        style: TextStyle(fontSize: appSubHeadingFontSize),
                      ),
                    ],
                  ),
                  suffix: Text(
                    ' ${timein.format(context)}',
                    style: TextStyle(
                        color: appTextColor, fontSize: appSubHeadingFontSize),
                  ),
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.green)),
                  disabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.green)),
                  border: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.green)),
                  enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.green))),
              onTap: () {
                setState(() {
                  _selectDateTime(context);
                  FocusScope.of(context).requestFocus(new FocusNode());
                });
              },
            ),
          ),
          SizedBox(
            height: 10.0,
          ),
          Expanded(
            child: gridboyin(textcolor),
          )
        ],
      ),
    );
  }

  Widget gridboyin(textcolor) {
    return OrientationBuilder(builder: (context, orientation) {
      bool checkorient = orientation == Orientation.landscape;
      return GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2, childAspectRatio: checkorient ? 2 : 1.2),
        itemCount: picklistfc.length,
        itemBuilder: (context, index) {
          return cardboyin(textcolor, index);
        },
      );
    });
  }

  Widget cardboyin(textcolor, index) {
    return AppCard(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => PickinglistEDC(
                      index: index,
                    )));
      },
      child: Column(
        children: [
          ListTile(
            title: Container(
                padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
                decoration: BoxDecoration(
                    border: Border(
                        left: BorderSide(
                            color: picklistfc[index]['color'], width: 4))),
                child: Text(
                  picklistfc[index]['title'],
                  style: TextStyle(
                      color: picklistfc[index]['color'],
                      fontSize: appSubHeadingFontSize),
                )),
            contentPadding: EdgeInsets.all(0),
            subtitle: Container(
                padding: EdgeInsets.fromLTRB(25, 0, 0, 0),
                child: Text(
                  picklistfc[index]['Subtitle'],
                  style:
                      TextStyle(color: textcolor, fontSize: appNormalTextSize),
                )),
          ),
          Row(children: [
            SizedBox(
              width: 10,
            ),
            Icon(
              Icons.location_on,
              color: Colors.blue,
            ),
            SizedBox(
              width: 10,
            ),
            Container(
              width: MediaQuery.of(context).size.width / 4,
              child: Wrap(
                children: [
                  Text(picklistfc[index]['address'],
                      style: TextStyle(
                          color: textcolor, fontSize: appNormalTextSize))
                ],
              ),
            )
          ])
        ],
      ),
    );
  }
}
