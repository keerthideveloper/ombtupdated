import 'package:ombt/pageMap.dart';

class Logesticmanage extends StatefulWidget {
  @override
  _LogesticmanageState createState() => _LogesticmanageState();
}

class _LogesticmanageState extends State<Logesticmanage> {
  double imagesize = 10 * SizeConfig.imageSizeMultiplier;
  double spacein = 1.2;

  List<dynamic> _listData = [
    {'image': 'assets/icons/fc_unloading.png', 'title': 'FC Unloading'},
    {'image': 'assets/icons/fc_picking.png', 'title': 'FC Picking'},
    {'image': 'assets/icons/fc_dispatch.png', 'title': 'FC Dispatch'},
    {'image': 'assets/icons/EDC_unloading.png', 'title': 'EDC Unloading'},
    {'image': 'assets/icons/EDC_picking.png', 'title': 'EDC Picking'},
    {'image': 'assets/icons/delivery_agent.png', 'title': 'Delivery Agent'},
  ];
  @override
  Widget build(BuildContext context) {
    return CommonBody(
      appLogo: 'logisticManager.png',
      appTitle: "Welcome to ekkam",
      onPressed: () => Navigator.of(context).pop(),
      child: GridView.builder(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2, childAspectRatio: 100 / 75),
          itemCount: _listData.length,
          itemBuilder: (BuildContext context, int index) {
            return AppCard(
              onTap: () {
                index == 1
                    ? Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Pickingatfc()))
                    : print('Not Yet Set');
              },
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: imagesize,
                      height: imagesize,
                      child: Image.asset(_listData[index]['image']),
                    ),
                    FittedBox(
                      fit: BoxFit.contain,
                      child: Text(_listData[index]['title'],
                          style: TextStyle(fontSize: appSubHeadingFontSize)),
                    ),
                  ]),
            );
          }),
    );
  }

  Widget dashboardin() {
    return StaggeredGridView.count(
      crossAxisCount: 2,
      crossAxisSpacing: 15.0,
      mainAxisSpacing: 15.0,
      padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 90.0),
      children: <Widget>[
        _buildTile(
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: imagesize,
                      height: imagesize,
                      child: Image.asset('assets/icons/fc_unloading.png'),
                    ),
                    FittedBox(
                      fit: BoxFit.contain,
                      child: Text('FC Unloading',
                          style: TextStyle(fontSize: appSubHeadingFontSize)),
                    ),
                  ]),
            ),
            onTap: () {}),
        _buildTile(
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: imagesize,
                      height: imagesize,
                      child: Image.asset('assets/icons/fc_picking.png'),
                    ),
                    FittedBox(
                      fit: BoxFit.contain,
                      child: Text('FC Picking',
                          style: TextStyle(fontSize: appSubHeadingFontSize)),
                    ),
                  ]),
            ),
            onTap: () => Navigator.push(context,
                MaterialPageRoute(builder: (context) => Pickingatfc()))
        ),
        _buildTile(
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: imagesize,
                      height: imagesize,
                      child: Image.asset('assets/icons/fc_dispatch.png'),
                    ),
                    FittedBox(
                      fit: BoxFit.contain,
                      child: Text('FC Dispatch',
                          style: TextStyle(fontSize: appSubHeadingFontSize)),
                    ),
                  ]),
            ), onTap: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => Outstanding()));
        }
        ),
        _buildTile(
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: imagesize,
                      height: imagesize,
                      child: Image.asset('assets/icons/EDC_unloading.png'),
                    ),
                    FittedBox(
                      fit: BoxFit.contain,
                      child: Text('EDC Unloading',
                          style: TextStyle(fontSize: appSubHeadingFontSize)),
                    ),
                  ]),
            ), onTap: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => Visitplan()));
        }),
        _buildTile(
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: imagesize,
                    height: imagesize,
                    child: Image.asset('assets/icons/EDC_picking.png'),
                  ),
                  FittedBox(
                    fit: BoxFit.contain,
                    child: Text('EDC Picking',
                        style: TextStyle(fontSize: appSubHeadingFontSize)),
                  ),
                ]),
          ),
          onTap: () => Navigator.push(
              context, MaterialPageRoute(builder: (context) => Order()))
        ),
        _buildTile(
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: imagesize,
                    height: imagesize,
                    child: Image.asset('assets/icons/delivery_agent.png'),
                  ),
                  FittedBox(
                    fit: BoxFit.contain,
                    child: Text('Delivery Agent',
                        style: TextStyle(fontSize: appSubHeadingFontSize)),
                  ),
                ]),
          ),
          onTap: () => Navigator.push(context,
              MaterialPageRoute(builder: (context) => Communication()))
        ),
      ],
      staggeredTiles: [
        StaggeredTile.extent(1, 120.0),
        StaggeredTile.extent(1, 120.0),
        StaggeredTile.extent(1, 120.0),
        StaggeredTile.extent(1, 120.0),
        StaggeredTile.extent(1, 120.0),
        StaggeredTile.extent(1, 120.0),
      ],
    );
  }
  //
  Widget _buildTile(Widget child, {Function() onTap, MaterialColor colorin}) {
    return Material(
        color: colorin,
        elevation: 10.0,
        borderRadius: BorderRadius.circular(5.0),
        // shadowColor: Color(0x802196F3),
        shadowColor: Color.fromRGBO(109, 195, 135, 1),
        child: InkWell(
            onTap: onTap != null
                ? () => onTap()
                : () {
                    print('Not set yet');
                  },
            child: child));
  }
}
