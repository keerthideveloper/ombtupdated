import 'package:ombt/pageMap.dart';

class DispatchEDC extends StatefulWidget {
  @override
  _DispatchEDCState createState() => _DispatchEDCState();
}

class _DispatchEDCState extends State<DispatchEDC> {
  List productdis = [
    {
      'imagepo': Image.asset('assets/icons/tomato.png'),
      'poname': 'Tomato',
      'poweight': '100 kg',
      'pocrates': '35'
    },
    {
      'imagepo': Image.asset('assets/icons/cauliflower.png'),
      'poname': 'Cauliflower',
      'poweight': '50 pcs',
      'pocrates': '15'
    }
  ];

  @override
  Widget build(BuildContext context) {
    return CommonBody(
      appLogo: 'logisticManager.png',
      appTitle: 'Dispatch For EDC-1',
      onPressed: () => Navigator.pop(context),
      child: Column(children: [
        Row(
          children: [
            Row(
              children: [
                Container(
                  width: 25,
                  child: Image.asset('assets/icons/logistics.png'),
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  'Vehicle List',
                  style: TextStyle(fontSize: appSubHeadingFontSize),
                )
              ],
            ),
            Spacer(flex: 1),
            Row(
              children: [
                Container(
                  width: 25,
                  child: Image.asset('assets/icons/tool-box-o.png'),
                ),
                SizedBox(
                  width: 10,
                ),
                Container(
                  width: 30,
                  child: Text(
                    '67',
                    style: TextStyle(fontSize: appSubHeadingFontSize),
                  ),
                )
              ],
            ),
          ],
        ),
        Divider(color: Colors.green, thickness: 1),
        Expanded(child: cardcreate()),
        SizedBox(
          height: 20,
        ),
        Container(
          width: MediaQuery.of(context).size.width,
          child: AppButton(
            name: 'Dispatch Vehicle',
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => FinalVehicle()));
            },
          ),
        ),
        SizedBox(
          height: 20,
        )
      ]),
    );
  }

  Widget cardcreate() {
    double widthspace = 10;
    return ListView.builder(
        itemCount: productdis.length,
        itemBuilder: (context, index) {
          return AppCard(
            child: Padding(
              padding: EdgeInsets.all(20),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: 150,
                      child: Row(
                        children: [
                          Container(
                            width: 20,
                            child: productdis[index]['imagepo'],
                          ),
                          SizedBox(
                            width: widthspace,
                          ),
                          Text(
                            productdis[index]['poname'],
                            style: TextStyle(fontSize: appNormalTextSize),
                          )
                        ],
                      ),
                    ),
                    Row(
                      children: [
                        Container(
                          height: 20,
                          child: VerticalDivider(
                            color: Colors.grey[300],
                            thickness: 1,
                          ),
                        ),
                        Text(
                          '${productdis[index]['poweight']}',
                          style: TextStyle(fontSize: appNormalTextSize),
                        ),
                        Container(
                          height: 20,
                          child: VerticalDivider(
                            color: Colors.grey[300],
                            thickness: 1,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Container(
                          width: 20,
                          child: Image.asset('assets/icons/tool-box-y.png'),
                        ),
                        SizedBox(
                          width: widthspace,
                        ),
                        Text(
                          productdis[index]['pocrates'],
                          style: TextStyle(fontSize: appNormalTextSize),
                        )
                      ],
                    ),
                  ]),
            ),
          );
        });
  }
}
