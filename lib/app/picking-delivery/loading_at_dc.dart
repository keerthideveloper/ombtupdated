import 'package:ombt/modules/unloading.dart';
import 'package:ombt/pageMap.dart';

class LoadingAtDc extends StatefulWidget {
  @override
  _LoadingAtDcState createState() => _LoadingAtDcState();
}

class _LoadingAtDcState extends State<LoadingAtDc> {
  @override
  Widget build(BuildContext context) {
    return CommonBody(
      appLogo: 'logisticManager.png',
      appTitle: 'Loading at DC',
      onPressed: () {
        Navigator.pop(context);
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Spacer(
            flex: 1,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Image.asset(
                    'assets/icons/logistics.png',
                  ),
                  SizedBox(
                    width: 10.0,
                  ),
                  Text('Vehicle List',
                      style: TextStyle(fontSize: appSubHeadingFontSize)),
                ],
              ),
              Row(
                children: [
                  Image.asset(
                    'assets/icons/tool-box3.png',
                  ),
                  SizedBox(
                    width: 10.0,
                  ),
                  Text('21/August/2020',
                      style: TextStyle(fontSize: appSubHeadingFontSize)),
                ],
              ),
            ],
          ),
          Divider(
            color: appPrimaryColor,
          ),
          SizedBox(
            height: 1.6 * SizeConfig.heightMultiplier,
          ),
          Flexible(
            child: GridView.builder(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  mainAxisSpacing: 10,
                  crossAxisSpacing: 10,
                ),
                itemCount: unloadingData.length,
                itemBuilder: (BuildContext context, index) {
                  Unloading _unloadingData = unloadingData[index];
                  return AppCard(
                    color: _unloadingData.color,
                    elevation: 0,
                    child: Container(
                      padding: EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Row(
                            children: [
                              Image.asset('assets/icons/logisticsWhite.png'),
                              SizedBox(
                                width: 12.0,
                              ),
                              Text(
                                _unloadingData.vehicleNo,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: appNormalTextSize),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              _unloadingData.color == Color(0xffFF854D)
                                  ? Image.asset('assets/icons/user (2)-2.png')
                                  : Image.asset('assets/icons/user (2)-1.png'),
                              SizedBox(
                                width: 12.0,
                              ),
                              Text(
                                _unloadingData.storeName,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: appNormalTextSize),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Image.asset('assets/icons/phone-1.png'),
                              SizedBox(
                                width: 12.0,
                              ),
                              Text(
                                _unloadingData.mobileNo,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: appNormalTextSize),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Image.asset('assets/icons/tool-boxWhite.png'),
                              SizedBox(
                                width: 12.0,
                              ),
                              Text(
                                _unloadingData.crates.toString(),
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: appNormalTextSize),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  );
                }),
            flex: 8,
          ),
          SizedBox(
            height: SizeConfig.heightMultiplier * 1.8,
          ),
          Container(
            height: SizeConfig.heightMultiplier * 13.0,
            padding: EdgeInsets.all(8.0),
            decoration: BoxDecoration(
              border: Border.all(
                color: Color(0xffFF854D),
              ),
            ),
            child: Text(
              'Comments: \n\nIntegration with locus will provide the store details for sequential loading of crates according to the store locations.',
              style: TextStyle(
                color: Color(0xffFF854D),
                fontSize: appNormalTextSize,
              ),
            ),
          ),
          SizedBox(
            height: SizeConfig.heightMultiplier * 1.8,
          ),
          AppButton(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => DeliverySchedule()));
            },
            name: 'Submit',
          ),
          Spacer(
            flex: 1,
          ),
        ],
      ),
    );
  }
}
