import 'package:ombt/modules/delivery.dart';
import 'package:ombt/pageMap.dart';

class RetailerLoading extends StatefulWidget {
  final int vehicleId;
  final int storeId;

  const RetailerLoading({Key key, this.vehicleId, this.storeId})
      : super(key: key);
  @override
  _RetailerLoadingState createState() => _RetailerLoadingState();
}

class _RetailerLoadingState extends State<RetailerLoading> {
  @override
  Widget build(BuildContext context) {
    return CommonBody(
      appLogo: 'logisticManager.png',
      appTitle: 'Retailer Loading',
      onPressed: () {
        Navigator.pop(context);
      },
      child: Column(
        children: [
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Row(
                  children: [
                    Image.asset('assets/icons/logistics.png'),
                    SizedBox(width: 10.0),
                    Text(
                      vehicleList[widget.vehicleId]['vehicleNo'],
                      style: TextStyle(fontSize: appSubHeadingFontSize),
                    ),
                  ],
                ),
                SizedBox(
                  height: 7.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                        vehicleList[widget.vehicleId]['storeList']
                            [widget.storeId]['storeName'],
                        style: TextStyle(fontSize: appSubHeadingFontSize)),
                    Container(
                      child: Row(
                        children: [
                          Image.asset(vehicleList[widget.vehicleId]['storeList']
                              [widget.storeId]['cratesImg']),
                          SizedBox(width: 10.0),
                          Text(vehicleList[widget.vehicleId]['crates'],
                              style:
                                  TextStyle(fontSize: appSubHeadingFontSize)),
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 7.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                        'Customer Id: ${vehicleList[widget.vehicleId]['storeList'][widget.storeId]['customerId']}',
                        style: TextStyle(fontSize: appSubHeadingFontSize)),
                    Text(
                        vehicleList[widget.vehicleId]['storeList']
                            [widget.storeId]['location'],
                        style: TextStyle(fontSize: appSubHeadingFontSize)),
                    Text(''),
                  ],
                ),
                SizedBox(
                  height: 7.0,
                ),
                Divider(
                  color: appPrimaryColor,
                ),
                Expanded(
                  child: ListView.builder(
                      itemCount: vehicleList[widget.vehicleId]['storeList']
                              [widget.storeId]['productList']
                          .length,
                      itemBuilder: (BuildContext context, index) {
                        return AppCard(
                          child: Container(
                            padding: EdgeInsets.all(10.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  width: SizeConfig.widthMultiplier * 7,
                                  child: Radio(
                                      value: null,
                                      groupValue: null,
                                      onChanged: (v) {}),
                                ),
                                Container(
                                  width: SizeConfig.widthMultiplier * 36,
                                  child: Row(
                                    children: [
                                      Image.asset(vehicleList[widget.vehicleId]
                                              ['storeList'][widget.storeId]
                                          ['productList'][index]['productImg']),
                                      SizedBox(
                                        width: 10.0,
                                      ),
                                      Text(
                                          vehicleList[widget.vehicleId]
                                                          ['storeList']
                                                      [widget.storeId]
                                                  ['productList'][index]
                                              ['productName'],
                                          style: TextStyle(
                                              fontSize: appNormalTextSize)),
                                    ],
                                  ),
                                ),
                                Container(
                                  height: 25,
                                  child: VerticalDivider(
                                    color: Colors.green,
                                    thickness: 0.5,
                                  ),
                                ),
                                Text(
                                    vehicleList[widget.vehicleId]['storeList']
                                            [widget.storeId]['productList']
                                        [index]['productQty'],
                                    style:
                                        TextStyle(fontSize: appNormalTextSize)),
                                Container(
                                  height: 25,
                                  child: VerticalDivider(
                                    color: Colors.green,
                                    thickness: 0.5,
                                  ),
                                ),
                                Container(
                                  child: Row(
                                    children: [
                                      Image.asset(vehicleList[widget.vehicleId]
                                              ['storeList'][widget.storeId]
                                          ['productList'][index]['crates']),
                                      SizedBox(
                                        width: 10.0,
                                      ),
                                      Text(
                                          vehicleList[widget.vehicleId]
                                                          ['storeList']
                                                      [widget.storeId]
                                                  ['productList'][index]
                                              ['cratesQty'],
                                          style: TextStyle(
                                              fontSize: appNormalTextSize)),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      }),
                ),
                AppButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => RetailerUnloading(
                                  vehicleId: widget.vehicleId,
                                  storeId: widget.storeId,
                                )));
                  },
                  name: 'Generate Temporary Invoice',
                ),
              ],
            ),
            flex: 8,
          ),
          Spacer(
            flex: 1,
          ),
        ],
      ),
    );
  }
}
