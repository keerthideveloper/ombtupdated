import 'package:ombt/modules/unloading.dart';
import 'package:ombt/pageMap.dart';

class UnloadingAtEDC extends StatefulWidget {
  @override
  _UnloadingAtEDCState createState() => _UnloadingAtEDCState();
}

class _UnloadingAtEDCState extends State<UnloadingAtEDC> {
  @override
  Widget build(BuildContext context) {
    return CommonBody(
      appLogo: 'logisticManager.png',
      appTitle: 'Unloading at EDC-1',
      onPressed: () {
        Navigator.pop(context);
      },
      child: Column(
        children: [
          Spacer(
            flex: 1,
          ),
          Flexible(
            child: GridView.builder(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  mainAxisSpacing: 10,
                  crossAxisSpacing: 10,
                ),
                itemCount: unloadingData.length,
                itemBuilder: (BuildContext context, index) {
                  Unloading _unloadingData = unloadingData[index];
                  return GestureDetector(
                    child: AppCard(
                      onTap: () {
                        // Navigator.push(
                        //     context,
                        //     MaterialPageRoute(
                        //         builder: (context) => VehicleWiseUnloadingAtDC(
                        //               vehicleId: _unloadingData.vehicleId,
                        //             )));
                      },
                      elevation: 0,
                      color: _unloadingData.color,
                      child: Container(
                        padding: EdgeInsets.all(10.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Image.asset('assets/icons/logisticsWhite.png'),
                                SizedBox(
                                  width: 12.0,
                                ),
                                Text(
                                  _unloadingData.vehicleNo,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: appNormalTextSize),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                _unloadingData.color == Color(0xffFF854D)
                                    ? Image.asset('assets/icons/user (2)-2.png')
                                    : Image.asset(
                                        'assets/icons/user (2)-1.png'),
                                SizedBox(
                                  width: 12.0,
                                ),
                                Text(
                                  _unloadingData.storeName,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: appNormalTextSize),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Image.asset('assets/icons/phone-1.png'),
                                SizedBox(
                                  width: 12.0,
                                ),
                                Text(
                                  _unloadingData.mobileNo,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: appNormalTextSize),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Image.asset('assets/icons/tool-boxWhite.png'),
                                SizedBox(
                                  width: 12.0,
                                ),
                                Text(
                                  _unloadingData.crates.toString(),
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: appNormalTextSize),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                }),
            flex: 8,
          ),
          Spacer(
            flex: 1,
          ),
        ],
      ),
    );
  }
}
