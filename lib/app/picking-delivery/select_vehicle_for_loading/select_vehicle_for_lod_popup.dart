import 'package:ombt/pageMap.dart';

Widget popupin(val, widthin, king) {
  return OrientationBuilder(builder: (context, orientation) {
    bool judge = orientation == Orientation.landscape;
    return judge
        ? cardboy(judge, val, widthin, king)
        : cardboy(judge, val, widthin, king);
  });
}

Widget cardboy(judge, val, widthin, king) {
  // double headcard = SizeConfig.textMultiplier * 2.5;
  return Stack(
    children: [
      Positioned(
          bottom: judge ? 10 : 50,
          top: judge ? 10 : null,
          right: judge ? widthin / 5 : null,
          left: judge ? widthin / 5 : null,
          child: Container(
            width: judge ? widthin / 2 : widthin,
            child: Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30)),
              child: Padding(
                padding: const EdgeInsets.all(30.0),
                child: SingleChildScrollView(
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Spacer(flex: 1),
                            Container(
                                width: 25,
                                height: 25,
                                child: pickinglistEDC[val]['productimg']),
                            SizedBox(width: 15),
                            Text(pickinglistEDC[val]['productname']),
                            Spacer(flex: 1),
                          ],
                        ),
                        Divider(
                          color: Colors.yellow,
                          thickness: 1,
                        ),
                        SizedBox(height: 15),
                        GestureDetector(
                          child: Text('Start......'),
                          onTap: king,
                        )
                      ]),
                ),
              ),
            ),
          ))
    ],
  );
}
