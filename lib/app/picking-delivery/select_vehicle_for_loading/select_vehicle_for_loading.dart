import 'package:intl/intl.dart';
import 'package:ombt/pageMap.dart';

class SelectVehicle extends StatefulWidget {
  @override
  _SelectVehicleState createState() => _SelectVehicleState();
}

class _SelectVehicleState extends State<SelectVehicle> {
  DateTime selectedDate = DateTime.now();
  double textsizein = SizeConfig.textMultiplier;
  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

  @override
  Widget build(BuildContext context) {
    return CommonBody(
      appLogo: 'logisticManager.png',
      appTitle: 'Select Vehicle for Loading',
      onPressed: () => Navigator.pop(context),
      child: Column(children: [
        Row(
          children: [
            Row(
              children: [
                Container(
                  width: 30,
                  child: Image.asset('assets/icons/logistics.png'),
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  'Vehicle List',
                  style: TextStyle(fontSize: appSubHeadingFontSize),
                )
              ],
            ),
            Spacer(flex: 1),
            GestureDetector(
              child: Row(
                children: [
                  Icon(
                    Icons.today,
                    color: Colors.green,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Container(
                    child: Text(
                      '${DateFormat('dd/MMM/yyyy').format(selectedDate.toLocal())}',
                      style: TextStyle(fontSize: appSubHeadingFontSize),
                    ),
                  )
                ],
              ),
              onTap: () => _selectDate(context),
            ),
          ],
        ),
        Divider(color: Colors.green, thickness: 1),
        Expanded(child: cardcreate()),
        SizedBox(
          height: 20,
        ),
        Container(
          width: MediaQuery.of(context).size.width,
          child: AppButton(
            name: 'SUBMIT',
            onPressed: () {},
          ),
        ),
        SizedBox(
          height: 20,
        )
      ]),
    );
  }

  Widget cardcreate() {
    var widthin = MediaQuery.of(context).size.width;
    Function king = () => Navigator.push(
        context, MaterialPageRoute(builder: (context) => DispatchEDC()));
    double widthspace = 10;
    return GridView.builder(
        gridDelegate:
            SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
        itemCount: selectVehicle.length,
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: () {
              showDialog(
                  context: context,
                  builder: (context) {
                    return popupin(index, widthin, king);
                  });
            },
            child: AppCard(
              elevation: 0.0,
              color: selectVehicle[index]['cardcolor'],
              child: Padding(
                padding: EdgeInsets.all(20),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Container(
                            width: 20,
                            child: Image.asset('assets/icons/logistics-w.png'),
                          ),
                          SizedBox(
                            width: widthspace,
                          ),
                          Text(
                            selectVehicle[index]['vehiclenumber'],
                            style: TextStyle(
                                fontSize: appNormalTextSize,
                                color: Colors.white),
                          )
                        ],
                      ),
                      Row(
                        children: [
                          Container(
                            width: 15,
                            child: Image.asset('assets/icons/user.png'),
                          ),
                          SizedBox(
                            width: widthspace,
                          ),
                          Text(
                            selectVehicle[index]['personname'],
                            style: TextStyle(
                                fontSize: appNormalTextSize,
                                color: Colors.white),
                          )
                        ],
                      ),
                      Row(
                        children: [
                          Container(
                            width: 15,
                            child: Image.asset('assets/icons/phone-1.png'),
                          ),
                          SizedBox(
                            width: widthspace,
                          ),
                          Text(
                            selectVehicle[index]['callnnumber'],
                            style: TextStyle(
                                fontSize: appNormalTextSize,
                                color: Colors.white),
                          )
                        ],
                      ),
                      Row(
                        children: [
                          Container(
                            width: 20,
                            child: Image.asset('assets/icons/tool-box-w.png'),
                          ),
                          SizedBox(
                            width: widthspace,
                          ),
                          Text(
                            selectVehicle[index]['cartnumber'],
                            style: TextStyle(
                                fontSize: appNormalTextSize,
                                color: Colors.white),
                          )
                        ],
                      ),
                    ]),
              ),
            ),
          );
        });
  }
}
