import 'package:ombt/pageMap.dart';

class PickinglistEDC extends StatefulWidget {
  final int index;
  PickinglistEDC({Key key, this.index}) : super(key: key);
  @override
  _PickinglistEDCState createState() => _PickinglistEDCState();
}

class _PickinglistEDCState extends State<PickinglistEDC> {
  @override
  Widget build(BuildContext context) {
    return CommonBody(
      appTitle: 'Picking List of ${picklistfc[widget.index]['title']}',
      appLogo: 'logisticManager.png',
      onPressed: () {
        Navigator.pop(context);
        setState(() {
          for (var i = 0; i < pickinglistEDC.length; i++) {
            pickinglistEDC[i]['checkin'] = false;
          }
        });
      },
      child: SingleChildScrollView(
        primary: false,
        child: Column(children: [
          Column(
            children: [
              ListTile(
                contentPadding: EdgeInsets.all(0),
                title: Text(
                  picklistfc[widget.index]['title'],
                  style: TextStyle(
                      color: picklistfc[widget.index]['color'],
                      fontSize: appSubHeadingFontSize),
                ),
                subtitle: Row(
                  children: [
                    Icon(Icons.location_on, color: Colors.green),
                    Text(
                      picklistfc[widget.index]['Subtitle'],
                      style: TextStyle(fontSize: appSubHeadingFontSize),
                    ),
                    Spacer(
                      flex: 1,
                    ),
                    Text(
                      'Pune',
                      style: TextStyle(fontSize: appSubHeadingFontSize),
                    )
                  ],
                ),
              ),
              Divider(
                color: Colors.green,
                thickness: 1.5,
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          SizedBox(
              height: MediaQuery.of(context).size.height / 2.6,
              child: makeList()),
          SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Text(
                'Total No of Crates for EDC1:',
                style: TextStyle(fontSize: appNormalTextSize),
              ),
              Spacer(flex: 4),
              Text(
                '85',
                style: TextStyle(fontSize: appNormalTextSize),
              ),
              Spacer(flex: 1)
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            child: AppButton(
              name: 'Allocate Vehicle to ${picklistfc[widget.index]['title']}',
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => SelectVehicle()));
              },
            ),
          )
        ]),
      ),
    );
  }

  int changer;
  List changevalue = [null, null, null, null, null];

  void makechange(val) {
    setState(() {
      changer = val;
      changevalue[val] = changer;
      print(changevalue);
      showDialog(
          context: context,
          builder: (context) {
            double widthin = MediaQuery.of(context).size.width;
            void king() {
              setState(() {
                pickinglistEDC[val]['checkin'] = true;
              });
              Navigator.of(context).pop();
            }

            return popupin(val, widthin, king);
          });
    });
  }

  Widget makeList() {
    return ListView.builder(
        itemCount: pickinglistEDC.length,
        itemBuilder: (context, index) {
          return cardboy(index);
        });
  }

  Widget cardboy(index) {
    return AppCard(
        child: OutlineButton(
      textColor: Colors.green,
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      padding: EdgeInsets.all(0),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
      highlightedBorderColor: Colors.green,
      borderSide: BorderSide(color: Colors.green[100]),
      onPressed: () {},
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: IntrinsicHeight(
          child: Row(
            children: [
              Radio(
                  value: index,
                  groupValue: pickinglistEDC[index]['checkin']
                      ? changevalue[index]
                      : changer,
                  onChanged: makechange),
              Spacer(
                flex: 1,
              ),
              Container(
                width: 120,
                child: Row(
                  children: [
                    Container(
                        width: 25,
                        height: 25,
                        child: pickinglistEDC[index]['productimg']),
                    SizedBox(width: 15),
                    Text(
                      pickinglistEDC[index]['productname'],
                      style: TextStyle(fontSize: appNormalTextSize),
                    ),
                  ],
                ),
              ),
              Spacer(
                flex: 2,
              ),
              Container(
                height: 25,
                child: VerticalDivider(
                  color: Colors.green,
                  thickness: 0.5,
                ),
              ),
              Text(
                pickinglistEDC[index]['productweight'],
                style: TextStyle(fontSize: appNormalTextSize),
              ),
              Container(
                height: 25,
                child: VerticalDivider(
                  color: Colors.green,
                  thickness: 0.5,
                ),
              ),
              Spacer(
                flex: 1,
              ),
              Container(
                  width: 15,
                  height: 15,
                  child: pickinglistEDC[index]['productcrateimg']),
              SizedBox(width: 10),
              Text(
                pickinglistEDC[index]['productcrateno'],
                style: TextStyle(fontSize: appNormalTextSize),
              ),
              Spacer(
                flex: 2,
              ),
              pickinglistEDC[index]['checkin']
                  ?
                  // ignore: dead_code
                  CircleAvatar(
                      radius: 10,
                      backgroundColor: Colors.green,
                      child: Icon(
                        Icons.check,
                        color: Colors.white,
                        size: 15,
                      ),
                    )
                  // ignore: dead_code
                  : SizedBox()
            ],
          ),
        ),
      ),
    ));
  }
}
