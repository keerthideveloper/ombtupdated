import 'package:ombt/pageMap.dart';

Widget popupin(val, widthin, king) {
  return OrientationBuilder(builder: (context, orientation) {
    bool judge = orientation == Orientation.landscape;
    return judge
        ? cardboy(judge, val, widthin, king)
        : cardboy(judge, val, widthin, king);
  });
}

Widget cardboy(judge, val, widthin, king) {
  // double headcard = SizeConfig.textMultiplier * 2.5;
  return Stack(
    children: [
      Positioned(
          bottom: judge ? 10 : 50,
          top: judge ? 10 : null,
          right: judge ? widthin / 5 : null,
          left: judge ? widthin / 5 : null,
          child: Container(
            width: judge ? widthin / 2 : widthin,
            child: Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30)),
              child: Padding(
                padding: const EdgeInsets.all(30.0),
                child: SingleChildScrollView(
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Spacer(flex: 1),
                            Container(
                              width: 20,
                              child: Image.asset('assets/icons/logistics.png'),
                            ),
                            SizedBox(width: 15),
                            Text(
                              selectVehicle[val]['vehiclenumber'],
                              style: TextStyle(
                                  fontSize: appSubHeadingFontSize,
                                  color: selectVehicle[val]['cardcolor']),
                            ),
                            Spacer(flex: 1),
                          ],
                        ),
                        SizedBox(height: 15),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Image.asset(
                                  'assets/icons/tool-box-o.png',
                                  width: 25,
                                ),
                                SizedBox(
                                  height: 10.0,
                                ),
                                Text(
                                  'No of Crates',
                                  style: TextStyle(
                                    color: selectVehicle[val]['cardcolor'],
                                    fontSize: appSubHeadingFontSize,
                                  ),
                                )
                              ],
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            Container(
                              width: 40,
                              height: 40,
                              decoration: BoxDecoration(
                                  border: Border.all(color: Colors.grey[200]),
                                  borderRadius: BorderRadius.circular(5)),
                              child: AppTextField(
                                labelName: '7',
                                focusEnableColor: appPrimaryColor,
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            GestureDetector(
                              child: Icon(
                                Icons.arrow_forward,
                                color: Colors.green,
                              ),
                              onTap: king,
                            )
                          ],
                        )
                      ]),
                ),
              ),
            ),
          ))
    ],
  );
}
