import 'package:ombt/pageMap.dart';

class FinalVehicle extends StatefulWidget {
  @override
  _FinalVehicleState createState() => _FinalVehicleState();
}

List descussin = [false, false, false, false];

class _FinalVehicleState extends State<FinalVehicle> {
  @override
  Widget build(BuildContext context) {
    return CommonBody(
      appLogo: 'logisticManager.png',
      appTitle: 'Final Vehicles Dispatch Status',
      onPressed: () => Navigator.pop(context),
      child: Column(children: [
        SizedBox(
          height: 20,
        ),
        Expanded(child: cardcreate()),
        SizedBox(
          height: 20,
        ),
        Container(
          width: MediaQuery.of(context).size.width,
          child: AppButton(
            name: 'All Vehicles Dispatched',
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => UnloadingAtEDC()));
            },
          ),
        ),
        SizedBox(
          height: 20,
        )
      ]),
    );
  }

  Widget cardcreate() {
    double widthspace = 10;
    return OrientationBuilder(builder: (context, orientation) {
      bool orient = orientation == Orientation.landscape;
      return GridView.builder(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              childAspectRatio: orient ? 1 : 2 / 2,
              mainAxisSpacing: 10,
              crossAxisSpacing: 10),
          itemCount: selectVehicle.length,
          itemBuilder: (context, index) {
            return GestureDetector(
              onTap: () {
                setState(() {
                  // for (var i = 0; i < descussin.length; i++) {
                  //   if (i == index) {
                  //     descussin[i] = true;
                  //   } else {
                  //     descussin[i] = false;
                  //   }
                  // }
                  descussin[index] = !descussin[index];
                });
              },
              child: Stack(
                children: [
                  AppCard(
                    color: selectVehicle[index]['cardcolor'],
                    elevation: 0,
                    child: Padding(
                      padding: EdgeInsets.all(20),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Container(
                                  width: 20,
                                  child: Image.asset(
                                      'assets/icons/logistics-w.png'),
                                ),
                                SizedBox(
                                  width: widthspace,
                                ),
                                Text(
                                  selectVehicle[index]['vehiclenumber'],
                                  style: TextStyle(
                                      fontSize: appNormalTextSize,
                                      color: Colors.white),
                                )
                              ],
                            ),
                            Row(
                              children: [
                                Container(
                                  width: 15,
                                  child: Image.asset('assets/icons/user.png'),
                                ),
                                SizedBox(
                                  width: widthspace,
                                ),
                                Text(
                                  selectVehicle[index]['personname'],
                                  style: TextStyle(
                                      fontSize: appNormalTextSize,
                                      color: Colors.white),
                                )
                              ],
                            ),
                            Row(
                              children: [
                                Container(
                                  width: 15,
                                  child:
                                      Image.asset('assets/icons/phone-1.png'),
                                ),
                                SizedBox(
                                  width: widthspace,
                                ),
                                Text(
                                  selectVehicle[index]['callnnumber'],
                                  style: TextStyle(
                                      fontSize: appNormalTextSize,
                                      color: Colors.white),
                                )
                              ],
                            ),
                            Row(
                              children: [
                                Container(
                                  width: 20,
                                  child: Image.asset(
                                      'assets/icons/tool-box-w.png'),
                                ),
                                SizedBox(
                                  width: widthspace,
                                ),
                                Text(
                                  selectVehicle[index]['cartnumber'],
                                  style: TextStyle(
                                      fontSize: appNormalTextSize,
                                      color: Colors.white),
                                )
                              ],
                            ),
                          ]),
                    ),
                  ),
                  Positioned(
                    right: 8,
                    top: 8,
                    child: descussin[index]
                        ? CircleAvatar(
                            radius: 10,
                            backgroundColor: Colors.green,
                            child: Center(
                              child: Icon(
                                Icons.check,
                                color: Colors.white,
                                size: 15,
                              ),
                            ))
                        : SizedBox.shrink(),
                  )
                ],
              ),
            );
          });
    });
  }
}
