import 'package:ombt/modules/delivery.dart';
import 'package:ombt/pageMap.dart';

class RetailerInvoicing extends StatefulWidget {
  final int vehicleId;
  final int storeId;

  const RetailerInvoicing({Key key, this.vehicleId, this.storeId})
      : super(key: key);
  @override
  _RetailerInvoicingState createState() => _RetailerInvoicingState();
}

class _RetailerInvoicingState extends State<RetailerInvoicing> {
  bool toggleValue = true;
  TextEditingController receivedCratesController = TextEditingController();
  TextEditingController pendingCratesController = TextEditingController();
  TextEditingController paidController = TextEditingController();

  @override
  void initState() {
    receivedCratesController.text = '15';
    pendingCratesController.text = '5';
    paidController.text = '2500';
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return CommonBody(
      appLogo: 'logisticManager.png',
      appTitle: 'Retailer Invoicing',
      onPressed: () {
        Navigator.pop(context);
      },
      child: Column(
        children: [
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Row(
                  children: [
                    Image.asset('assets/icons/logistics.png'),
                    SizedBox(width: 10.0),
                    Text(vehicleList[widget.vehicleId]['vehicleNo'],
                        style: TextStyle(fontSize: appSubHeadingFontSize)),
                  ],
                ),
                SizedBox(
                  height: 7.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                        vehicleList[widget.vehicleId]['storeList']
                            [widget.storeId]['storeName'],
                        style: TextStyle(fontSize: appSubHeadingFontSize)),
                    Container(
                      child: Row(
                        children: [
                          Image.asset(vehicleList[widget.vehicleId]['storeList']
                              [widget.storeId]['cratesImg']),
                          SizedBox(width: 10.0),
                          Text(vehicleList[widget.vehicleId]['crates'],
                              style:
                                  TextStyle(fontSize: appSubHeadingFontSize)),
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 7.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                        'Customer Id: ${vehicleList[widget.vehicleId]['storeList'][widget.storeId]['customerId']}',
                        style: TextStyle(fontSize: appSubHeadingFontSize)),
                    Text(''),
                    Text(
                        vehicleList[widget.vehicleId]['storeList']
                            [widget.storeId]['location'],
                        style: TextStyle(fontSize: appSubHeadingFontSize)),
                  ],
                ),
                SizedBox(
                  height: 7.0,
                ),
                Divider(
                  color: Color(0xff6BCC8C),
                ),
                Expanded(
                  child: ListView.builder(
                      itemCount: vehicleList[widget.vehicleId]['storeList']
                              [widget.storeId]['productList']
                          .length,
                      itemBuilder: (BuildContext context, index) {
                        return AppCard(
                          child: Container(
                            padding: EdgeInsets.all(20.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  width: SizeConfig.widthMultiplier * 30,
                                  child: Row(
                                    children: [
                                      Image.asset(vehicleList[widget.vehicleId]
                                              ['storeList'][widget.storeId]
                                          ['productList'][index]['productImg']),
                                      SizedBox(
                                        width: 10.0,
                                      ),
                                      Text(
                                          vehicleList[widget.vehicleId]
                                                          ['storeList']
                                                      [widget.storeId]
                                                  ['productList'][index]
                                              ['productName'],
                                          style: TextStyle(
                                              fontSize: appNormalTextSize)),
                                    ],
                                  ),
                                ),
                                Container(
                                  height: 25,
                                  child: VerticalDivider(
                                    color: Color(0xff6BCC8C).withOpacity(0.4),
                                  ),
                                ),
                                Text(
                                    vehicleList[widget.vehicleId]['storeList']
                                            [widget.storeId]['productList']
                                        [index]['productQty'],
                                    style:
                                        TextStyle(fontSize: appNormalTextSize)),
                                Container(
                                  height: 25,
                                  child: VerticalDivider(
                                    color: Color(0xff6BCC8C).withOpacity(0.4),
                                  ),
                                ),
                                Container(
                                  width: SizeConfig.widthMultiplier * 15,
                                  child: Row(
                                    children: [
                                      Image.asset(vehicleList[widget.vehicleId]
                                              ['storeList'][widget.storeId]
                                          ['productList'][index]['crates']),
                                      SizedBox(
                                        width: 10.0,
                                      ),
                                      Text(
                                          vehicleList[widget.vehicleId]
                                                          ['storeList']
                                                      [widget.storeId]
                                                  ['productList'][index]
                                              ['cratesQty'],
                                          style: TextStyle(
                                              fontSize: appNormalTextSize)),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      }),
                ),
                AppButton(
                  onPressed: () {},
                  name: 'Final Invoice',
                ),
                SizedBox(
                  height: 10.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: Row(
                        children: [
                          Text('Received Crates:',
                              style: TextStyle(fontSize: appNormalTextSize)),
                          SizedBox(
                            width: 6.0,
                          ),
                          Container(
                            height: SizeConfig.heightMultiplier * 5,
                            width: SizeConfig.widthMultiplier * 13.5,
                            child: AppTextField(),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      child: Row(
                        children: [
                          Text('Pending Crates:',
                              style: TextStyle(
                                  color: Color(0xffFF854D),
                                  fontSize: appNormalTextSize)),
                          SizedBox(
                            width: 6.0,
                          ),
                          Container(
                            height: SizeConfig.heightMultiplier * 5,
                            width: SizeConfig.widthMultiplier * 13.5,
                            child: AppTextField(
                              focusEnableColor:
                                  Color(0xffFF854D).withOpacity(0.5),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: Row(
                        children: [
                          Text(
                            'Total Amount To be Paid: Rs 2500',
                            style: TextStyle(fontSize: appNormalTextSize),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      child: Row(
                        children: [
                          Image.asset('assets/icons/fast.png'),
                          SizedBox(
                            width: 5.0,
                          ),
                          Text(
                            '1:45 pm',
                            style: TextStyle(fontSize: appNormalTextSize),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: Row(
                        children: [
                          Text(
                            'Payment Received',
                            style: TextStyle(fontSize: appNormalTextSize),
                          ),
                          SizedBox(
                            width: 10.0,
                          ),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                toggleValue = !toggleValue;
                              });
                            },
                            child: Container(
                              height: SizeConfig.heightMultiplier * 4.2,
                              width: SizeConfig.widthMultiplier * 17.3,
                              decoration: BoxDecoration(
                                color: toggleValue
                                    ? Color(0xff58BC7A)
                                    : Color(0xff00953B),
                                borderRadius: BorderRadius.circular(2.0),
                              ),
                              child: Row(
                                children: [
                                  Expanded(
                                    flex: toggleValue ? 4 : 2,
                                    child: Container(
                                      decoration: BoxDecoration(
                                        color: toggleValue
                                            ? Color(0xff00953B)
                                            : Color(0xff58BC7A),
                                        borderRadius:
                                            BorderRadius.circular(2.0),
                                      ),
                                      child: Center(
                                        child: Visibility(
                                          visible: toggleValue,
                                          child: Text(
                                            'Yes',
                                            style: TextStyle(
                                                fontSize: appNormalTextSize,
                                                color: Colors.white),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    flex: toggleValue ? 2 : 4,
                                    child: Container(
                                      child: Center(
                                        child: Visibility(
                                          visible: !toggleValue,
                                          child: Text(
                                            'No',
                                            style: TextStyle(
                                                fontSize: appNormalTextSize,
                                                color: Colors.white),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: SizeConfig.heightMultiplier * 5,
                      width: SizeConfig.widthMultiplier * 27,
                      child: AppTextField(),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10.0,
                ),
                AppButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => DeliverySchedule(
                          status: true,
                          id: widget.storeId,
                        ),
                      ),
                    );
                  },
                  name: 'DONE',
                ),
              ],
            ),
            flex: 8,
          ),
          Spacer(
            flex: 1,
          ),
        ],
      ),
    );
  }
}
