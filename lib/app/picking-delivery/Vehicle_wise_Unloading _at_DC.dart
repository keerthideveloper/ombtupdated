import 'package:ombt/modules/unloading.dart';
import 'package:ombt/pageMap.dart';

class VehicleWiseUnloadingAtDC extends StatefulWidget {
  final int vehicleId;

  const VehicleWiseUnloadingAtDC({Key key, this.vehicleId}) : super(key: key);
  @override
  _VehicleWiseUnloadingAtDCState createState() =>
      _VehicleWiseUnloadingAtDCState();
}

class _VehicleWiseUnloadingAtDCState extends State<VehicleWiseUnloadingAtDC> {
  String vehicleNo;
  bool selectAll = false;
  List _vehicleInfo = [];
  @override
  void initState() {
    getVehicleInfo();
    super.initState();
  }

  getVehicleInfo() {
    _vehicleInfo = unloadingData
        .where((element) => element.vehicleId == widget.vehicleId)
        .toList();
    print(_vehicleInfo[0].vehicleNo);
  }

  @override
  Widget build(BuildContext context) {
    return CommonBody(
      appLogo: 'logisticManager.png',
      appTitle: 'Vehicle-wise Unloading at DC',
      onPressed: () {
        Navigator.pop(context);
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Spacer(
            flex: 1,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Image.asset(
                    'assets/icons/logistics.png',
                  ),
                  SizedBox(
                    width: 10.0,
                  ),
                  Text(
                    _vehicleInfo[0].vehicleNo,
                    style: TextStyle(fontSize: appSubHeadingFontSize),
                  ),
                ],
              ),
              Row(
                children: [
                  Image.asset(
                    'assets/icons/tool-box3.png',
                  ),
                  SizedBox(
                    width: 10.0,
                  ),
                  Text(_vehicleInfo[0].crates.toString(),
                      style: TextStyle(fontSize: appSubHeadingFontSize)),
                ],
              ),
            ],
          ),
          Divider(
            color: Color(0xff6BCC8C),
          ),
          InkWell(
            onTap: () {
              setState(() {
                selectAll = !selectAll;
              });
            },
            child: Text('SelectAll',
                style: TextStyle(fontSize: appNormalTextSize)),
          ),
          Flexible(
            child: ListView.builder(
                itemCount: productDataNew
                    .where((element) => element.vehicleId == widget.vehicleId)
                    .length,
                itemBuilder: (BuildContext context, index) {
                  ProductDataNew _productData = productDataNew[index];
                  return AppCard(
                    child: Container(
                      padding: EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              InkWell(
                                onTap: () {
                                  setState(() {
                                    _productData.selected =
                                        !_productData.selected;
                                  });
                                },
                                child: Container(
                                  height: SizeConfig.heightMultiplier * 3,
                                  width: SizeConfig.widthMultiplier * 4,
                                  child: selectAll
                                      ? Icon(
                                          Icons.check_circle,
                                          color: Colors.green,
                                          size: 15.0,
                                        )
                                      : !_productData.selected
                                          ? Icon(
                                              Icons.check_circle,
                                              color: Colors.green,
                                              size: 15.0,
                                            )
                                          : Image.asset(
                                              'assets/icons/error.png'),
                                ),
                              ),
                              Container(
                                width: SizeConfig.widthMultiplier * 38,
                                child: Row(
                                  children: [
                                    Image.asset(_productData.productImg),
                                    SizedBox(
                                      width: 10.0,
                                    ),
                                    Text(_productData.productName,
                                        style: TextStyle(
                                            fontSize: appNormalTextSize)),
                                  ],
                                ),
                              ),
                              Container(
                                height: 25,
                                child: VerticalDivider(
                                  color: Colors.green,
                                  thickness: 0.5,
                                ),
                              ),
                              Text(
                                _productData.productQty,
                                style: TextStyle(fontSize: appNormalTextSize),
                              ),
                              Container(
                                height: 25,
                                child: VerticalDivider(
                                  color: Colors.green,
                                  thickness: 0.5,
                                ),
                              ),
                              Container(
                                child: Row(
                                  children: [
                                    Image.asset(_productData.crates),
                                    SizedBox(
                                      width: 10.0,
                                    ),
                                    Text(_productData.cratesQty,
                                        style: TextStyle(
                                            fontSize: appNormalTextSize)),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 1.5 * SizeConfig.heightMultiplier,
                          ),
                          Container(
                            height: 3 * SizeConfig.heightMultiplier,
                            width: 23 * SizeConfig.widthMultiplier,
                            child: RaisedButton(
                              padding: EdgeInsets.symmetric(vertical: 5),
                              color: Color(0xffF36523),
                              onPressed: () {},
                              child: Text(
                                'Raise Issue',
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                }),
            flex: 8,
          ),
          AppButton(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => LoadingAtDc()));
            },
            name: 'Unloading complete at DC',
          ),
          Spacer(
            flex: 1,
          ),
        ],
      ),
    );
  }
}
